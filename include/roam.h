// This file is part of libroam
// Copyright (c) 2019-2022 Greg Griffith
// Copyright (c) 2019-2024 The Roam Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef ROAM_INCLUDE_ROAM_H
#define ROAM_INCLUDE_ROAM_H

#include <cstdint>
#include <string>

#include "roam_datapacket.h"
#include "roam_version.h"

#if defined(_WIN32) || defined(__MINGW32__) || defined(__MINGW64__)
# define ROAM_API __declspec(dllexport)
#else
# define ROAM_API __attribute__ ((visibility ("default")))
#endif

static const std::string ROAM_MESSAGE_MAGIC = "ROAM Signed Message:\n";

ROAM_API uint16_t roam_get_mainnet_port();

ROAM_API uint16_t roam_get_testnet_port();

// packet manager uses a sqlite database to store information across application restarts
// strDirName is the directory in which the packet manager database is located
// strFileName is the name of the file in that directory
ROAM_API void roam_initalise(const char* c_str_dir_name, const uint64_t c_str_dir_name_len,
    const char* c_str_file_name, const uint64_t c_str_file_name_len);

ROAM_API void roam_shutdown();

// the data is expected to be raw binary data from the network
ROAM_API bool roam_process_packet(const uint8_t* data, const uint64_t len_data);

// A backup for roam_process_packet that passes in a packet by fields
ROAM_API bool roam_process_packet_from_data(uint8_t packet_version,
    uint16_t dest_buffer_id,
    uint16_t source_buffer_id,
    uint64_t packet_total_length,
    uint8_t* packet_checksum,
    uint64_t packet_checksum_len,
    uint64_t salt,
    uint8_t* sender_pubkey,
    uint64_t sender_pubkey_len,
    uint8_t* sender_signature,
    uint64_t sender_signature_len,
    uint8_t* packet_data,
    uint64_t packet_data_len);

// the following description has been replaced by a redesign to build roam into applications directly
// rather than running as system service. It is considered in the future to write a system service version as well

// the use of a pupkey with buffers is analagous to a descriptor to a socket on the OS
// only the process holding the pubkey(descriptior) can access the data
// manually calling roam_read_messages and getbufferkey should eventually be replaced by
// a better signal system such as ZMQ (similar to the event queue at the OS level). this may require the messages to be encrypted so that
// only the desired listening application may decipher the data which of course adds some overhead
// but is probably better for security.
// afterthought - might be best to open a pipe/socket (or other IPC?) on a random port to the registering application instead of zmq as mentioned above

ROAM_API uint16_t roam_register_new_application(const bool &bool_reject,
    char* res_c_str_pubkey,
    const uint64_t res_c_str_pubkey_max_len);

ROAM_API bool roam_release_registered_application(const uint16_t &n_protocol_id);


//bool isPrivate;
// tags are compressed
//PubKey vchPubKey;
//PrivKey vchPrivKey;


ROAM_API bool roam_add_tag(const uint8_t* data, const uint64_t len_data);


ROAM_API bool roam_add_tag_from_data(bool bool_is_private,
    const uint8_t* pubkey_data,
    const uint64_t pubkey_data_len,
    const uint8_t* privkey_data,
    const uint64_t privkey_data_len);

// always len 20
ROAM_API bool roam_have_tag(const uint8_t* data);

// Note - max_data_len needs to be divisible by 2
ROAM_API uint64_t roam_get_buffer_ids_with_new_data(uint8_t* data, uint64_t max_data_len);

ROAM_API int64_t roam_create_message(const uint8_t* pubkey, const uint64_t len_pubkey, const uint16_t &n_protocol_id,
    const uint8_t* data, const uint64_t lenData, uint8_t* ret, const uint64_t max_len_ret);

ROAM_API int64_t roam_create_message_sig(const uint8_t* pubkey, uint64_t len_pubkey, const uint16_t &n_protocol_id,
    const uint8_t* data, uint64_t len_data, const char* c_str_sender_pubkey, uint64_t len_sender_pubkey,
    const uint8_t* signature, uint64_t len_signature, uint8_t* ret, const uint64_t max_len_ret);

ROAM_API int64_t roam_read_messages(const uint16_t &n_protocol_id, uint8_t* ret, const uint64_t lenRet);

ROAM_API void roam_record_request_origin(const uint64_t &nonce, const uint8_t* source_pubkey_bytes, const uint64_t &len_pubkey);

ROAM_API uint64_t roam_get_request_origin(const uint64_t &nonce, uint8_t* source_pubkey_bytes, const uint64_t &max_len_pubkey);

ROAM_API void roam_record_route_to_peer(const uint8_t* search_pubkey_bytes, const uint64_t &len_pubkey, const int64_t &node);

ROAM_API bool roam_have_route_to_peer(const uint8_t* search_pubkey_bytes, const uint64_t &len_pubkey);

ROAM_API bool roam_get_route_to_peer(const uint8_t* search_pubkey_bytes, const uint64_t &len_pubkey, int64_t &node_id);

ROAM_API uint64_t roam_get_public_tag_hex(uint8_t* ret, const uint64_t &max_len_ret);

ROAM_API uint64_t roam_sign_with_public_tag(const uint8_t* message, const uint64_t &len_message,
    uint8_t* ret, const uint64_t &max_len_ret);

ROAM_API bool roam_is_old_request(const uint64_t nonce);

ROAM_API void roam_add_new_request_time_data(const uint64_t nonce);

#endif // ROAM_INCLUDE_ROAM_H
