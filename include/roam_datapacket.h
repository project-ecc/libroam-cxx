// This file is part of libroam
// Copyright (c) 2019-2022 Greg Griffith
// Copyright (c) 2019-2024 The Roam Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef ROAM_INCLUDE_DATAPACKET_H
#define ROAM_INCLUDE_DATAPACKET_H

#include <inttypes.h>
#include <random>
#include <vector>

#include "roam_serialize.h"

const uint64_t MEGABYTE = 1000000;
// BEGIN Roam Consensus Variales
const uint64_t ROAM_PACKET_HEADER_SIZE = 45; // 45 bytes
const uint64_t ROAM_MAX_DATA_SEGMENT_SIZE = 1 * MEGABYTE;
const uint64_t ROAM_MAX_SEGMENTS_PER_PACKET = 1;
const uint64_t ROAM_MAX_PACKET_SIZE = ROAM_PACKET_HEADER_SIZE + (ROAM_MAX_SEGMENTS_PER_PACKET * ROAM_MAX_DATA_SEGMENT_SIZE);
// END
const uint8_t ROAM_PACKET_VERSION = 1;

class RoamPacketHeader
{
public:
    // versioning of the RoamPacketHeader and related classes
    uint8_t n_packet_version;
    // n_dest_buffer_id should match the bufferId in vec_buffers in the peers packet manager
    // a bufferId of 0 is an unbound buffer. this is used by the peer initiating the connection
    // (the client in the client-server model) to signal to the destination peer (the server in
    // the client-server model) that this is a new connection.
    uint16_t n_dest_buffer_id;
    // n_source_buffer_id is the bufferid in vec_buffers in our packet manager, we expect the peers
    // to respond by sending the response to this buffer Id
    uint16_t n_source_buffer_id;
    // packet serialize size
    uint64_t n_total_length;
    // sha256 checksum of the data, use a byte vector for expandability in the future
    std::vector<uint8_t> n_checksum;

    // needed for message deserialisation
    RoamPacketHeader() {}

    RoamPacketHeader(const uint16_t n_dest_buffer_idIn, const uint16_t n_source_buffer_idIn)
    {
        set_null();
        n_dest_buffer_id = n_dest_buffer_idIn;
        n_source_buffer_id = n_source_buffer_idIn;
        n_total_length = ROAM_PACKET_HEADER_SIZE;
    }

    void set_null()
    {
        n_packet_version = ROAM_PACKET_VERSION;
        n_dest_buffer_id = 0;
        n_source_buffer_id = 0;
        n_checksum.clear();
        n_checksum.reserve(32);
        n_total_length = ROAM_PACKET_HEADER_SIZE;
    }

    ADD_SERIALIZE_METHODS
    template <typename Stream, typename Operation>
    inline void SerializationOp(Stream &s, Operation ser_action)
    {
        READWRITE(n_packet_version);
        READWRITE(n_dest_buffer_id);
        READWRITE(n_source_buffer_id);
        READWRITE(n_total_length);
        READWRITE(n_checksum);
    }

    friend class RoamPacket;
};

class RoamPacket : public RoamPacketHeader
{
public:
    // randomly generated, added to prevent the same checksum for
    // two messages with the same content data
    uint64_t n_salt;

    std::vector<uint8_t> v_sender; // pubkey of sender
    std::vector<uint8_t> v_signature; // sig of the sender
    std::vector<uint8_t> v_data;

    RoamPacket() : RoamPacketHeader() {}

    RoamPacket(const RoamPacketHeader &header) : RoamPacketHeader(header)
    {
        *((RoamPacketHeader *)this) = header;
    }

    RoamPacket(const uint16_t n_dest_buffer_idIn, const uint16_t n_source_buffer_idIn)
    {
        set_null();
        this->n_dest_buffer_id = n_dest_buffer_idIn;
        this->n_source_buffer_id = n_source_buffer_idIn;
    }

    void set_null()
    {
        n_packet_version = ROAM_PACKET_VERSION;
        n_total_length = 0;
        n_dest_buffer_id = 0;
        n_source_buffer_id = 0;
        n_checksum.clear();
        n_checksum.reserve(32);
        v_sender.clear();
        v_signature.clear();
        v_data.clear();
        n_total_length = ROAM_PACKET_HEADER_SIZE;
    }

    ADD_SERIALIZE_METHODS
    template <typename Stream, typename Operation>
    inline void SerializationOp(Stream &s, Operation ser_action)
    {
        READWRITE(*(RoamPacketHeader *)this);
        READWRITE(n_salt);
        READWRITE(v_sender);
        READWRITE(v_signature);
        READWRITE(v_data);
    }
};

#endif // ROAM_INCLUDE_DATAPACKET_H
