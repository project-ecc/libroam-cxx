// Copyright (c) 2022 Greg Griffith
// Copyright (c) 2022-2023 The Roam developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "test_main.h"
#include "key.h"

#ifdef WIN32
std::string GetSpecialFolderPath(int nFolder, bool fCreate)
{
    char pszPath[MAX_PATH] = "";

    if (SHGetSpecialFolderPathA(nullptr, pszPath, nFolder, fCreate))
    {
        return std::string(pszPath);
    }

    LOGA("SHGetSpecialFolderPathA() failed, could not obtain requested path.\n");
    return "";
}
#endif

std::string GetDefaultDataDir()
{
// Windows < Vista: C:\Documents and Settings\Username\Application Data\Bitcoin
// Windows >= Vista: C:\Users\Username\AppData\Roaming\Bitcoin
// Mac: ~/Library/Application Support/Bitcoin
// Unix: ~/.bitcoin
#ifdef WIN32
    // Windows
    return GetSpecialFolderPath(CSIDL_APPDATA) + std::string("libroam");
#else
    std::string pathRet;
    char *pszHome = getenv("HOME");
    if (pszHome == nullptr || strlen(pszHome) == 0)
    {
        pathRet = std::string("/");
    }
    else
    {
        pathRet = std::string(pszHome);
    }
#ifdef MAC_OSX
    // Mac
    return pathRet = pathRet + "/Library/Application Support/" + std::string("libroam");
#else
    // Unix
    return pathRet = pathRet + std::string("/.") + std::string("libroam");
#endif
#endif
}

int main()
{
    // get the home directory
    const std::string strDirName = GetDefaultDataDir();
    const std::string strFileName = "roam_test.db";
    test_create(strDirName, strFileName);
    test_basics();
    roam_shutdown();
}
