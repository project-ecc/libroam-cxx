// Copyright (c) 2022 Greg Griffith
// Copyright (c) 2022-2023 The Roam developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "test_main.h"

uint16_t test_buffer_registration(std::string &str_pubkey, bool reject)
{
    char res_c_str_pubkey[1000];
    uint16_t bufferId = roam_register_new_application(reject, res_c_str_pubkey, 1000);
    assert(bufferId != 0);
    str_pubkey = std::string(res_c_str_pubkey);
    assert(str_pubkey != "");
    return bufferId;
}

void test_packet_create(const RoutingTag &destTag, const uint16_t nSendBufferId,
    const std::vector<uint8_t> &v_data, std::vector<uint8_t> &ret)
{
    std::vector<uint8_t> vPubKey;
    PubKey pubkey = destTag.get_pubkey();
    for (size_t i = 0; i < pubkey.size(); ++i)
    {
        vPubKey.emplace_back(pubkey[i]);
    }
    ret.reserve(1000);
    int64_t psize = roam_create_message(vPubKey.data(), vPubKey.size(), nSendBufferId, v_data.data(), v_data.size(), ret.data(), 1000);
    assert (psize != 0);
}

void test_buffer_send(const std::vector<uint8_t> packet)
{
    // intentionally left blank for now
}

void test_buffer_read(const uint16_t bufferId, std::vector<uint8_t> &v_data)
{
    uint8_t tmpData[1000];
    int64_t ret = roam_read_messages(bufferId, tmpData, 1000);
    std::vector<RoamPacket> vPackets;
    DataStream ds(tmpData, tmpData + ret, SER_NETWORK, ROAM_GENERAL_VERSION);
    ds >> vPackets;
    for (RoamPacket &packet : vPackets)
    {
        std::vector<uint8_t> vpacketData = packet.v_data;
        v_data.insert(v_data.end(), vpacketData.begin(), vpacketData.end());
    }
    assert(v_data.empty() == false);
}

void test_buffer_close(const uint16_t bufferId)
{
    assert(roam_release_registered_application(bufferId) == true);
}

void test_basics()
{
    // setup some tags
    RoutingTag tag1;
    tag1.make_new_tag();
    DataStream ds(SER_NETWORK, ROAM_GENERAL_VERSION);
    ds << tag1;
    uint8_t* tag1_data = ds.data();
    const size_t len_tag1_data = ds.size();
    assert(roam_add_tag(tag1_data, len_tag1_data));

    PubKey searchKey1 = tag1.get_pubkey();
    printf("searchKey1 raw = %s \n", searchKey1.get_hex().c_str());
    printf("searchKey1 hash = %s \n", searchKey1.get_hash().get_hex().c_str());
    KeyID searchKeyID1 = searchKey1.get_key_id();
    assert(roam_have_tag(searchKeyID1.begin()));

    RoutingTag tag2;
    tag2.make_new_tag();
    ds.clear();
    ds << tag2;
    uint8_t* tag2_data = ds.data();
    const size_t len_tag2_data = ds.size();
    assert(roam_add_tag(tag2_data, len_tag2_data));

    PubKey searchKey2 = tag2.get_pubkey();
    printf("searchKey2 raw = %s \n", searchKey2.get_hex().c_str());
    printf("searchKey2 hash = %s \n", searchKey2.get_hash().get_hex().c_str());
    KeyID searchKeyID2 = searchKey2.get_key_id();
    assert(roam_have_tag(searchKeyID2.begin()));

    // test buffer registration
    std::string strPubkey1 = "";
    bool reject = false;
    uint16_t buffer_id_1 = test_buffer_registration(strPubkey1, reject);
    printf("registered buffer %u \n", buffer_id_1);

    // test creating a packet
    std::vector<uint8_t> packet1;
    std::string strData = "Hello";
    std::vector<uint8_t> data1(strData.begin(), strData.end());
    // send to tag 2, the response buffer id for tag2 to use is buffer_id_1,
    // send data1, get packet1 back to send
    test_packet_create(tag2, buffer_id_1, data1, packet1);

    // test sending from one buffer to another
    //test_buffer_send(packet1);

    // check for available data
    // we should have available data in the second buffer we made when sending to ourselves
    uint16_t c_buffers_with_data[1000];
    uint64_t res = roam_get_buffer_ids_with_new_data((uint8_t*)c_buffers_with_data, 1000 * sizeof(uint16_t));
    assert(res == 1);
    printf("c_buffers_with_data[0] = %u \n", c_buffers_with_data[0]);
    std::vector<uint16_t> v_buffers_with_data(c_buffers_with_data, c_buffers_with_data + res);
    printf("v_buffers_with_data size = %lu \n", v_buffers_with_data.size());
    printf("v_buffers_with_data[0] = %u \n", v_buffers_with_data[0]);
    assert(v_buffers_with_data.size() == 1);

    uint16_t bufferId2 = v_buffers_with_data[0];
    // test reading the received message in the receiving buffer
    std::vector<uint8_t> bufferData;
    bufferData.reserve(100);
    test_buffer_read(bufferId2, bufferData);
    assert(data1 == bufferData);

    // test closing the buffers
    test_buffer_close(buffer_id_1);
    test_buffer_close(bufferId2);
}
