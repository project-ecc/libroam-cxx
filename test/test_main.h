// Copyright (c) 2022 Greg Griffith
// Copyright (c) 2022-2023 The Roam developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef ROAM_TEST_TESTMAIN_H
#define ROAM_TEST_TESTMAIN_H

// roam,h includes
#include "include/roam.h"

// roam.cpp includes
#include "lib/packetmanager.h"

#include <assert.h>

void test_create(const std::string &strDirName, const std::string &strFileName);
void test_basics();

#endif // ROAM_TEST_TESTMAIN_H
