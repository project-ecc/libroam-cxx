// Copyright (c) 2022 Greg Griffith
// Copyright (c) 2022-2023 The Roam developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "test_main.h"

void test_create(const std::string &strDirName, const std::string &strFileName)
{
    const char* c_strDirName = strDirName.c_str();
    const char* c_strFileName = strFileName.c_str();
    roam_initalise(c_strDirName, strlen(c_strDirName), c_strFileName, strlen(c_strFileName));
}
