# Libroam++
Libroam++ depends on the secp256k1 lib and sqlite3 amalgamation.


## Secp256k1 library
To pull updates for libsecp256k1 from its upstream repository, run the following commands:
```
git fetch libsecp256k1 master
git subtree pull --prefix lib/libsecp256k1 master --squash

```

## Sqlite3 amalgamation
See info about the SQLite amalgamation inclusion [here.](/lib/sqlite/README.md)


## Building Libroam++
Libroam++ uses the automake build system.

### Building on Debian/Ubuntu
To build run the following commands:
```
apt-get install build-essential libtool autotools-dev automake pkg-config ca-certificates ccache
./autogen.sh
./configure
make -j `nproc`
```

### Building for Android
For Android cross compilation (all architectures, requires ubuntu24+):
1. sudo apt-get install android-sdk google-android-cmdline-tools-13.0-installer
2. sudo sdkmanager "cmake;3.22.1" "ndk;27.2.12479018" "platforms;android-34" "build-tools;34.0.0" "tools" "platform-tools" "cmdline-tools;latest"
3. sudo sdkmanager --licenses


## Coding style guide
- Class names should use the CapWords convention.  
- Function and method names should be lowercase, with words separated by underscores as necessary to improve readability.  
- Use one leading underscore only for non-public methods and instance/class variables.  
- Constants use all capital letters with underscores separating words.
