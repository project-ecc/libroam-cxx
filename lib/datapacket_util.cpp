// This file is part of libroam
// Copyright (c) 2019-2022 Greg Griffith
// Copyright (c) 2019-2022 The Roam Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "datapacket_util.h"
#include "datastream.h"
#include "pubkey.h"

std::vector<uint8_t> packet_header_serialize(const RoamPacketHeader &packet_header)
{
    DataStream ds_packet_header(SER_NETWORK, ROAM_GENERAL_VERSION);
    ds_packet_header << packet_header;
    return std::vector<uint8_t>(ds_packet_header.begin(), ds_packet_header.end());
}

std::vector<uint8_t> packet_serialize(const RoamPacket &packet)
{
    DataStream ds_packet(SER_NETWORK, ROAM_GENERAL_VERSION);
    ds_packet << packet;
    return std::vector<uint8_t>(ds_packet.begin(), ds_packet.end());
}

void packet_update_total_length(RoamPacket& packet)
{
    packet.n_total_length = ROAM_PACKET_HEADER_SIZE;
    packet.n_total_length += 8; // nSalt
    packet.n_total_length += GetSizeOfCompactSize(packet.v_sender.size()) + packet.v_sender.size();
    packet.n_total_length += GetSizeOfCompactSize(packet.v_signature.size()) + packet.v_signature.size();
    packet.n_total_length += GetSizeOfCompactSize(packet.v_data.size()) + packet.v_data.size();
}

void packet_add_sender(RoamPacket& packet, const std::vector<uint8_t> &sender)
{
    packet.v_sender = sender;
    packet_update_total_length(packet);
}

void packet_add_signature(RoamPacket& packet, const std::vector<uint8_t> &sig)
{
    packet.v_signature = sig;
    packet_update_total_length(packet);
}

void packet_add_data(RoamPacket& packet, const std::vector<uint8_t> &data)
{
    packet.v_data.insert(packet.v_data.end(), data.begin(), data.end());
    packet_update_total_length(packet);
}

RoamPacketHeader packet_get_header(RoamPacket& packet)
{
    RoamPacketHeader header;
    header.n_packet_version = packet.n_packet_version;
    header.n_dest_buffer_id = packet.n_dest_buffer_id;
    header.n_source_buffer_id = packet.n_source_buffer_id;
    header.n_total_length = packet.n_total_length;
    header.n_checksum = packet.n_checksum;
    return header;
}

bool packet_verify_signature(const RoamPacket &packet)
{
    if (packet.v_sender.empty() && packet.v_signature.empty())
    {
        // No signature nor sender is valid
        return true;
    }

    HashWriter ss(SER_GETHASH, 0);
    ss << packet.v_data;

    PubKey sender(packet.v_sender);
    PubKey pubkey;
    if (!pubkey.recover_compact(ss.get_hash(), packet.v_signature))
    {
        // LogPrint("roam", "RoamPacket::VerifySignature(): ERROR, failed to recover compact pubkey \n");
        return false;
    }

    return (pubkey.get_key_id() == sender.get_key_id());
}
