// This file is part of libroam
// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2016 The Bitcoin Core developers
// Copyright (c) 2014-2019 The Eccoin developers
// Copyright (c) 2019-2022 Greg Griffith
// Copyright (c) 2019-2022 The Roam Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef ROAM_TAGDB_H
#define ROAM_TAGDB_H

#include "crypter.h"
#include "db.h"
#include "routingtag.h"

#include <stdint.h>
#include <string>
#include <utility>
#include <vector>

/**
 * Private key encryption is done based on a MasterKey,
 * which holds a salt and random encryption key.
 *
 * MasterKeys are encrypted using AES-256-CBC using a key
 * derived using derivation method nDerivationMethod
 * (0 == EVP_sha512()) and derivation iterations nDeriveIterations.
 * vchOtherDerivationParameters is provided for alternative algorithms
 * which may require more parameters (such as scrypt).
 *
 * Wallet Private Keys are then encrypted using AES-256-CBC
 * with the double-sha256 of the public key as the IV, and the
 * master key's key as the encryption key (see keystore.[ch]).
 */

/** Master key for wallet encryption */
class MasterKey
{
public:
    std::vector<uint8_t> vchCryptedKey;
    std::vector<uint8_t> vchSalt;
    //! 0 = EVP_sha512()
    //! 1 = scrypt()
    uint32_t nDerivationMethod;
    uint32_t nDeriveIterations;
    //! Use this for more parameters to key derivation,
    //! such as the various parameters to scrypt
    std::vector<uint8_t> vchOtherDerivationParameters;

    ADD_SERIALIZE_METHODS

    template <typename Stream, typename Operation>
    inline void SerializationOp(Stream &s, Operation ser_action)
    {
        READWRITE(vchCryptedKey);
        READWRITE(vchSalt);
        READWRITE(nDerivationMethod);
        READWRITE(nDeriveIterations);
        READWRITE(vchOtherDerivationParameters);
    }

    MasterKey()
    {
        // 25000 rounds is just under 0.1 seconds on a 1.86 GHz Pentium M
        // ie slightly lower than the lowest hardware we need bother supporting
        nDeriveIterations = 25000;
        nDerivationMethod = 0;
        vchOtherDerivationParameters = std::vector<uint8_t>(0);
    }
};


/** A key pool entry */
class KeyPoolEntry
{
public:
    int64_t n_time;
    PubKey vchPubKey;

    KeyPoolEntry()
    {
        n_time = get_time();
    }

    KeyPoolEntry(const PubKey &vchPubKeyIn)
    {
        n_time = get_time();
        vchPubKey = vchPubKeyIn;
    }

    ADD_SERIALIZE_METHODS

    template <typename Stream, typename Operation>
    inline void SerializationOp(Stream &s, Operation ser_action)
    {
        int n_version = s.get_version();
        if (!(s.get_type() & SER_GETHASH))
        {
            READWRITE(n_version);
        }
        READWRITE(n_time);
        READWRITE(vchPubKey);
    }
};

class NetTagStore;

/** Access to the routing database (routing.db) */
class RoutingTagDB : public SQLiteDatabase
{
public:
    RoutingTagDB(const std::string &strDirName, const std::string &strFileName)
        : SQLiteDatabase(strDirName, strFileName)
    {
    }

    bool write_tag(const RoutingTag &tag);
    bool write_crypted_tag(const RoutingTag &tag);
    bool write_master_tag(uint32_t nID, const MasterKey &kMasterKey);

    bool write_last_used_public_tag(RoutingTag &publicRoutingTag);
    bool read_last_used_public_tag(RoutingTag &publicRoutingTag);

    bool read_pool(int64_t nPool, KeyPoolEntry &keypool);
    bool write_pool(int64_t nPool, const KeyPoolEntry &keypool);
    bool erase_pool(int64_t nPool);

    bool load_all_records(NetTagStore *pwallet);

private:
    RoutingTagDB() = delete;
    RoutingTagDB(const RoutingTagDB &);
    void operator=(const RoutingTagDB &);

    bool read_key(const DataStream &key, DataStream &value);
    bool write_key(const DataStream &key, const DataStream &value, bool overwrite);
    bool erase_key(const DataStream &key);
};

#endif // ECCOIN_TAGDB_H
