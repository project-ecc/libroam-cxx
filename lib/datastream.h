// This file is part of the libroam c++ library
// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2016 The Bitcoin Core developers
// Copyright (c) 2014-2018 The Eccoin developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BITCOIN_STREAMS_H
#define BITCOIN_STREAMS_H

#include "include/roam_serialize.h"
#include "include/roam_version.h"
#include "secure_memory.h"

#include <algorithm>
#include <assert.h>
#include <ios>
#include <limits>
#include <map>
#include <set>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <string>
#include <utility>
#include <vector>

/**
 * Double ended buffer combining vector and stream-like interfaces.
 *
 * >> and << read and write unformatted data using the above serialization
 * templates. Fills with data in linear time; some stringstream implementations
 * take N^2 time.
 */
class DataStream
{
protected:
    typedef CSerializeData vector_type;
    vector_type vch;
    uint32_t n_read_pos;

    int n_type;
    int n_version;

public:
    typedef vector_type::allocator_type allocator_type;
    typedef vector_type::size_type size_type;
    typedef vector_type::difference_type difference_type;
    typedef vector_type::reference reference;
    typedef vector_type::const_reference const_reference;
    typedef vector_type::value_type value_type;
    typedef vector_type::iterator iterator;
    typedef vector_type::const_iterator const_iterator;
    typedef vector_type::reverse_iterator reverse_iterator;

    explicit DataStream(int nTypeIn, int nVersionIn)
    {
        init(nTypeIn, nVersionIn);
    }
    explicit DataStream()
    {
        init(SER_NETWORK, ROAM_GENERAL_VERSION);
    }
    DataStream(const_iterator pbegin, const_iterator pend, int nTypeIn, int nVersionIn) : vch(pbegin, pend)
    {
        init(nTypeIn, nVersionIn);
    }

    DataStream(const char *pbegin, const char *pend, int nTypeIn, int nVersionIn) : vch(pbegin, pend)
    {
        init(nTypeIn, nVersionIn);
    }

    DataStream(const uint8_t *pbegin, const uint8_t *pend, int nTypeIn, int nVersionIn) : vch(pbegin, pend)
    {
        init(nTypeIn, nVersionIn);
    }

    DataStream(const vector_type &vchIn, int nTypeIn, int nVersionIn) : vch(vchIn.begin(), vchIn.end())
    {
        init(nTypeIn, nVersionIn);
    }

    DataStream(const std::vector<char> &vchIn, int nTypeIn, int nVersionIn) : vch(vchIn.begin(), vchIn.end())
    {
        init(nTypeIn, nVersionIn);
    }

    DataStream(const std::vector<uint8_t> &vchIn, int nTypeIn, int nVersionIn) : vch(vchIn.begin(), vchIn.end())
    {
        init(nTypeIn, nVersionIn);
    }

    template <typename... Args>
    DataStream(int nTypeIn, int nVersionIn, Args &&...args)
    {
        init(nTypeIn, nVersionIn);
        ::SerializeMany(*this, std::forward<Args>(args)...);
    }

    void init(int nTypeIn, int nVersionIn)
    {
        n_read_pos = 0;
        n_type = nTypeIn;
        n_version = nVersionIn;
    }

    DataStream &operator+=(const DataStream &b)
    {
        vch.insert(vch.end(), b.begin(), b.end());
        return *this;
    }

    friend DataStream operator+(const DataStream &a, const DataStream &b)
    {
        DataStream ret = a;
        ret += b;
        return (ret);
    }

    std::string str() const
    {
        return (std::string(begin(), end()));
    }
    //
    // Vector subset
    //
    const_iterator begin() const
    {
        return vch.begin() + n_read_pos;
    }
    iterator begin()
    {
        return vch.begin() + n_read_pos;
    }
    const_iterator end() const
    {
        return vch.end();
    }
    iterator end()
    {
        return vch.end();
    }
    size_type size() const
    {
        return vch.size() - n_read_pos;
    }
    bool empty() const
    {
        return vch.size() == n_read_pos;
    }
    void resize(size_type n, value_type c = 0)
    {
        vch.resize(n + n_read_pos, c);
    }
    void reserve(size_type n)
    {
        vch.reserve(n + n_read_pos);
    }
    const_reference operator[](size_type pos) const
    {
        return vch[pos + n_read_pos];
    }
    reference operator[](size_type pos)
    {
        return vch[pos + n_read_pos];
    }
    void clear()
    {
        vch.clear();
        n_read_pos = 0;
    }
    iterator insert(iterator it, const char &x = char())
    {
        return vch.insert(it, x);
    }
    void insert(iterator it, size_type n, const char &x)
    {
        vch.insert(it, n, x);
    }
    value_type *data()
    {
        return vch.data() + n_read_pos;
    }
    const value_type *data() const
    {
        return vch.data() + n_read_pos;
    }
    void insert(iterator it, std::vector<char>::const_iterator first, std::vector<char>::const_iterator last)
    {
        if (last == first)
        {
            return;
        }

        assert(last - first > 0);
        if (it == vch.begin() + n_read_pos && (uint32_t)(last - first) <= n_read_pos)
        {
            // special case for inserting at the front when there's room
            n_read_pos -= (last - first);
            memcpy(&vch[n_read_pos], &first[0], last - first);
        }
        else
        {
            vch.insert(it, first, last);
        }
    }

    void insert(iterator it, const char *first, const char *last)
    {
        if (last == first)
        {
            return;
        }

        assert(last - first > 0);
        if (it == vch.begin() + n_read_pos && (uint32_t)(last - first) <= n_read_pos)
        {
            // special case for inserting at the front when there's room
            n_read_pos -= (last - first);
            memcpy(&vch[n_read_pos], &first[0], last - first);
        }
        else
        {
            vch.insert(it, first, last);
        }
    }

    iterator erase(iterator it)
    {
        if (it == vch.begin() + n_read_pos)
        {
            // special case for erasing from the front
            if (++n_read_pos >= vch.size())
            {
                // whenever we reach the end, we take the opportunity to clear
                // the buffer
                n_read_pos = 0;
                return vch.erase(vch.begin(), vch.end());
            }
            return vch.begin() + n_read_pos;
        }
        else
        {
            return vch.erase(it);
        }
    }

    iterator erase(iterator first, iterator last)
    {
        if (first == vch.begin() + n_read_pos)
        {
            // special case for erasing from the front
            if (last == vch.end())
            {
                n_read_pos = 0;
                return vch.erase(vch.begin(), vch.end());
            }
            else
            {
                n_read_pos = (last - vch.begin());
                return last;
            }
        }
        else
            return vch.erase(first, last);
    }

    inline void compact()
    {
        vch.erase(vch.begin(), vch.begin() + n_read_pos);
        n_read_pos = 0;
    }

    bool rewind(size_type n)
    {
        // Rewind by n characters if the buffer hasn't been compacted yet
        if (n > n_read_pos)
            return false;
        n_read_pos -= n;
        return true;
    }

    //
    // Stream subset
    //
    bool eof() const
    {
        return size() == 0;
    }
    DataStream *rdbuf()
    {
        return this;
    }
    int in_avail()
    {
        return size();
    }
    void set_type(int n)
    {
        n_type = n;
    }
    int get_type() const
    {
        return n_type;
    }
    void set_version(int n)
    {
        n_version = n;
    }
    int get_version() const
    {
        return n_version;
    }
    void read(char *pch, size_t nSize)
    {
        if (nSize == 0)
        {
            return;
        }

        // Read from the beginning of the buffer
        uint32_t nReadPosNext = n_read_pos + nSize;
        if (nReadPosNext >= vch.size())
        {
            if (nReadPosNext > vch.size())
            {
                throw std::ios_base::failure("DataStream::read(): end of data");
            }
            memcpy(pch, &vch[n_read_pos], nSize);
            n_read_pos = 0;
            vch.clear();
            return;
        }
        memcpy(pch, &vch[n_read_pos], nSize);
        n_read_pos = nReadPosNext;
    }

    void read(int8_t *pch, size_t nSize)
    {
        if (nSize == 0)
        {
            return;
        }

        // Read from the beginning of the buffer
        uint32_t nReadPosNext = n_read_pos + nSize;
        if (nReadPosNext >= vch.size())
        {
            if (nReadPosNext > vch.size())
            {
                throw std::ios_base::failure("DataStream::read(): end of data");
            }
            memcpy(pch, &vch[n_read_pos], nSize);
            n_read_pos = 0;
            vch.clear();
            return;
        }
        memcpy(pch, &vch[n_read_pos], nSize);
        n_read_pos = nReadPosNext;
    }

    void ignore(int nSize)
    {
        // Ignore from the beginning of the buffer
        if (nSize < 0)
        {
            throw std::ios_base::failure("DataStream::ignore(): nSize negative");
        }
        uint32_t nReadPosNext = n_read_pos + nSize;
        if (nReadPosNext >= vch.size())
        {
            if (nReadPosNext > vch.size())
                throw std::ios_base::failure("DataStream::ignore(): end of data");
            n_read_pos = 0;
            vch.clear();
            return;
        }
        n_read_pos = nReadPosNext;
    }

    void write(const char *pch, size_t nSize)
    {
        // Write to the end of the buffer
        vch.insert(vch.end(), pch, pch + nSize);
    }

    template <typename Stream>
    void Serialize(Stream &s) const
    {
        // Special case: stream << stream concatenates like stream += stream
        if (!vch.empty())
            s.write((char *)&vch[0], vch.size() * sizeof(vch[0]));
    }

    template <typename T>
    DataStream &operator<<(const T &obj)
    {
        // Serialize to this stream
        ::Serialize(*this, obj);
        return (*this);
    }

    template <typename T>
    DataStream &operator>>(T &obj)
    {
        // Unserialize from this stream
        ::Unserialize(*this, obj);
        return (*this);
    }

    void get_and_clear(CSerializeData &d)
    {
        d.insert(d.end(), begin(), end());
        clear();
    }

    /**
     * XOR the contents of this stream with a certain key.
     *
     * @param[in] key    The key used to XOR the data in this stream.
     */
     // must be Xor not xor to avoid conflicts with system libs
    void Xor(const std::vector<uint8_t> &key)
    {
        if (key.size() == 0)
        {
            return;
        }

        for (size_type i = 0, j = 0; i != size(); i++)
        {
            vch[i] ^= key[j++];

            // This potentially acts on very many bytes of data, so it's
            // important that we calculate `j`, i.e. the `key` index in this way
            // instead of doing a %, which would effectively be a division for
            // each byte Xor'd -- much slower than need be.
            if (j == key.size())
                j = 0;
        }
    }
};

#endif // BITCOIN_STREAMS_H
