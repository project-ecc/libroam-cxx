// This file is part of the libroam c++ library
// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2016 The Bitcoin Core developers
// Copyright (c) 2014-2021 The Eccoin developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BITCOIN_UTILTIME_H
#define BITCOIN_UTILTIME_H

#include <stdint.h>
#include <string>
#include <vector>

#define ARRAYLEN(array) (sizeof(array) / sizeof((array)[0]))

int64_t get_time();
int64_t get_time_millis();
int64_t get_time_micros();
int64_t get_system_time_in_seconds();
void milli_sleep(int64_t n);

std::vector<uint8_t> decode_base64(const char *p, bool *pfInvalid = NULL);
std::string decode_base64(const std::string &str);
std::string encode_base64(const uint8_t *pch, size_t len);
std::string encode_base64(const std::string &str);

/**
 * Remove unsafe chars. Safe chars chosen to allow simple messages/URLs/email
 * addresses, but avoid anything even possibly remotely dangerous like & or >
 * @param[in] str    The string to sanitize
 * @param[in] rule   The set of safe chars to choose (default: least restrictive)
 * @return           A new string without unsafe chars
 */
signed char hex_digit(char c);
std::vector<uint8_t> parse_hex(const char *psz);
std::vector<uint8_t> parse_hex(const std::string &str);

template <typename T>
std::string hex_str(const T itbegin, const T itend, bool fSpaces = false)
{
    std::string rv;
    static const char hexmap[16] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    rv.reserve((itend - itbegin) * 3);
    for (T it = itbegin; it < itend; ++it)
    {
        unsigned char val = (unsigned char)(*it);
        if (fSpaces && it != itbegin)
            rv.push_back(' ');
        rv.push_back(hexmap[val >> 4]);
        rv.push_back(hexmap[val & 15]);
    }

    return rv;
}

template <typename T>
inline std::string hex_str(const T &vch, bool fSpaces = false)
{
    return hex_str(vch.begin(), vch.end(), fSpaces);
}

#endif // BITCOIN_UTILTIME_H
