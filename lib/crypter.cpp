// This file is part of the libroam c++ library
// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2016 The Bitcoin Core developers
// Copyright (c) 2014-2018 The Eccoin developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "crypter.h"

#include "crypto/aes.h"
#include "crypto/sha512.h"
#include "util.h"

#include <string>
#include <vector>

int Crypter::bytes_to_key_SHA512AES(const std::vector<uint8_t>& chSalt, const SecureString& strKeyData, int count, uint8_t *key,uint8_t *iv) const
{
    // This mimics the behavior of openssl's EVP_BytesToKey with an aes256cbc
    // cipher and sha512 message digest. Because sha512's output size (64b) is
    // greater than the aes256 block size (16b) + aes256 key size (32b),
    // there's no need to process more than once (D_0).

    if(!count || !key || !iv)
        return 0;

    uint8_t buf[SHA512::OUTPUT_SIZE];
    SHA512 di;

    di.write((const uint8_t*)strKeyData.data(), strKeyData.size());
    di.write(chSalt.data(), chSalt.size());
    di.finalize(buf);

    for(int i = 0; i != count - 1; i++)
    {
        di.reset().write(buf, sizeof(buf)).finalize(buf);
    }

    memcpy(key, buf, WALLET_CRYPTO_KEY_SIZE);
    memcpy(iv, buf + WALLET_CRYPTO_KEY_SIZE, WALLET_CRYPTO_IV_SIZE);
    memory_cleanse(buf, sizeof(buf));
    return WALLET_CRYPTO_KEY_SIZE;
}

bool Crypter::set_key_from_passphrase(const SecureString &strKeyData,
    const std::vector<uint8_t> &chSalt,
    const uint32_t nRounds,
    const uint32_t nDerivationMethod)
{
    if (nRounds < 1 || chSalt.size() != WALLET_CRYPTO_SALT_SIZE)
    {
        return false;
    }
    int32_t i = 0;
    if (nDerivationMethod == 0)
    {
        i = bytes_to_key_SHA512AES(chSalt, strKeyData, nRounds, chKey, chIV);
    }
    if (i < 0 || (uint32_t)i != WALLET_CRYPTO_KEY_SIZE)
    {
        memory_cleanse(chKey, sizeof(chKey));
        memory_cleanse(chIV, sizeof(chIV));
        return false;
    }

    fKeySet = true;
    return true;
}

bool Crypter::set_key(const KeyingMaterial &chNewKey, const std::vector<uint8_t> &chNewIV)
{
    if (chNewKey.size() != WALLET_CRYPTO_KEY_SIZE || chNewIV.size() != WALLET_CRYPTO_KEY_SIZE)
        return false;

    memcpy(&chKey[0], &chNewKey[0], sizeof chKey);
    memcpy(&chIV[0], &chNewIV[0], sizeof chIV);

    fKeySet = true;
    return true;
}

bool Crypter::encrypt(const KeyingMaterial &vchPlaintext, std::vector<uint8_t> &vchCiphertext)
{
    if (!fKeySet)
        return false;
    // max ciphertext len for a n bytes of plaintext is
    // n + AES_BLOCKSIZE bytes
    vchCiphertext.resize(vchPlaintext.size() + AES_BLOCKSIZE);

    AES256CBCEncrypt enc(chKey, chIV, true);
    size_t nLen = enc.encrypt(&vchPlaintext[0], vchPlaintext.size(), &vchCiphertext[0]);
    if (nLen < vchPlaintext.size())
        return false;
    vchCiphertext.resize(nLen);
    return true;
}

bool Crypter::encrypt(const KeyingMaterial &vchPlaintext, CryptedPrivKey &vchCiphertext)
{
    if (!fKeySet)
        return false;
    // max ciphertext len for a n bytes of plaintext is
    // n + AES_BLOCKSIZE bytes
    vchCiphertext.resize(vchPlaintext.size() + AES_BLOCKSIZE);

    AES256CBCEncrypt enc(chKey, chIV, true);
    size_t nLen = enc.encrypt(&vchPlaintext[0], vchPlaintext.size(), &vchCiphertext[0]);
    if (nLen < vchPlaintext.size())
        return false;
    vchCiphertext.resize(nLen);
    return true;
}

bool Crypter::decrypt(const std::vector<uint8_t> &vchCiphertext, KeyingMaterial &vchPlaintext)
{
    if (!fKeySet)
        return false;
    // plaintext will always be equal to or lesser than length of ciphertext
    int nLen = vchCiphertext.size();
    vchPlaintext.resize(nLen);
    AES256CBCDecrypt dec(chKey, chIV, true);
    nLen = dec.decrypt(&vchCiphertext[0], vchCiphertext.size(), &vchPlaintext[0]);
    if (nLen == 0)
        return false;
    vchPlaintext.resize(nLen);
    return true;
}

bool Crypter::decrypt(const CryptedPrivKey &vchCiphertext, KeyingMaterial &vchPlaintext)
{
    if (!fKeySet)
        return false;
    // plaintext will always be equal to or lesser than length of ciphertext
    int nLen = vchCiphertext.size();
    vchPlaintext.resize(nLen);
    AES256CBCDecrypt dec(chKey, chIV, true);
    nLen = dec.decrypt(&vchCiphertext[0], vchCiphertext.size(), &vchPlaintext[0]);
    if (nLen == 0)
        return false;
    vchPlaintext.resize(nLen);
    return true;
}
