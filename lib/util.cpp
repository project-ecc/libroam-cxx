// This file is part of the libroam c++ library
// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2016 The Bitcoin Core developers
// Copyright (c) 2014-2018 The Eccoin developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "util.h"

#include <atomic>
#include <cassert>
#include <chrono>
#include <cstring>
#include <thread>

int64_t get_time()
{
    time_t now = time(NULL);
    assert(now > 0);
    return now;
}

int64_t get_time_millis()
{
    std::chrono::time_point<std::chrono::system_clock> clock_now = std::chrono::system_clock::now();
    int64_t now = std::chrono::duration_cast<std::chrono::milliseconds>(clock_now.time_since_epoch()).count();
    assert(now > 0);
    return now;
}

int64_t get_time_micros()
{
    std::chrono::time_point<std::chrono::system_clock> clock_now = std::chrono::system_clock::now();
    int64_t now = std::chrono::duration_cast<std::chrono::microseconds>(clock_now.time_since_epoch()).count();
    assert(now > 0);
    return now;
}

int64_t get_system_time_in_seconds()
{
    return get_time_micros() / 1000000;
}

void milli_sleep(int64_t n)
{
    std::this_thread::sleep_for(std::chrono::milliseconds(n));
}

std::string encode_base64(const uint8_t *pch, size_t len)
{
    static const char *pbase64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    std::string strRet = "";
    strRet.reserve((len + 2) / 3 * 4);
    int mode = 0;
    int left = 0;
    const uint8_t *pchEnd = pch + len;
    while (pch < pchEnd)
    {
        int enc = *(pch++);
        switch (mode)
        {
        case 0: // we have no bits
            strRet += pbase64[enc >> 2];
            left = (enc & 3) << 4;
            mode = 1;
            break;

        case 1: // we have two bits
            strRet += pbase64[left | (enc >> 4)];
            left = (enc & 15) << 2;
            mode = 2;
            break;

        case 2: // we have four bits
            strRet += pbase64[left | (enc >> 6)];
            strRet += pbase64[enc & 63];
            mode = 0;
            break;
        }
    }
    if (mode)
    {
        strRet += pbase64[left];
        strRet += '=';
        if (mode == 1)
        {
            strRet += '=';
        }
    }
    return strRet;
}

std::string encode_base64(const std::string &str)
{
    return encode_base64((const uint8_t *)str.c_str(), str.size());
}

std::vector<uint8_t> decode_base64(const char *p, bool *pfInvalid)
{
    static const int decode64_table[256] = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1,
        63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
        12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28, 29, 30, 31, 32, 33,
        34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};

    if (pfInvalid)
    {
        *pfInvalid = false;
    }
    std::vector<uint8_t> vchRet;
    vchRet.reserve(strlen(p) * 3 / 4);
    int mode = 0;
    int left = 0;
    while (1)
    {
        int dec = decode64_table[(uint8_t)*p];
        if (dec == -1)
        {
            break;
        }
        p++;
        switch (mode)
        {
        case 0: // we have no bits and get 6
            left = dec;
            mode = 1;
            break;

        case 1: // we have 6 bits and keep 4
            vchRet.push_back((left << 2) | (dec >> 4));
            left = dec & 15;
            mode = 2;
            break;

        case 2: // we have 4 bits and get 6, we keep 2
            vchRet.push_back((left << 4) | (dec >> 2));
            left = dec & 3;
            mode = 3;
            break;

        case 3: // we have 2 bits and get 6
            vchRet.push_back((left << 6) | dec);
            mode = 0;
            break;
        }
    }
    if (pfInvalid)
    {
        switch (mode)
        {
        case 0: // 4n base64 characters processed: ok
            break;

        case 1: // 4n+1 base64 character processed: impossible
            *pfInvalid = true;
            break;

        case 2: // 4n+2 base64 characters processed: require '=='
            if (left || p[0] != '=' || p[1] != '=' || decode64_table[(uint8_t)p[2]] != -1)
                *pfInvalid = true;
            break;

        case 3: // 4n+3 base64 characters processed: require '='
            if (left || p[0] != '=' || decode64_table[(uint8_t)p[1]] != -1)
                *pfInvalid = true;
            break;
        }
    }
    return vchRet;
}

std::string decode_base64(const std::string &str)
{
    std::vector<uint8_t> vchRet = decode_base64(str.c_str());
    return (vchRet.size() == 0) ? std::string() : std::string((const char *)&vchRet[0], vchRet.size());
}


const signed char p_util_hexdigit[256] =
{
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, -1, -1, -1, -1, -1, -1,
    -1, 0xa, 0xb, 0xc, 0xd, 0xe, 0xf, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, 0xa, 0xb, 0xc, 0xd, 0xe, 0xf, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
};

signed char hex_digit(char c)
{
    return p_util_hexdigit[(uint8_t)c];
}

std::vector<uint8_t> parse_hex(const char *psz)
{
    // convert hex dump to vector
    std::vector<uint8_t> vch;
    while (true)
    {
        while (isspace(*psz))
            psz++;
        signed char c = hex_digit(*psz++);
        if (c == (signed char)-1)
            break;
        uint8_t n = (c << 4);
        c = hex_digit(*psz++);
        if (c == (signed char)-1)
            break;
        n |= c;
        vch.push_back(n);
    }
    return vch;
}

std::vector<uint8_t> parse_hex(const std::string &str)
{
    return parse_hex(str.c_str());
}
