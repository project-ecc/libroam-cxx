// This file is part of the libroam c++ library
// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2016 The Bitcoin Core developers
// Copyright (c) 2015-2017 The Bitcoin Unlimited developers
// Copyright (c) 2014-2018 The Eccoin developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BITCOIN_PUBKEY_H
#define BITCOIN_PUBKEY_H

#include "crypto/hash.h"
#include "include/roam_serialize.h"
#include "uint256.h"
#include "util.h"

#include <stdexcept>
#include <vector>

/** A reference to a Key: the Hash160 of its serialized public key */
class KeyID : public uint160
{
public:
    KeyID() : uint160() {}
    KeyID(const uint160 &in) : uint160(in) {}
};

typedef uint256 ChainCode;

/** An encapsulated secp256k1 public key. */
class PubKey
{
private:
    /**
     * Just store the serialized data.
     * Its length can very cheaply be computed from the first byte.
     */
    uint8_t vch[65];

    //! Compute the length of a pubkey with a given first byte.
    uint32_t static get_len(uint8_t chHeader)
    {
        if (chHeader == 2 || chHeader == 3)
        {
            return 33;
        }
        if (chHeader == 4 || chHeader == 6 || chHeader == 7)
        {
            return 65;
        }
        return 0;
    }

    //! Set this key data to be invalid
    void invalidate()
    {
        memset(vch, 0, 65);
        vch[0] = 0xFF;
    }

public:
    //! Construct an invalid public key.
    PubKey()
    {
        invalidate();
    }
    std::vector<uint8_t> get_bytes() const
    {
        std::vector<uint8_t> ch;
        for (uint32_t i = 0; i < size(); i++)
        {
            ch.emplace_back(vch[i]);
        }
        return ch;
    }

    std::string get_hex() const
    {
        std::vector<uint8_t> ch;
        for (uint32_t i = 0; i < size(); i++)
        {
            ch.emplace_back(vch[i]);
        }
        return "0x" + hex_str(ch);
    }

    //! Initialize a public key using begin/end iterators to byte data.
    template <typename T>
    void set(const T pbegin, const T pend)
    {
        int32_t len = pend == pbegin ? 0 : get_len(pbegin[0]);
        if (len && len == (pend - pbegin))
        {
            memcpy(vch, (uint8_t *)&pbegin[0], len);
        }
        else
        {
            invalidate();
        }
    }

    //! Construct a public key using begin/end iterators to byte data.
    template <typename T>
    PubKey(const T pbegin, const T pend)
    {
        set(pbegin, pend);
    }

    //! Construct a public key from a byte vector.
    PubKey(const std::vector<uint8_t> &_vch)
    {
        set(_vch.begin(), _vch.end());
    }
    PubKey(const std::string &_vch_hex_str)
    {
        std::vector<unsigned char> _vch = parse_hex(_vch_hex_str);
        set(_vch.begin(), _vch.end());
    }
    //! Simple read-only vector-like interface to the pubkey data.
    size_t size() const
    {
        return get_len(vch[0]);
    }
    const uint8_t *begin() const
    {
        return vch;
    }
    const uint8_t *end() const
    {
        return vch + size();
    }
    const uint8_t &operator[](uint32_t pos) const
    {
        return vch[pos];
    }
    //! Comparator implementation.
    friend bool operator==(const PubKey &a, const PubKey &b)
    {
        return a.vch[0] == b.vch[0] && std::memcmp(a.vch, b.vch, a.size()) == 0;
    }
    friend bool operator!=(const PubKey &a, const PubKey &b)
    {
        return !(a == b);
    }
    friend bool operator<(const PubKey &a, const PubKey &b)
    {
        return a.vch[0] < b.vch[0] || (a.vch[0] == b.vch[0] && std::memcmp(a.vch, b.vch, a.size()) < 0);
    }

    //! Implement serialization, as if this was a byte vector.
    uint32_t GetSerializeSize(int32_t n_type, int32_t n_version) const
    {
        return size() + 1;
    }
    template <typename Stream>
    void Serialize(Stream &s) const
    {
        uint32_t len = size();
        ::WriteCompactSize(s, len);
        s.write((char *)vch, len);
    }
    template <typename Stream>
    void Unserialize(Stream &s)
    {
        uint32_t len = ::ReadCompactSize(s);
        if (len <= 65)
        {
            s.read((int8_t *)vch, len);
        }
        else
        {
            // invalid pubkey, skip available data
            int8_t dummy;
            while (len--)
            {
                s.read(&dummy, 1);
            }
            invalidate();
        }
    }
    //! Get the KeyID of this public key (hash of its serialization)
    KeyID get_key_id() const
    {
        return KeyID(Hash160(vch, vch + size()));
    }
    //! Get the 256-bit hash of this public key.
    uint256 get_hash() const
    {
        return hash_dbl_sha256(vch, vch + size());
    }
    /*
     * Check syntactic correctness.
     *
     * Note that this is consensus critical as CheckSig() calls it!
     */
    bool is_valid() const
    {
        return size() > 0;
    }
    //! fully validate whether this is a valid public key (more expensive than is_valid())
    bool is_fully_valid() const;

    //! Check whether this is a compressed public key.
    bool is_compressed() const
    {
        return size() == 33;
    }
    /**
     * Verify a DER signature (~72 bytes).
     * If this public key is not fully valid, the return value will be false.
     */
    bool verify_ECDSA(const uint256 &hash, const std::vector<uint8_t> &vchSig) const;

    /**
     * Verify a Schnorr signature (=64 bytes).
     * If this public key is not fully valid, the return value will be false.
     */
    bool verify_Schnorr(const uint256 &hash, const std::vector<uint8_t> &vchSig) const;

    /**
     * Check whether a signature is normalized (lower-S).
     */
    static bool check_low_s(const std::vector<uint8_t> &vchSig);

    //! Recover a public key from a compact ECDSA signature.
    bool recover_compact(const uint256 &hash, const std::vector<uint8_t> &vchSig);

    //! Turn this public key into an uncompressed public key.
    bool decompress();

    //! Derive BIP32 child pubkey.
    bool derive(PubKey &pubkeyChild, uint256 &ccChild, uint32_t nChild, const uint256 &cc) const;
};

/** Users of this module must hold an Secp256k1VerifyHandle. The constructor and
 *  destructor of these are not allowed to run in parallel, though. */
class Secp256k1VerifyHandle
{
    static int ref_count;

public:
    Secp256k1VerifyHandle();
    ~Secp256k1VerifyHandle();
};

#endif // BITCOIN_PUBKEY_H
