// This file is part of the libroam c++ library
// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2016 The Bitcoin Core developers
// Copyright (c) 2014-2018 The Eccoin developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BITCOIN_WALLET_CRYPTER_H
#define BITCOIN_WALLET_CRYPTER_H

#include "key.h"
#include "include/roam_serialize.h"
#include "secure_memory.h"

const uint32_t WALLET_CRYPTO_KEY_SIZE = 32;
const uint32_t WALLET_CRYPTO_SALT_SIZE = 8;
const uint32_t WALLET_CRYPTO_IV_SIZE = 16;

typedef std::vector<uint8_t, secure_allocator<uint8_t> > KeyingMaterial;

/** Encryption/decryption context with key information */
class Crypter
{
private:
    uint8_t chKey[WALLET_CRYPTO_KEY_SIZE];
    uint8_t chIV[WALLET_CRYPTO_KEY_SIZE];
    bool fKeySet;

private:
    int bytes_to_key_SHA512AES(const std::vector<uint8_t>& chSalt, const SecureString& strKeyData, int32_t count, uint8_t *key, uint8_t *iv) const;

public:
    bool set_key_from_passphrase(const SecureString &strKeyData,
        const std::vector<uint8_t> &chSalt,
        const uint32_t nRounds,
        const uint32_t nDerivationMethod);
    bool encrypt(const KeyingMaterial &vchPlaintext, std::vector<uint8_t> &vchCiphertext);
    bool encrypt(const KeyingMaterial &vchPlaintext, CryptedPrivKey &vchCiphertext);
    bool decrypt(const std::vector<uint8_t> &vchCiphertext, KeyingMaterial &vchPlaintext);
    bool decrypt(const CryptedPrivKey &vchCiphertext, KeyingMaterial &vchPlaintext);
    bool set_key(const KeyingMaterial &chNewKey, const std::vector<uint8_t> &chNewIV);

    void clean_key()
    {
        memory_cleanse(chKey, sizeof(chKey));
        memory_cleanse(chIV, sizeof(chIV));
        fKeySet = false;
    }

    Crypter()
    {
        fKeySet = false;

        // Try to keep the key data out of swap (and be a bit over-careful to keep the IV that we don't even use out of
        // swap)
        // Note that this does nothing about suspend-to-disk (which will put all our key data on disk)
        // Note as well that at no point in this program is any attempt made to prevent stealing of keys by reading the
        // memory of the running process.
        LockedPageManager::Instance().LockRange(&chKey[0], sizeof chKey);
        LockedPageManager::Instance().LockRange(&chIV[0], sizeof chIV);
    }

    ~Crypter()
    {
        clean_key();

        LockedPageManager::Instance().UnlockRange(&chKey[0], sizeof chKey);
        LockedPageManager::Instance().UnlockRange(&chIV[0], sizeof chIV);
    }
};

#endif // BITCOIN_WALLET_CRYPTER_H
