// Copyright (c) 2014 The Bitcoin Core developers
// Copyright (c) 2015-2017 The Bitcoin Unlimited developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "hmac_sha512.h"

#include <string.h>

HMAC_SHA512::HMAC_SHA512(const uint8_t* key, size_t keylen)
{
    uint8_t rkey[128];
    if (keylen <= 128)
    {
        memcpy(rkey, key, keylen);
        memset(rkey + keylen, 0, 128 - keylen);
    }
    else
    {
        SHA512().write(key, keylen).finalize(rkey);
        memset(rkey + 64, 0, 64);
    }

    for (int n = 0; n < 128; n++)
    {
        rkey[n] ^= 0x5c;
    }
    outer.write(rkey, 128);

    for (int n = 0; n < 128; n++)
    {
        rkey[n] ^= 0x5c ^ 0x36;
    }
    inner.write(rkey, 128);
}

void HMAC_SHA512::finalize(uint8_t hash[OUTPUT_SIZE])
{
    uint8_t temp[64];
    inner.finalize(temp);
    outer.write(temp, 64).finalize(hash);
}
