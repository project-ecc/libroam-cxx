// This file is part of the libroam c++ library
// Copyright (c) 2014-2018 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BITCOIN_CRYPTO_RIPEMD160_H
#define BITCOIN_CRYPTO_RIPEMD160_H

#include <stdint.h>
#include <stdlib.h>

/** A hasher class for RIPEMD-160. */
class RIPEMD160
{
private:
    uint32_t s[5];
    uint8_t buf[64];
    size_t bytes;

public:
    static const size_t OUTPUT_SIZE = 20;

    RIPEMD160();
    RIPEMD160& write(const uint8_t* data, size_t len);
    void finalize(uint8_t hash[OUTPUT_SIZE]);
    RIPEMD160& reset();
};

#endif // BITCOIN_CRYPTO_RIPEMD160_H
