// This file is part of the libroam c++ library
// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2016 The Bitcoin Core developers
// Copyright (c) 2014-2018 The Eccoin developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BITCOIN_HASH_H
#define BITCOIN_HASH_H

#include "hmac_sha512.h"
#include "include/roam_serialize.h"
#include "include/roam_version.h"
#include "ripemd160.h"
#include "sha256.h"
#include "uint256.h"

#include <vector>

/** A hasher class for Bitcoin's 256-bit hash (double SHA-256). */
class DoubleSha256
{
private:
    SHA256 sha;

public:
    static const size_t OUTPUT_SIZE = SHA256::OUTPUT_SIZE;

    void finalize(uint8_t hash[OUTPUT_SIZE])
    {
        uint8_t buf[SHA256::OUTPUT_SIZE];
        sha.finalize(buf);
        sha.reset().write(buf, SHA256::OUTPUT_SIZE).finalize(hash);
    }

    DoubleSha256 &write(const uint8_t *data, size_t len)
    {
        sha.write(data, len);
        return *this;
    }

    DoubleSha256 &reset()
    {
        sha.reset();
        return *this;
    }
};

/** Compute the 256-bit hash of an object. */
template <typename T1>
inline uint256 hash_dbl_sha256(const T1 pbegin, const T1 pend)
{
    static const uint8_t pblank[1] = {};
    uint256 result;
    DoubleSha256()
        .write(pbegin == pend ? pblank : (const uint8_t *)&pbegin[0], (pend - pbegin) * sizeof(pbegin[0]))
        .finalize((uint8_t *)&result);
    return result;
}

/** Compute the 256-bit hash of the concatenation of two objects. */
template <typename T1, typename T2>
inline uint256 hash_dbl_sha256(const T1 p1begin, const T1 p1end, const T2 p2begin, const T2 p2end)
{
    static const uint8_t pblank[1] = {};
    uint256 result;
    DoubleSha256()
        .write(p1begin == p1end ? pblank : (const uint8_t *)&p1begin[0], (p1end - p1begin) * sizeof(p1begin[0]))
        .write(p2begin == p2end ? pblank : (const uint8_t *)&p2begin[0], (p2end - p2begin) * sizeof(p2begin[0]))
        .finalize((uint8_t *)&result);
    return result;
}

/** Compute the 256-bit hash of the concatenation of three objects. */
template <typename T1, typename T2, typename T3>
inline uint256 hash_dbl_sha256(const T1 p1begin,
    const T1 p1end,
    const T2 p2begin,
    const T2 p2end,
    const T3 p3begin,
    const T3 p3end)
{
    static const uint8_t pblank[1] = {};
    uint256 result;
    DoubleSha256()
        .write(p1begin == p1end ? pblank : (const uint8_t *)&p1begin[0], (p1end - p1begin) * sizeof(p1begin[0]))
        .write(p2begin == p2end ? pblank : (const uint8_t *)&p2begin[0], (p2end - p2begin) * sizeof(p2begin[0]))
        .write(p3begin == p3end ? pblank : (const uint8_t *)&p3begin[0], (p3end - p3begin) * sizeof(p3begin[0]))
        .finalize((uint8_t *)&result);
    return result;
}


/** A hasher class for Bitcoin's 160-bit hash (SHA-256 + RIPEMD-160). */
class BitcoinHash160
{
private:
    SHA256 sha;

public:
    static const size_t OUTPUT_SIZE = RIPEMD160::OUTPUT_SIZE;

    void finalize(unsigned char hash[OUTPUT_SIZE])
    {
        unsigned char buf[SHA256::OUTPUT_SIZE];
        sha.finalize(buf);
        RIPEMD160().write(buf, SHA256::OUTPUT_SIZE).finalize(hash);
    }

    BitcoinHash160 &write(const unsigned char *data, size_t len)
    {
        sha.write(data, len);
        return *this;
    }

    BitcoinHash160 &reset()
    {
        sha.reset();
        return *this;
    }
};

/** Compute the 160-bit hash an object. */
template <typename T1>
inline uint160 Hash160(const T1 pbegin, const T1 pend)
{
    static unsigned char pblank[1] = {};
    uint160 result;
    BitcoinHash160()
        .write(pbegin == pend ? pblank : (const unsigned char *)&pbegin[0], (pend - pbegin) * sizeof(pbegin[0]))
        .finalize((unsigned char *)&result);
    return result;
}

/** Compute the 160-bit hash of a vector. */
inline uint160 Hash160(const std::vector<unsigned char> &vch)
{
    return Hash160(vch.begin(), vch.end());
}

/** A writer stream (for serialization) that computes a 256-bit hash. */
class HashWriter
{
private:
    DoubleSha256 ctx;

public:
    int n_type;
    int n_version;

    HashWriter(int nTypeIn, int nVersionIn) : n_type(nTypeIn), n_version(nVersionIn) {}
    HashWriter &write(const char *pch, size_t size)
    {
        ctx.write((const uint8_t *)pch, size);
        return (*this);
    }

    // invalidates the object
    uint256 get_hash()
    {
        uint256 result;
        ctx.finalize((uint8_t *)&result);
        return result;
    }

    template <typename T>
    HashWriter &operator<<(const T &obj)
    {
        // Serialize to this stream
        ::Serialize(*this, obj);
        return (*this);
    }
};

inline void BIP32Hash(const uint256 &chainCode,
    uint32_t nChild,
    uint8_t header,
    const uint8_t data[32],
    uint8_t output[64])
{
    uint8_t num[4];
    num[0] = (nChild >> 24) & 0xFF;
    num[1] = (nChild >> 16) & 0xFF;
    num[2] = (nChild >> 8) & 0xFF;
    num[3] = (nChild >> 0) & 0xFF;
    HMAC_SHA512(chainCode.begin(), chainCode.size()).write(&header, 1).write(data, 32).write(num, 4).finalize(output);
}

#endif // BITCOIN_HASH_H
