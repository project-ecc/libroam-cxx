// Copyright (c) 2015 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.
//
// C++ wrapper around ctaes, a constant-time AES implementation

#ifndef BITCOIN_CRYPTO_AES_H
#define BITCOIN_CRYPTO_AES_H

extern "C" {
#include "ctaes/ctaes.h"
}

static const int AES_BLOCKSIZE = 16;
static const int AES256_KEYSIZE = 32;

/** An encryption class for AES-256. */
class AES256Encrypt
{
private:
    AES256_ctx ctx;

public:
    AES256Encrypt(const uint8_t key[32]);
    ~AES256Encrypt();
    void encrypt(uint8_t ciphertext[16], const uint8_t plaintext[16]) const;
};

/** A decryption class for AES-256. */
class AES256Decrypt
{
private:
    AES256_ctx ctx;

public:
    AES256Decrypt(const uint8_t key[32]);
    ~AES256Decrypt();
    void decrypt(uint8_t plaintext[16], const uint8_t ciphertext[16]) const;
};

class AES256CBCEncrypt
{
public:
    AES256CBCEncrypt(const uint8_t key[AES256_KEYSIZE], const uint8_t ivIn[AES_BLOCKSIZE], bool padIn);
    ~AES256CBCEncrypt();
    int encrypt(const uint8_t* data, int size, uint8_t* out) const;

private:
    const AES256Encrypt enc;
    const bool pad;
    uint8_t iv[AES_BLOCKSIZE];
};

class AES256CBCDecrypt
{
public:
    AES256CBCDecrypt(const uint8_t key[AES256_KEYSIZE], const uint8_t ivIn[AES_BLOCKSIZE], bool padIn);
    ~AES256CBCDecrypt();
    int decrypt(const uint8_t* data, int size, uint8_t* out) const;

private:
    const AES256Decrypt dec;
    const bool pad;
    uint8_t iv[AES_BLOCKSIZE];
};

#endif // BITCOIN_CRYPTO_AES_H
