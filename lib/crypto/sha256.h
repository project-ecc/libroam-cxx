// This file is part of the libroam c++ library
// Copyright (c) 2014-2018 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.


#ifndef BITCOIN_CRYPTO_SHA256_H
#define BITCOIN_CRYPTO_SHA256_H

#include <stdint.h>
#include <stdlib.h>

/** A hasher class for SHA-256. */
class SHA256
{
private:
    uint32_t s[8];
    uint8_t buf[64];
    uint64_t bytes;

public:
    static const size_t OUTPUT_SIZE = 32;

    SHA256();
    SHA256& write(const uint8_t* data, size_t len);
    void finalize(uint8_t hash[OUTPUT_SIZE]);
    SHA256& reset();
};

#endif // BITCOIN_CRYPTO_SHA256_H
