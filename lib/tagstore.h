// This file is part of libroam
// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2016 The Bitcoin Core developers
// Copyright (c) 2014-2019 The Eccoin developers
// Copyright (c) 2019-2022 Greg Griffith
// Copyright (c) 2019-2022 The Roam Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef ROAM_TAGSTORE_H
#define ROAM_TAGSTORE_H

#include "routingtag.h"
#include "tagdb.h"

#include <atomic>
#include <mutex>
#include <set>

// KeyID here is the hash of the tags pubkey
typedef std::map<KeyID, RoutingTag> TagMap;

typedef std::vector<uint8_t, secure_allocator<uint8_t> > KeyingMaterial;

class NetTagStore
{
public:
    mutable std::recursive_mutex cs_TagStore;

    /// mapTags holds either crypted or non crypted tags, depending on fUseCrypto
    TagMap mapTags;
    KeyingMaterial vMasterKey;

    //! if fUseCrypto is true, mapKeys must be empty
    //! if fUseCrypto is false, vMasterKey must be empty
    std::atomic<bool> fUseCrypto;

    //! keeps track of whether Unlock has run a thorough check before
    std::atomic<bool> fDecryptionThoroughlyChecked;

    ////// net tag store
    bool fFileBacked;
    std::string strRoutingDir;
    std::string strRoutingFile;
    std::set<int64_t> setKeyPool;
    typedef std::map<uint32_t, MasterKey> MasterKeyMap;
    MasterKeyMap mapMasterKeys;
    uint32_t nMasterKeyMaxID;
    RoutingTag publicRoutingTag;

private:
    RoutingTagDB *proutingdbEncryption;

protected:
    bool SetCrypted();

    //! will encrypt previously unencrypted keys
    bool EncryptKeys(KeyingMaterial &vMasterKeyIn);

    bool Unlock(const KeyingMaterial &vMasterKeyIn);

    bool AddCryptedTagToTagMap(const RoutingTag &tag);

    bool AddTagToTagMap(const RoutingTag &tag);

public:
    NetTagStore();
    ~NetTagStore();
    void set_null();

    bool IsCrypted() const;
    bool IsLocked() const;
    bool Lock();
    bool HaveTag(const KeyID &address) const;
    bool GetTag(const KeyID &address, RoutingTag &tagOut) const;
    bool GetTagPubKey(const KeyID &address, PubKey &vchPubKeyOut) const;
    void GetTagIds(std::set<KeyID> &setAddress) const;
    bool AddCryptedTag(const RoutingTag &tag);
    bool AddTag(const RoutingTag &tag);
    bool load(const std::string &strDirName, const std::string &strFileName);
    PubKey GetCurrentPublicTagPubKey();
    bool LoadCryptedTag(const RoutingTag &tag);
    //! Adds a key to the store, without saving it to disk (used by LoadWallet)
    bool LoadTag(const RoutingTag &tag);

    RoutingTag GetCurrentPublicTag()
    {
        return publicRoutingTag;
    }
};

#endif // ECCOIN_TAGSTORE_H
