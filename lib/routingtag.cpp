// This file is part of libroam
// Copyright (c) 2019-2022 Greg Griffith
// Copyright (c) 2019-2022 The Roam Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "routingtag.h"

#include "random.h"

#include <secp256k1.h>
#include <secp256k1_recovery.h>

extern secp256k1_context *g_secp256k1_context_sign;

/** These functions are taken from the libsecp256k1 distribution and are very ugly. */
static int ec_privkey_import_der(const secp256k1_context *ctx,
    uint8_t *out32,
    const uint8_t *privkey,
    size_t privkeylen)
{
    const uint8_t *end = privkey + privkeylen;
    int lenb = 0;
    int len = 0;
    memset(out32, 0, 32);
    /* sequence header */
    if (end < privkey + 1 || *privkey != 0x30)
    {
        return 0;
    }
    privkey++;
    /* sequence length constructor */
    if (end < privkey + 1 || !(*privkey & 0x80))
    {
        return 0;
    }
    lenb = *privkey & ~0x80;
    privkey++;
    if (lenb < 1 || lenb > 2)
    {
        return 0;
    }
    if (end < privkey + lenb)
    {
        return 0;
    }
    /* sequence length */
    len = privkey[lenb - 1] | (lenb > 1 ? privkey[lenb - 2] << 8 : 0);
    privkey += lenb;
    if (end < privkey + len)
    {
        return 0;
    }
    /* sequence element 0: version number (=1) */
    if (end < privkey + 3 || privkey[0] != 0x02 || privkey[1] != 0x01 || privkey[2] != 0x01)
    {
        return 0;
    }
    privkey += 3;
    /* sequence element 1: octet string, up to 32 bytes */
    if (end < privkey + 2 || privkey[0] != 0x04 || privkey[1] > 0x20 || end < privkey + 2 + privkey[1])
    {
        return 0;
    }
    memcpy(out32 + 32 - privkey[1], privkey + 2, privkey[1]);
    if (!secp256k1_ec_seckey_verify(ctx, out32))
    {
        memset(out32, 0, 32);
        return 0;
    }
    return 1;
}

PubKey RoutingTag::get_pubkey() const
{
    return vchPubKey;
}
PrivKey RoutingTag::get_privkey() const
{
    return vchPrivKey;
}
bool RoutingTag::is_private() const
{
    return isPrivate;
}
void RoutingTag::convert_to_public_tag()
{
    isPrivate = false;
}
void RoutingTag::make_new_tag()
{
    Key key;
    key.make_new_key();
    vchPubKey = key.get_pubkey();
    vchPrivKey = key.get_privkey();
}

bool RoutingTag::verify_pubkey(const PubKey &pubkey) const
{
    // tag pubkeys are always compressed
    if (pubkey.is_compressed() != true)
    {
        return false;
    }
    uint8_t rnd[8];
    std::string str = "Bitcoin key verification\n";
    get_strong_rand_bytes(rnd, sizeof(rnd));
    uint256 hash;
    DoubleSha256().write((uint8_t *)str.data(), str.size()).write(rnd, sizeof(rnd)).finalize(hash.begin());
    std::vector<uint8_t> vchSig;
    sign(hash, vchSig);
    return pubkey.verify_ECDSA(hash, vchSig);
}

bool RoutingTag::check_if_valid() const
{
    uint8_t vch[32];
    return ec_privkey_import_der(g_secp256k1_context_sign, (uint8_t *)vch, &vchPrivKey[0], vchPrivKey.size());
}

bool RoutingTag::sign(const uint256 &hash, std::vector<uint8_t> &vchSig, uint32_t test_case) const
{
    if (!check_if_valid())
    {
        return false;
    }
    uint8_t vch[32];
    if (!ec_privkey_import_der(g_secp256k1_context_sign, (uint8_t *)vch, &vchPrivKey[0], vchPrivKey.size()))
    {
        return false;
    }
    vchSig.resize(72);
    size_t nSigLen = 72;
    uint8_t extra_entropy[32] = {0};
    WriteLE32(extra_entropy, test_case);
    secp256k1_ecdsa_signature sig;
    int ret = secp256k1_ecdsa_sign(g_secp256k1_context_sign, &sig, hash.begin(), (uint8_t *)&vch,
        secp256k1_nonce_function_rfc6979, test_case ? extra_entropy : NULL);
    assert(ret);
    secp256k1_ecdsa_signature_serialize_der(g_secp256k1_context_sign, (uint8_t *)&vchSig[0], &nSigLen, &sig);
    vchSig.resize(nSigLen);
    return true;
}

bool RoutingTag::sign_compact(const uint256 &hash, std::vector<uint8_t> &vchSig) const
{
    uint8_t vch[32];
    if (!ec_privkey_import_der(g_secp256k1_context_sign, (uint8_t *)vch, &vchPrivKey[0], vchPrivKey.size()))
    {
        return false;
    }
    vchSig.resize(65);
    int rec = -1;
    secp256k1_ecdsa_recoverable_signature sig;
    int ret = secp256k1_ecdsa_sign_recoverable(
        g_secp256k1_context_sign, &sig, hash.begin(), (uint8_t *)&vch, secp256k1_nonce_function_rfc6979, NULL);
    assert(ret);
    secp256k1_ecdsa_recoverable_signature_serialize_compact(
        g_secp256k1_context_sign, (uint8_t *)&vchSig[1], &rec, &sig);
    assert(ret);
    assert(rec != -1);
    vchSig[0] = 27 + rec;
    return true;
}
