// This file is part of libroam
// Copyright (c) 2019-2022 Greg Griffith
// Copyright (c) 2019-2024 The Roam Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef ROAM_DATAPACKET_UTIL_H
#define ROAM_DATAPACKET_UTIL_H

#include "include/roam_datapacket.h"

// serialize a packet header into a vector of bytes
std::vector<uint8_t> packet_header_serialize(const RoamPacketHeader &packet_header);

// serialize a packet into a vector of bytes
std::vector<uint8_t> packet_serialize(const RoamPacket &packet);

// add sender information to a packet, needed if the packet has a signature
void packet_add_sender(RoamPacket& packet, const std::vector<uint8_t> &sender);

// add signature information to a packet, needed if the packet has a sender
void packet_add_signature(RoamPacket& packet, const std::vector<uint8_t> &sig);

// add data to the packet data payload
void packet_add_data(RoamPacket& packet, const std::vector<uint8_t> &data);

// return the header of a packet as a RoamPacketHeader struct
RoamPacketHeader packet_get_header(RoamPacket& packet);

// verify the signature of a packet using the sender and signature information
bool packet_verify_signature(const RoamPacket &packet);

#endif // ROAM_DATAPACKET_UTIL_H
