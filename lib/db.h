// Copyright (c) 2020-2021 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BITCOIN_WALLET_SQLITE_H
#define BITCOIN_WALLET_SQLITE_H

#include "sqlite/sqlite3.h"

#include "datastream.h"

#include <atomic>
#include <mutex>

struct sqlite3_stmt;
struct sqlite3;

class SQLiteDatabase;

enum class DatabaseStatus
{
    SUCCESS,
    FAILED_BAD_PATH,
    FAILED_BAD_FORMAT,
    FAILED_ALREADY_LOADED,
    FAILED_ALREADY_EXISTS,
    FAILED_NOT_FOUND,
    FAILED_CREATE,
    FAILED_LOAD,
    FAILED_VERIFY,
    FAILED_ENCRYPT,
    FAILED_INVALID_BACKUP_FILE,
};

/** RAII class that provides a database cursor */
class SQLiteCursor
{
public:
    sqlite3_stmt* m_cursor_stmt{nullptr};
    // Copies of the prefix things for the prefix cursor.
    // Prevents SQLite from accessing temp variables for the prefix things.
    std::vector<uint8_t> m_prefix_range_start;
    std::vector<uint8_t> m_prefix_range_end;

    explicit SQLiteCursor() {}
    explicit SQLiteCursor(std::vector<uint8_t> start_range, std::vector<uint8_t> end_range)
        : m_prefix_range_start(std::move(start_range)),
        m_prefix_range_end(std::move(end_range))
    {}

    ~SQLiteCursor();

    enum class Status
    {
        FAIL,
        MORE,
        DONE,
    };

    Status Next(DataStream& key, DataStream& value);
};

/** RAII class that provides access to a SQLite3 */
class SQLiteBatch
{
private:
    SQLiteDatabase& m_database;

    sqlite3_stmt* m_read_stmt{nullptr};
    sqlite3_stmt* m_insert_stmt{nullptr};
    sqlite3_stmt* m_overwrite_stmt{nullptr};
    sqlite3_stmt* m_delete_stmt{nullptr};
    sqlite3_stmt* m_delete_prefix_stmt{nullptr};

    void SetupSQLStatements();
    bool ExecStatement(sqlite3_stmt* stmt, const uint8_t* blob, const uint64_t blob_len);

    bool HasKey(DataStream&& key);
    bool ErasePrefix(uint8_t* prefix, const uint64_t prefix_len);

public:
    explicit SQLiteBatch(SQLiteDatabase& database);
    ~SQLiteBatch()
    {
        Close();
    }

    void Close();

    std::unique_ptr<SQLiteCursor> GetNewCursor();
    std::unique_ptr<SQLiteCursor> GetNewPrefixCursor(uint8_t* prefix, const uint64_t prefix_len);
    bool TxnBegin();
    bool TxnCommit();
    bool TxnAbort();

    bool read_key(const DataStream& key, DataStream& value);
    bool write_key(const DataStream& key, const DataStream& value, bool overwrite = true);
    bool erase_key(const DataStream& key);
};

/** An instance of this class represents one SQLite3 database.
 **/
class SQLiteDatabase
{
private:
    std::atomic<unsigned int> nUpdateCounter;

    const std::string m_dir_path;
    const std::string m_file_path;
    const std::string m_full_path;

    /**
     * This mutex protects SQLite initialization and shutdown.
     * sqlite3_config() and sqlite3_shutdown() are not thread-safe (sqlite3_initialize() is).
     * Concurrent threads that execute SQLiteDatabase::SQLiteDatabase() should have just one
     * of them do the init and the rest wait for it to complete before all can proceed.
     */
    static std::mutex g_sqlite_mutex;
    static int g_sqlite_count;

    void Cleanup() noexcept;

public:
    sqlite3* m_db;

public:
    SQLiteDatabase() = delete;

    // Create DB handle to real database
    SQLiteDatabase(const std::string& dir_path, const std::string& file_path);

    ~SQLiteDatabase();

    bool Verify(std::string& error);

    // Open the database if it is not already opened
    void Open();

    // Close the database
    void Close();

    // Rewrite the entire database on disk
    bool Rewrite(const char* skip = nullptr);

    // Back up the entire database to a file.
    bool Backup(const std::string& dest) const;

    void IncrementUpdateCounter()
    {
        ++nUpdateCounter;
    }

    std::string Filename()
    {
        return m_file_path;
    }

    std::string Format()
    {
        return "sqlite";
    }

    // Make a SQLiteBatch connected to this database
    std::unique_ptr<SQLiteBatch> MakeBatch();
};

std::string SQLiteDatabaseVersion();

#endif // BITCOIN_WALLET_SQLITE_H
