// This file is part of libroam
// Copyright (c) 2019-2022 Greg Griffith
// Copyright (c) 2019-2022 The Roam Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef ROAM_AODV_H
#define ROAM_AODV_H

#include <map>
#include <set>
#include <shared_mutex>
#include <utility>
#include <vector>

#include "aodv_messages.h"
#include "util.h"

// read https://www.ietf.org/rfc/rfc3561.txt for details on aodv
// the difference with this system is instead of trying to find
// and ip address, we are trying to find a peer with a specific
// pubkey

// struct RREQRESPONSE has been moved to its own header for easier inclusion in roam.h

class AodvRouteTable
{
private:
    // a reverse lookup table for map_routes_by_pubkey
    std::map<NodeId, std::set<PubKey> > map_routes_by_peer_id;

    // a map of what pubkeys have are known by which nodes
    // when we want to send a message to a pubkey, this map tells us which peer
    // to route it through (peer identified by its ID)
    std::map<PubKey, std::set<NodeId> > map_routes_by_pubkey;

    // nonce, GetTimeMillis
    std::map<uint64_t, uint64_t> map_request_time;
    // nonce, nodeId
    std::map<uint64_t, PubKey> map_request_source;

    // holds information about peers that are directly connected
    std::map<NodeId, PubKey> map_direct_peers;

public:
    mutable std::shared_mutex cs_aodv;

private:
    bool _have_key_route(const PubKey &key);
    bool have_key_route(const PubKey &key);

    bool _have_id_route(const NodeId &node);
    bool have_id_route(const NodeId &node);

    void _get_id_key_table(std::map<NodeId, std::set<PubKey> > &IdKey);
    void _get_key_id_table(std::map<PubKey, std::set<NodeId> > &KeyId);

public:
    AodvRouteTable()
    {
        map_routes_by_peer_id.clear();
        map_routes_by_pubkey.clear();
        map_request_time.clear();
        map_request_source.clear();
        map_direct_peers.clear();
    }
    bool have_route(const PubKey &key);
    bool have_route(const NodeId &node);
    void get_id_key_table(std::map<NodeId, std::set<PubKey> > &IdKey);
    void get_key_id_table(std::map<PubKey, std::set<NodeId> > &KeyId);
    void get_routing_tables(std::map<NodeId, std::set<PubKey> > &IdKey,
        std::map<PubKey, std::set<NodeId> > &KeyId,
        std::map<NodeId, PubKey> &directConnect);

    void add_route(const PubKey &key, const NodeId &node, bool directConnect = false);
    void remove_routes_for_peer(const NodeId &node);
    bool get_key_node(const PubKey &key, NodeId &result);
    void add_new_request_time_data(const uint64_t &nonce);
    bool is_old_request(const uint64_t &nonce);
    void add_new_request_source(const uint64_t &nonce, const PubKey &nodeId);
    bool get_request_source(const uint64_t &nonce, PubKey &source);
};

/*
void RequestRouteToPeer(CConnman &connman, const PubKey &source, const uint64_t &nonce, const PubKey &searchKey);
void RequestRouteToPeer(CConnman *connman, const PubKey &source, const uint64_t &nonce, const PubKey &searchKey);
*/

#endif
