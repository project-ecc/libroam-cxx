// This file is part of the libroam c++ library
// Copyright (c) 2014-2016 The Bitcoin Core developers
// Copyright (c) 2014-2018 The Eccoin developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "base58.h"
#include "crypto/hash.h"
#include "uint256.h"

/** All alphanumeric characters except for "0", "I", "O", and "l" */
static const char *pszBase58 = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";

bool DecodeBase58(const char *psz, std::vector<uint8_t> &vch)
{
    // Skip leading spaces.
    while (*psz && isspace(*psz))
        psz++;
    // Skip and count leading '1's.
    int zeroes = 0;
    while (*psz == '1')
    {
        zeroes++;
        psz++;
    }
    // Allocate enough space in big-endian base256 representation.
    std::vector<uint8_t> b256(strlen(psz) * 733 / 1000 + 1); // log(58) / log(256), rounded up.
    // Process the characters.
    while (*psz && !isspace(*psz))
    {
        // Decode base58 character
        const char *ch = strchr(pszBase58, *psz);
        if (ch == NULL)
            return false;
        // Apply "b256 = b256 * 58 + ch".
        int carry = ch - pszBase58;
        for (std::vector<uint8_t>::reverse_iterator it = b256.rbegin(); it != b256.rend(); it++)
        {
            carry += 58 * (*it);
            *it = carry % 256;
            carry /= 256;
        }
        if (carry != 0)
        {
            return false;
        }
        psz++;
    }
    // Skip trailing spaces.
    while (isspace(*psz))
        psz++;
    if (*psz != 0)
        return false;
    // Skip leading zeroes in b256.
    std::vector<uint8_t>::iterator it = b256.begin();
    while (it != b256.end() && *it == 0)
        it++;
    // Copy result into output vector.
    vch.reserve(zeroes + (b256.end() - it));
    vch.assign(zeroes, 0x00);
    while (it != b256.end())
        vch.push_back(*(it++));
    return true;
}

std::string EncodeBase58(const uint8_t *pbegin, const uint8_t *pend)
{
    // Skip & count leading zeroes.
    int zeroes = 0;
    while (pbegin != pend && *pbegin == 0)
    {
        pbegin++;
        zeroes++;
    }
    // Allocate enough space in big-endian base58 representation.
    std::vector<uint8_t> b58((pend - pbegin) * 138 / 100 + 1); // log(256) / log(58), rounded up.
    // Process the bytes.
    while (pbegin != pend)
    {
        int carry = *pbegin;
        // Apply "b58 = b58 * 256 + ch".
        for (std::vector<uint8_t>::reverse_iterator it = b58.rbegin(); it != b58.rend(); it++)
        {
            carry += 256 * (*it);
            *it = carry % 58;
            carry /= 58;
        }
        if (carry != 0)
        {
            return "";
        }
        pbegin++;
    }
    // Skip leading zeroes in base58 result.
    std::vector<uint8_t>::iterator it = b58.begin();
    while (it != b58.end() && *it == 0)
        it++;
    // Translate the result into a string.
    std::string str;
    str.reserve(zeroes + (b58.end() - it));
    str.assign(zeroes, '1');
    while (it != b58.end())
        str += pszBase58[*(it++)];
    return str;
}

std::string EncodeBase58(const std::vector<uint8_t> &vch)
{
    return EncodeBase58(&vch[0], &vch[0] + vch.size());
}
bool DecodeBase58(const std::string &str, std::vector<uint8_t> &vchRet)
{
    return DecodeBase58(str.c_str(), vchRet);
}

std::string encode_base58_check(const std::vector<uint8_t> &vchIn)
{
    // add 4-byte hash check to the end
    std::vector<uint8_t> vch(vchIn);
    uint256 hash = hash_dbl_sha256(vch.begin(), vch.end());
    vch.insert(vch.end(), (uint8_t *)&hash, (uint8_t *)&hash + 4);
    return EncodeBase58(vch);
}

bool decode_base58_check(const char *psz, std::vector<uint8_t> &vchRet)
{
    if (!DecodeBase58(psz, vchRet) || (vchRet.size() < 4))
    {
        vchRet.clear();
        return false;
    }
    // re-calculate the checksum, insure it matches the included 4-byte checksum
    uint256 hash = hash_dbl_sha256(vchRet.begin(), vchRet.end() - 4);
    if (memcmp(&hash, &vchRet.end()[-4], 4) != 0)
    {
        vchRet.clear();
        return false;
    }
    vchRet.resize(vchRet.size() - 4);
    return true;
}

bool decode_base58_check(const std::string &str, std::vector<uint8_t> &vchRet)
{
    return decode_base58_check(str.c_str(), vchRet);
}
