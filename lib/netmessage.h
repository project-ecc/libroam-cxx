// This file is part of libroam
// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2016 The Bitcoin Core developers
// Copyright (c) 2014-2020 The Eccoin developers
// Copyright (c) 2022-2023 The Roam developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef ROAM_NETMESSAGE_H
#define ROAM_NETMESSAGE_H

#include "datastream.h"
#include "packetmanager.h"
#include "pubkey.h"

#include <array>

static const uint16_t RREQ = 19;
static const uint16_t RREP = 20;
static const uint16_t RERR = 21;
static const uint16_t SENDPACKET = 22;

/**
 * Message header.
 * (4) message start.
 * (2) command.
 * (4) size.
 */
class MessageHeader
{
public:
    static const uint32_t MESSAGE_START_SIZE = 4;
    static const uint32_t COMMAND_SIZE = 2;
    static const uint32_t MESSAGE_SIZE_SIZE = 4;
    static const uint32_t MESSAGE_SIZE_OFFSET = MESSAGE_START_SIZE + COMMAND_SIZE;
    static const uint32_t HEADER_SIZE = MESSAGE_START_SIZE + COMMAND_SIZE + MESSAGE_SIZE_SIZE;

    typedef std::array<uint8_t, MESSAGE_START_SIZE> MessageMagic;

    static constexpr MessageMagic ROAM_NET_MAGIC = {0xce, 0xf1, 0xdb, 0xfa};

    MessageMagic array_message_start;
    uint16_t n_command;
    uint32_t n_message_size;

    MessageHeader(const MessageMagic &array_message_start_in)
    {
        std::memcpy(std::begin(array_message_start), std::begin(array_message_start_in), MESSAGE_START_SIZE);
        n_command = 0;
        n_message_size = -1;
    }

    MessageHeader(const MessageMagic &array_message_start_in, const uint16_t n_command, uint32_t n_message_sizeIn)
    {
        std::memcpy(std::begin(array_message_start), std::begin(array_message_start_in), MESSAGE_START_SIZE);
        this->n_command = n_command;
        n_message_size = n_message_sizeIn;
    }

    uint16_t get_command() const
    {
        return n_command;
    }

    bool is_valid(const MessageMagic &array_message_start_in) const
    {
        // Check start string
        if (memcmp(std::begin(array_message_start), std::begin(array_message_start_in), MESSAGE_START_SIZE) != 0)
        {
            return false;
        }

        // Check the command string for errors
        if (n_command != RREQ && n_command != RREP && n_command != RERR && n_command != SENDPACKET)
        {
            return false;
        }

        // Message size
        if (n_message_size > MAX_SIZE)
        {
            //LogPrintf("MessageHeader::is_valid(): (%s, %u bytes) n_message_size > "
            //          "MAX_SIZE\n",
            //    get_command(), n_message_size);
            return false;
        }

        return true;
    }

    ADD_SERIALIZE_METHODS;

    template <typename Stream, typename Operation>
    inline void SerializationOp(Stream &s, Operation ser_action)
    {
        READWRITE(FLATDATA(array_message_start));
        READWRITE(n_command);
        READWRITE(n_message_size);
    }
};

struct NetMessage
{
    // Parsing header (false) or data (true)
    bool in_data;

    // Partially received header.
    DataStream hdr_buffer;

    // Complete header.
    MessageHeader hdr;
    uint32_t n_hdr_pos;

    // Received message data.
    DataStream recv_stream;
    uint32_t n_data_pos;

    // Time (in microseconds) of message receipt.
    int64_t n_time;

    // default constructor builds an empty message object to accept assignment of real messages
    NetMessage() : hdr_buffer(0, 0), hdr({0, 0, 0, 0}), recv_stream(0, 0)
    {
        hdr_buffer.resize(MessageHeader::HEADER_SIZE);
        in_data = false;
        n_hdr_pos = 0;
        n_data_pos = 0;
        n_time = 0;
    }

    NetMessage(const MessageHeader::MessageMagic &array_message_start_in, int nTypeIn, int nVersionIn)
        : hdr_buffer(nTypeIn, nVersionIn), hdr(array_message_start_in), recv_stream(nTypeIn, nVersionIn)
    {
        hdr_buffer.resize(MessageHeader::HEADER_SIZE);
        in_data = false;
        n_hdr_pos = 0;
        n_data_pos = 0;
        n_time = 0;
    }

    bool complete() const
    {
        if (!in_data)
        {
            return false;
        }

        return (hdr.n_message_size == n_data_pos);
    }

    void set_version(int nVersionIn)
    {
        hdr_buffer.set_version(nVersionIn);
        recv_stream.set_version(nVersionIn);
    }

    int read_header(const uint8_t *pch, uint32_t nBytes)
    {
        // copy data to temporary parsing buffer
        uint32_t nRemaining = MessageHeader::HEADER_SIZE - n_hdr_pos;
        uint32_t nCopy = std::min(nRemaining, nBytes);
        std::memcpy(&hdr_buffer[n_hdr_pos], pch, nCopy);
        n_hdr_pos += nCopy;
        // if header incomplete, exit
        if (n_hdr_pos < 24)
        {
            return nCopy;
        }
        // deserialize to MessageHeader
        try
        {
            hdr_buffer >> hdr;
        }
        catch (const std::exception &)
        {
            return -1;
        }
        // reject messages larger than MAX_SIZE
        if (hdr.n_message_size > MAX_SIZE)
        {
            return -1;
        }
        // switch state to reading message data
        in_data = true;
        return nCopy;
    }

    int read_data(const uint8_t *pch, uint32_t nBytes)
    {
        uint32_t nRemaining = hdr.n_message_size - n_data_pos;
        uint32_t nCopy = std::min(nRemaining, nBytes);
        if (recv_stream.size() < n_data_pos + nCopy)
        {
            // Allocate up to 256 KiB ahead, but never more than the total message
            // size.
            recv_stream.resize(std::min(hdr.n_message_size, n_data_pos + nCopy + 256 * 1024));
        }
        std::memcpy(&recv_stream[n_data_pos], pch, nCopy);
        n_data_pos += nCopy;
        return nCopy;
    }
};

bool process_packet(PacketManager* pacman, const PubKey &searchKey, RoamPacket &newPacket);
bool process_net_message(PacketManager* pacman, NetMessage &message);
bool process_net_message(PacketManager* pacman, const uint8_t* data, uint64_t len_data);

#endif // ROAM_NETMESSAGE_H
