// This file is part of libroam
// Copyright (c) 2019-2022 Greg Griffith
// Copyright (c) 2019-2024 The Roam Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "include/roam.h"

#include "packetmanager.h"

#include <secp256k1.h>

extern PacketManager* g_manager;
extern secp256k1_context *g_secp256k1_context_sign;

ROAM_API uint16_t roam_get_mainnet_port()
{
    // ports are 16 bit unsigned ints
    return 47710;
}

ROAM_API uint16_t roam_get_testnet_port()
{
    // ports are 16 bit unsigned ints
    return 47810;
}

// packet manager uses a sqlite database to store information across application restarts
// strDirName is the directory in which the packet manager database is located
// strFileName is the name of the file in that directory
ROAM_API void roam_initalise(const char* c_str_dir_name, const uint64_t c_str_dir_name_len,
    const char* c_str_file_name, const uint64_t c_str_file_name_len)
{
    // only allow one instance of libroam
    if (g_manager != nullptr)
    {
        return;
    }
    ecc_start();
    std::string str_dir_name(c_str_dir_name, c_str_dir_name_len);
    std::string str_file_name(c_str_file_name, c_str_file_name_len);
    g_manager = new PacketManager(str_dir_name, str_file_name);
    // sanity check must come AFTER packet manager is made because the packet manager
    // makes and owns the verify context
    ecc_init_sanity_check();
}

ROAM_API void roam_shutdown()
{
    // deletes the held object
    delete g_manager;
    ecc_stop();
    g_manager = nullptr;
}


// the data is expected to be raw binary data from the network
ROAM_API bool roam_process_packet(const uint8_t* data, const uint64_t len_data)
{
    DataStream ds(data, data+len_data, SER_NETWORK, ROAM_GENERAL_VERSION);
    RoamPacket packet;
    ds >> packet;
    return g_manager->process_packet(packet);
}

// A backup for roam_process_packet that passes in a packet by fields
ROAM_API bool roam_process_packet_from_data(uint8_t packet_version,
    uint16_t dest_buffer_id,
    uint16_t source_buffer_id,
    uint64_t packet_total_length,
    uint8_t* packet_checksum,
    uint64_t packet_checksum_len,
    uint64_t salt,
    uint8_t* sender_pubkey,
    uint64_t sender_pubkey_len,
    uint8_t* sender_signature,
    uint64_t sender_signature_len,
    uint8_t* packet_data,
    uint64_t packet_data_len)
{
    RoamPacket packet;
    packet.n_packet_version = packet_version;
    packet.n_dest_buffer_id = dest_buffer_id;
    packet.n_source_buffer_id = source_buffer_id;
    packet.n_total_length = packet_total_length;
    // require packet checkum len to be 32 for now (sha256 hash size)
    if (packet_checksum_len != 32)
    {
        return false;
    }
    packet.n_checksum.reserve(packet_checksum_len);
    std::copy(packet_checksum, packet_checksum + packet_checksum_len, packet.n_checksum.begin());
    packet.n_salt = salt;
    packet.v_sender = std::vector<uint8_t>(sender_pubkey, sender_pubkey + sender_pubkey_len);
    packet.v_signature = std::vector<uint8_t>(sender_signature, sender_signature + sender_signature_len);
    packet.v_data = std::vector<uint8_t>(packet_data, packet_data + packet_data_len);
    return g_manager->process_packet(packet);
}

// the following description has been replaced by a redesign to build roam into applications directly
// rather than running as system service. It is considered in the future to write a system service version as well

// the use of a pupkey with buffers is analagous to a descriptor to a socket on the OS
// only the process holding the pubkey(descriptior) can access the data
// manually calling roam_read_messages and getbufferkey should eventually be replaced by
// a better signal system such as ZMQ (similar to the event queue at the OS level). this may require the messages to be encrypted so that
// only the desired listening application may decipher the data which of course adds some overhead
// but is probably better for security.
// afterthought - might be best to open a pipe/socket (or other IPC?) on a random port to the registering application instead of zmq as mentioned above

ROAM_API uint16_t roam_register_new_application(const bool &bool_reject,
    char* res_c_str_pubkey,
    const uint64_t res_c_str_pubkey_max_len)
{
    std::string str_pubkey = "";
    // getting a buffer 0 result is a failure
    uint16_t buffer_id = g_manager->register_buffer(str_pubkey, bool_reject);
    if (buffer_id != 0)
    {
        if (res_c_str_pubkey_max_len > (str_pubkey.size() + 1))
        {
            const char* ret_c_str_pubkey = str_pubkey.c_str();
            std::memcpy(res_c_str_pubkey, ret_c_str_pubkey, strlen(ret_c_str_pubkey));
        }
    }
    return buffer_id;
}

ROAM_API bool roam_release_registered_application(const uint16_t &n_protocol_id)
{
    return g_manager->release_buffer(n_protocol_id);
}


//bool isPrivate;
// tags are compressed
//PubKey vchPubKey;
//PrivKey vchPrivKey;


ROAM_API bool roam_add_tag(const uint8_t* data, const uint64_t len_data)
{
    DataStream ds(data, data+len_data, SER_NETWORK, ROAM_GENERAL_VERSION);
    RoutingTag new_tag;
    ds >> new_tag;
    return g_manager->get_tag_store()->AddTag(new_tag);
}


ROAM_API bool roam_add_tag_from_data(bool bool_is_private,
    const uint8_t* pubkey_data,
    const uint64_t pubkey_data_len,
    const uint8_t* privkey_data,
    const uint64_t privkey_data_len)
{
    PubKey pubkey(pubkey_data, pubkey_data+pubkey_data_len);
    PrivKey privkey(privkey_data, privkey_data + privkey_data_len);
    RoutingTag new_tag(bool_is_private, pubkey, privkey);
    return g_manager->get_tag_store()->AddTag(new_tag);
}

// always len 20
ROAM_API bool roam_have_tag(const uint8_t* data)
{
    uint160 data160(std::vector<uint8_t>(data, data+20));
    KeyID tag_id(data160);
    return g_manager->get_tag_store()->HaveTag(tag_id);
}

// Note - maxDataLen needs to be divisible by 2
ROAM_API uint64_t roam_get_buffer_ids_with_new_data(uint8_t* data, uint64_t max_data_len)
{
    std::vector<uint16_t> new_data = g_manager->get_ids_with_new_data();
    printf("roam_get_buffer_ids_with_new_data(): found %lu buffers with new data \n", new_data.size());
    if (new_data.size() > max_data_len / 2)
    {
        // not enough space to return all IDs
        printf("roam_get_buffer_ids_with_new_data() returning false\n");
        return false;
    }
    // the caller will have to parse this data into uint16_t array from a byte array
    std::memcpy(data, new_data.data(), new_data.size() + sizeof(uint16_t));
    return new_data.size();
}

ROAM_API int64_t roam_create_message(const uint8_t* pubkey, const uint64_t len_pubkey, const uint16_t &n_protocol_id,
    const uint8_t* data, const uint64_t len_data, uint8_t* ret, const uint64_t max_len_ret)
{
    std::vector<uint8_t> v_pubkey(pubkey, pubkey + len_pubkey);
    std::vector<uint8_t> v_data(data, data + len_data);
    std::vector<uint8_t> _ret;
    int64_t res = g_manager->create_packet(v_pubkey, n_protocol_id, v_data, _ret);
    if (res > max_len_ret)
    {
        res = max_len_ret;
    }
    std::copy(_ret.begin(), _ret.end(), ret);
    return res;
}

ROAM_API int64_t roam_create_message_sig(const uint8_t* pubkey, uint64_t len_pubkey, const uint16_t &n_protocol_id,
    const uint8_t* data, uint64_t len_data, const char* c_str_sender_pubkey, uint64_t len_sender_pubkey,
    const uint8_t* signature, uint64_t len_signature, uint8_t* ret, const uint64_t max_len_ret)
{
    std::vector<uint8_t> v_pubkey(pubkey, pubkey + len_pubkey);
    std::vector<uint8_t> v_data(data, data + len_data);
    std::string str_sender_pubkey(c_str_sender_pubkey, c_str_sender_pubkey + len_sender_pubkey);
    std::vector<uint8_t> v_signature(signature, signature + len_signature);
    std::vector<uint8_t> _ret;
    int64_t res = g_manager->create_packet(v_pubkey, n_protocol_id, v_data, str_sender_pubkey, v_signature, _ret);
    if (res > max_len_ret)
    {
        res = max_len_ret;
    }
    std::copy(_ret.begin(), _ret.end(), ret);
    return res;
}

ROAM_API int64_t roam_read_messages(const uint16_t &n_protocol_id, uint8_t* ret, const uint64_t len_ret)
{
    std::vector<RoamPacket> v_buffer_data;
    if (!g_manager->get_buffer(n_protocol_id, v_buffer_data))
    {
        return -1;
    }
    DataStream ds(SER_NETWORK, ROAM_GENERAL_VERSION);
    ds << v_buffer_data;
    const uint64_t res_len = ds.size();
    if (res_len > len_ret)
    {
        // TODO - GetBuffer clears data, if the len_ret is too small data will be lost,
        // need to reimplement GetBuffer
        return -1;
    }
    std::memcpy(ret, ds.data(), ds.size());
    return res_len;
}

ROAM_API void roam_record_request_origin(const uint64_t &nonce, const uint8_t* source_pubkey_bytes, const uint64_t &len_pubkey)
{
    PubKey source(source_pubkey_bytes, source_pubkey_bytes + len_pubkey);
    g_manager->record_request_origin(nonce, source);
}

ROAM_API uint64_t roam_get_request_origin(const uint64_t &nonce, uint8_t* source_pubkey_bytes, const uint64_t &max_len_pubkey)
{
    PubKey source;
    if (!g_manager->get_request_origin(nonce, source))
    {
        return 0;
    }
    if (source.size() > max_len_pubkey)
    {
        return 0;
    }
    std::copy(source.begin(), source.end(), source_pubkey_bytes);
    return (uint64_t)source.size();
}

ROAM_API void roam_record_route_to_peer(const uint8_t* search_pubkey_bytes, const uint64_t &len_pubkey, const int64_t &node)
{
    PubKey search_key(search_pubkey_bytes, search_pubkey_bytes + len_pubkey);
    g_manager->record_route_to_peer(search_key, node);
}

ROAM_API bool roam_have_route_to_peer(const uint8_t* search_pubkey_bytes, const uint64_t &len_pubkey)
{
    PubKey search_key(search_pubkey_bytes, search_pubkey_bytes + len_pubkey);
    return g_manager->have_route(search_key);
}

ROAM_API bool roam_get_route_to_peer(const uint8_t* search_pubkey_bytes, const uint64_t &len_pubkey, int64_t &node_id)
{
    PubKey search_key(search_pubkey_bytes, search_pubkey_bytes + len_pubkey);
    return g_manager->get_key_node(search_key, node_id);
}

ROAM_API uint64_t roam_get_public_tag_hex(uint8_t* ret, const uint64_t &max_len_ret)
{
    std::string public_tag_hex_str = g_manager->get_tag_store()->GetCurrentPublicTagPubKey().get_hex();
    const uint64_t public_tag_hex_size = (public_tag_hex_str.size() + 1);
    if (max_len_ret < public_tag_hex_size)
    {
        return 0;
    }
    const char* pub_tag_hex_c_str = public_tag_hex_str.c_str();
    std::memcpy(ret, pub_tag_hex_c_str, public_tag_hex_size);
    return public_tag_hex_size;
}

ROAM_API uint64_t roam_sign_with_public_tag(const uint8_t* message, const uint64_t &len_message, uint8_t* ret, const uint64_t &max_len_ret)
{
    RoutingTag public_tag = g_manager->get_tag_store()->GetCurrentPublicTag();

    std::string str_message(message, message + len_message);
    HashWriter ss(SER_GETHASH, 0);
    ss << ROAM_MESSAGE_MAGIC;
    ss << str_message;
    std::vector<uint8_t> v_sig;
    if (!public_tag.sign_compact(ss.get_hash(), v_sig))
    {
        return 0;
    }
    if (max_len_ret < v_sig.size())
    {
        return 0;
    }
    std::copy(v_sig.begin(), v_sig.end(), ret);
    return v_sig.size();
}

ROAM_API bool roam_is_old_request(const uint64_t nonce)
{
    return g_manager->is_old_request(nonce);
}

ROAM_API void roam_add_new_request_time_data(const uint64_t nonce)
{
    g_manager->add_new_request_time_data(nonce);
}
