// Copyright (c) 2020-2022 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "db.h"

#include "crypto/common.h"
#include "netmessage.h"

#include <filesystem>
#include <optional>
#include <stdint.h>
#include <utility>
#include <vector>

static constexpr int32_t WALLET_SCHEMA_VERSION = 0;

static void ErrorLogCallback(void* arg, int code, const char* msg)
{
    // From sqlite3_config() documentation for the SQLITE_CONFIG_LOG option:
    // "The void pointer that is the second argument to SQLITE_CONFIG_LOG is passed through as
    // the first parameter to the application-defined logger function whenever that function is
    // invoked."
    // Assert that this is the case:
    assert(arg == nullptr);
    printf("SQLite Error. Code: %d. Message: %s\n", code, msg);
}

static int TraceSqlCallback(unsigned code, void* context, void* param1, void* param2)
{
    auto* db = static_cast<SQLiteDatabase*>(context);
    if (code == SQLITE_TRACE_STMT)
    {
        auto* stmt = static_cast<sqlite3_stmt*>(param1);
        // To be conservative and avoid leaking potentially secret information
        // in the log file, only expand statements that query the database, not
        // statements that update the database.
        char* expanded{sqlite3_stmt_readonly(stmt) ? sqlite3_expanded_sql(stmt) : nullptr};
        printf("[%s] SQLite Statement: %s\n", db->Filename().c_str(), expanded ? expanded : sqlite3_sql(stmt));
        if (expanded)
        {
            sqlite3_free(expanded);
        }
    }
    return SQLITE_OK;
}

static bool BindBlobToStatement(sqlite3_stmt* stmt, int index, const uint8_t* blob, const uint64_t blob_len, const std::string& description)
{
    // Pass a pointer to the empty string "" below instead of passing the
    // blob.data() pointer if the blob.data() pointer is null. Passing a null
    // data pointer to bind_blob would cause sqlite to bind the SQL NULL value
    // instead of the empty blob value X'', which would mess up SQL comparisons.
    int res;
    if (blob == nullptr || blob_len == 0)
    {
        int res = sqlite3_bind_blob(stmt, index, "", 0, SQLITE_STATIC);
    }
    else
    {
        int res = sqlite3_bind_blob(stmt, index, blob, blob_len, SQLITE_STATIC);
    }
    if (res != SQLITE_OK)
    {
        printf("Unable to bind %s to statement: %s\n", description.c_str(), sqlite3_errstr(res));
        sqlite3_clear_bindings(stmt);
        sqlite3_reset(stmt);
        return false;
    }

    return true;
}

static std::optional<int> ReadPragmaInteger(sqlite3* db, const std::string& key, const std::string& description, std::string& error)
{
    std::string stmt_text = "PRAGMA " + key;
    sqlite3_stmt* pragma_read_stmt{nullptr};
    int ret = sqlite3_prepare_v2(db, stmt_text.c_str(), -1, &pragma_read_stmt, nullptr);
    if (ret != SQLITE_OK)
    {
        sqlite3_finalize(pragma_read_stmt);
        error = "SQLiteDatabase: Failed to prepare the statement to fetch " + description + ": " + std::string(sqlite3_errstr(ret));
        return std::nullopt;
    }
    ret = sqlite3_step(pragma_read_stmt);
    if (ret != SQLITE_ROW)
    {
        sqlite3_finalize(pragma_read_stmt);
        error = "SQLiteDatabase: Failed to fetch " + description + ": " + std::string(sqlite3_errstr(ret));
        return std::nullopt;
    }
    int result = sqlite3_column_int(pragma_read_stmt, 0);
    sqlite3_finalize(pragma_read_stmt);
    return result;
}

static void SetPragma(sqlite3* db, const std::string& key, const std::string& value, const std::string& err_msg)
{
    std::string stmt_text = "PRAGMA " + key +" = " + value;
    int ret = sqlite3_exec(db, stmt_text.c_str(), nullptr, nullptr, nullptr);
    if (ret != SQLITE_OK)
    {
        throw std::runtime_error("SQLiteDatabase: " + err_msg + ": " + std::string(sqlite3_errstr(ret)) + "\n");
    }
}

std::mutex SQLiteDatabase::g_sqlite_mutex;
int SQLiteDatabase::g_sqlite_count = 0;

SQLiteDatabase::SQLiteDatabase(const std::string& dir_path, const std::string& file_path)
    : m_dir_path(dir_path), m_file_path(file_path), m_full_path(dir_path + "/" + file_path)
{
    m_db = nullptr;
    nUpdateCounter = 0;
    {
        std::lock_guard<std::mutex> init_guard(g_sqlite_mutex);
        printf("Using SQLite Version %s\n", SQLiteDatabaseVersion().c_str());
        printf("Using database %s\n", m_dir_path.c_str());

        if (++g_sqlite_count == 1)
        {
            // Setup logging
            int ret = sqlite3_config(SQLITE_CONFIG_LOG, ErrorLogCallback, nullptr);
            if (ret != SQLITE_OK)
            {
                throw std::runtime_error("SQLiteDatabase: Failed to setup error log: " + std::string(sqlite3_errstr(ret)) + "\n");
            }
            // Force serialized threading mode
            ret = sqlite3_config(SQLITE_CONFIG_SERIALIZED);
            if (ret != SQLITE_OK)
            {
                throw std::runtime_error("SQLiteDatabase: Failed to configure serialized threading mode: " + std::string(sqlite3_errstr(ret)) + "\n");
            }
        }
        int ret = sqlite3_initialize(); // This is a no-op if sqlite3 is already initialized
        if (ret != SQLITE_OK)
        {
            throw std::runtime_error("SQLiteDatabase: Failed to initialize SQLite: " + std::string(sqlite3_errstr(ret)) +"\n");
        }
    }
    try
    {
        Open();
    }
    catch (const std::runtime_error&)
    {
        // If open fails, cleanup this object and rethrow the exception
        Cleanup();
        throw;
    }
}

void SQLiteBatch::SetupSQLStatements()
{
    const std::vector<std::pair<sqlite3_stmt**, const char*>> statements{
        {&m_read_stmt, "SELECT value FROM main WHERE key = ?"},
        {&m_insert_stmt, "INSERT INTO main VALUES(?, ?)"},
        {&m_overwrite_stmt, "INSERT or REPLACE into main values(?, ?)"},
        {&m_delete_stmt, "DELETE FROM main WHERE key = ?"},
        {&m_delete_prefix_stmt, "DELETE FROM main WHERE instr(key, ?) = 1"},
    };

    for (const auto& [stmt_prepared, stmt_text] : statements)
    {
        if (*stmt_prepared == nullptr)
        {
            int res = sqlite3_prepare_v2(m_database.m_db, stmt_text, -1, stmt_prepared, nullptr);
            if (res != SQLITE_OK)
            {
                throw std::runtime_error("SQLiteDatabase: Failed to setup SQL statements: " + std::string(sqlite3_errstr(res)) + "\n");
            }
        }
    }
}

SQLiteDatabase::~SQLiteDatabase()
{
    Cleanup();
}

void SQLiteDatabase::Cleanup() noexcept
{
    Close();

    std::lock_guard<std::mutex> cleanup_guard(g_sqlite_mutex);
    if (--g_sqlite_count == 0)
    {
        int ret = sqlite3_shutdown();
        if (ret != SQLITE_OK)
        {
            printf("SQLiteDatabase: Failed to shutdown SQLite: %s\n", sqlite3_errstr(ret));
        }
    }
}

bool SQLiteDatabase::Verify(std::string& error)
{
    assert(m_db);

    // Check the application ID matches our network magic
    auto read_result = ReadPragmaInteger(m_db, "application_id", "the application id", error);
    if (!read_result.has_value()) return false;
    uint32_t app_id = static_cast<uint32_t>(read_result.value());
    uint32_t net_magic = ReadBE32(MessageHeader::ROAM_NET_MAGIC.data());
    if (app_id != net_magic)
    {
        error = "SQLiteDatabase: Unexpected application id. Expected " + std::to_string(net_magic) + ", got " + std::to_string(app_id);
        return false;
    }

    // Check our schema version
    read_result = ReadPragmaInteger(m_db, "user_version", "sqlite wallet schema version", error);
    if (!read_result.has_value()) return false;
    int32_t user_ver = read_result.value();
    if (user_ver != WALLET_SCHEMA_VERSION)
    {
        error = "SQLiteDatabase: Unknown sqlite wallet schema version " + std::to_string(user_ver) + ". Only version " + std::to_string(WALLET_SCHEMA_VERSION) + " is supported";
        return false;
    }

    sqlite3_stmt* stmt{nullptr};
    int ret = sqlite3_prepare_v2(m_db, "PRAGMA integrity_check", -1, &stmt, nullptr);
    if (ret != SQLITE_OK)
    {
        sqlite3_finalize(stmt);
        error = "SQLiteDatabase: Failed to prepare statement to verify database: " + std::string(sqlite3_errstr(ret));
        return false;
    }
    while (true)
    {
        ret = sqlite3_step(stmt);
        if (ret == SQLITE_DONE)
        {
            break;
        }
        if (ret != SQLITE_ROW)
        {
            error = "SQLiteDatabase: Failed to execute statement to verify database: " + std::string(sqlite3_errstr(ret));
            break;
        }
        const char* msg = (const char*)sqlite3_column_text(stmt, 0);
        if (!msg)
        {
            error = "SQLiteDatabase: Failed to read database verification error: " + std::string(sqlite3_errstr(ret));
            break;
        }
        std::string str_msg(msg);
        if (str_msg == "ok")
        {
            continue;
        }
        if (error.empty())
        {
            error = "Failed to verify database\n";
        }
        error += str_msg + "\n";
    }
    sqlite3_finalize(stmt);
    return error.empty();
}

void SQLiteDatabase::Open()
{
    int flags = SQLITE_OPEN_FULLMUTEX | SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE;
    /*
    if (false) // MEMORY ONLY DB
    {
        flags |= SQLITE_OPEN_MEMORY; // In memory database for mock db
    }
    */
    if (m_db == nullptr)
    {
        try
        {
            std::filesystem::create_directories(m_dir_path);
        }
        catch (const std::filesystem::filesystem_error &)
        {
            if (!std::filesystem::exists(m_dir_path) || !std::filesystem::is_directory(m_dir_path))
            {
                throw;
            }
        }
        int ret = sqlite3_open_v2(m_full_path.c_str(), &m_db, flags, nullptr);
        if (ret != SQLITE_OK)
        {
            throw std::runtime_error("SQLiteDatabase: Failed to open database: " + std::string(sqlite3_errstr(ret)) + "\n");
        }
        ret = sqlite3_extended_result_codes(m_db, 1);
        if (ret != SQLITE_OK)
        {
            throw std::runtime_error("SQLiteDatabase: Failed to enable extended result codes: " + std::string(sqlite3_errstr(ret)) + "\n");
        }
        // Trace SQL statements if tracing is enabled with -debug=walletdb -loglevel=walletdb:trace
        /*
        if (LogAcceptCategory(BCLog::WALLETDB, BCLog::Level::Trace))
        {
           ret = sqlite3_trace_v2(m_db, SQLITE_TRACE_STMT, TraceSqlCallback, this);
           if (ret != SQLITE_OK)
           {
               printf("Failed to enable SQL tracing for %s\n", Filename().c_str());
           }
        }
        */
    }

    if (sqlite3_db_readonly(m_db, "main") != 0)
    {
        throw std::runtime_error("SQLiteDatabase: Database opened in readonly mode but read-write permissions are needed");
    }

    // Acquire an exclusive lock on the database
    // First change the locking mode to exclusive
    SetPragma(m_db, "locking_mode", "exclusive", "Unable to change database locking mode to exclusive");
    // Now begin a transaction to acquire the exclusive lock. This lock won't be released until we close because of the exclusive locking mode.
    int ret = sqlite3_exec(m_db, "BEGIN EXCLUSIVE TRANSACTION", nullptr, nullptr, nullptr);
    if (ret != SQLITE_OK)
    {
        throw std::runtime_error("SQLiteDatabase: Unable to obtain an exclusive lock on the database, is it being used by another instance of " PACKAGE_NAME "?\n");
    }
    ret = sqlite3_exec(m_db, "COMMIT", nullptr, nullptr, nullptr);
    if (ret != SQLITE_OK)
    {
        throw std::runtime_error("SQLiteDatabase: Unable to end exclusive lock transaction: " + std::string(sqlite3_errstr(ret)) + "\n");
    }

    // Enable fullfsync for the platforms that use it
    SetPragma(m_db, "fullfsync", "true", "Failed to enable fullfsync");

    // Make the table for our key-value pairs
    // First check that the main table exists
    sqlite3_stmt* check_main_stmt{nullptr};
    ret = sqlite3_prepare_v2(m_db, "SELECT name FROM sqlite_master WHERE type='table' AND name='main'", -1, &check_main_stmt, nullptr);
    if (ret != SQLITE_OK)
    {
        throw std::runtime_error("SQLiteDatabase: Failed to prepare statement to check table existence: " + std::string(sqlite3_errstr(ret)) + "\n");
    }
    ret = sqlite3_step(check_main_stmt);
    if (sqlite3_finalize(check_main_stmt) != SQLITE_OK)
    {
        throw std::runtime_error("SQLiteDatabase: Failed to finalize statement checking table existence: " + std::string(sqlite3_errstr(ret)) + "\n");
    }
    bool table_exists;
    if (ret == SQLITE_DONE)
    {
        table_exists = false;
    }
    else if (ret == SQLITE_ROW)
    {
        table_exists = true;
    }
    else
    {
        throw std::runtime_error("SQLiteDatabase: Failed to execute statement to check table existence: " + std::string(sqlite3_errstr(ret)) + "\n");
    }

    // Do the db setup things because the table doesn't exist only when we are creating a new wallet
    if (!table_exists)
    {
        ret = sqlite3_exec(m_db, "CREATE TABLE main(key BLOB PRIMARY KEY NOT NULL, value BLOB NOT NULL)", nullptr, nullptr, nullptr);
        if (ret != SQLITE_OK)
        {
            throw std::runtime_error("SQLiteDatabase: Failed to create new database: " + std::string(sqlite3_errstr(ret)) +"\n");
        }

        // Set the application id
        uint32_t app_id = ReadBE32(MessageHeader::ROAM_NET_MAGIC.data());
        SetPragma(m_db, "application_id", std::to_string(app_id), "Failed to set the application id");

        // Set the user version
        SetPragma(m_db, "user_version", std::to_string(WALLET_SCHEMA_VERSION), "Failed to set the wallet schema version");
    }
}

bool SQLiteDatabase::Rewrite(const char* skip)
{
    // Rewrite the database using the VACUUM command: https://sqlite.org/lang_vacuum.html
    int ret = sqlite3_exec(m_db, "VACUUM", nullptr, nullptr, nullptr);
    return ret == SQLITE_OK;
}

bool SQLiteDatabase::Backup(const std::string& dest) const
{
    sqlite3* db_copy;
    int res = sqlite3_open(dest.c_str(), &db_copy);
    if (res != SQLITE_OK)
    {
        sqlite3_close(db_copy);
        return false;
    }
    sqlite3_backup* backup = sqlite3_backup_init(db_copy, "main", m_db, "main");
    if (!backup)
    {
        printf("%s: Unable to begin backup: %s\n", __func__, sqlite3_errmsg(m_db));
        sqlite3_close(db_copy);
        return false;
    }
    // Specifying -1 will copy all of the pages
    res = sqlite3_backup_step(backup, -1);
    if (res != SQLITE_DONE)
    {
        printf("%s: Unable to backup: %s\n", __func__, sqlite3_errstr(res));
        sqlite3_backup_finish(backup);
        sqlite3_close(db_copy);
        return false;
    }
    res = sqlite3_backup_finish(backup);
    sqlite3_close(db_copy);
    return res == SQLITE_OK;
}

void SQLiteDatabase::Close()
{
    int res = sqlite3_close(m_db);
    if (res != SQLITE_OK)
    {
        throw std::runtime_error("SQLiteDatabase: Failed to close database: " + std::string(sqlite3_errstr(res)) + "\n");
    }
    m_db = nullptr;
}

std::unique_ptr<SQLiteBatch> SQLiteDatabase::MakeBatch()
{
    // We ignore flush_on_close because we don't do manual flushing for SQLite
    return std::make_unique<SQLiteBatch>(*this);
}

SQLiteBatch::SQLiteBatch(SQLiteDatabase& database)
    : m_database(database)
{
    // Make sure we have a db handle
    assert(m_database.m_db);
    SetupSQLStatements();
}

void SQLiteBatch::Close()
{
    // If m_db is in a transaction (i.e. not in autocommit mode), then abort the transaction in progress
    if (m_database.m_db && sqlite3_get_autocommit(m_database.m_db) == 0) {
        if (TxnAbort())
        {
            printf("SQLiteBatch: Batch closed unexpectedly without the transaction being explicitly committed or aborted\n");
        }
        else
        {
            printf("SQLiteBatch: Batch closed and failed to abort transaction\n");
        }
    }

    // Free all of the prepared statements
    const std::vector<std::pair<sqlite3_stmt**, const char*>> statements{
        {&m_read_stmt, "read"},
        {&m_insert_stmt, "insert"},
        {&m_overwrite_stmt, "overwrite"},
        {&m_delete_stmt, "delete"},
        {&m_delete_prefix_stmt, "delete prefix"},
    };

    for (const auto& [stmt_prepared, stmt_description] : statements)
    {
        int res = sqlite3_finalize(*stmt_prepared);
        if (res != SQLITE_OK)
        {
            printf("SQLiteBatch: Batch closed but could not finalize %s statement: %s\n",
                      stmt_description, sqlite3_errstr(res));
        }
        *stmt_prepared = nullptr;
    }
}

bool SQLiteBatch::read_key(const DataStream& key, DataStream& value)
{
    if (!m_database.m_db) return false;
    assert(m_read_stmt);

    // Bind: leftmost parameter in statement is index 1
    if (!BindBlobToStatement(m_read_stmt, 1, key.data(), key.size(), "key"))
    {
        return false;
    }
    int res = sqlite3_step(m_read_stmt);
    if (res != SQLITE_ROW)
    {
        if (res != SQLITE_DONE)
        {
            // SQLITE_DONE means "not found", don't log an error in that case.
            printf("%s: Unable to execute statement: %s\n", __func__, sqlite3_errstr(res));
        }
        sqlite3_clear_bindings(m_read_stmt);
        sqlite3_reset(m_read_stmt);
        return false;
    }
    // Leftmost column in result is index 0
    value.clear();
    value.write( (char*)sqlite3_column_blob(m_read_stmt, 0), (size_t)sqlite3_column_bytes(m_read_stmt, 0) );
    sqlite3_clear_bindings(m_read_stmt);
    sqlite3_reset(m_read_stmt);
    return true;
}

bool SQLiteBatch::write_key(const DataStream& key, const DataStream& value, bool overwrite)
{
    if (!m_database.m_db)
    {
        return false;
    }
    assert(m_insert_stmt && m_overwrite_stmt);
    sqlite3_stmt* stmt;
    if (overwrite)
    {
        stmt = m_overwrite_stmt;
    }
    else
    {
        stmt = m_insert_stmt;
    }
    // Bind: leftmost parameter in statement is index 1
    // Insert index 1 is key, 2 is value
    if (!BindBlobToStatement(stmt, 1, key.data(), key.size(), "key"))
    {
        return false;
    }
    if (!BindBlobToStatement(stmt, 2, value.data(), value.size(), "value"))
    {
        return false;
    }
    // Execute
    int res = sqlite3_step(stmt);
    sqlite3_clear_bindings(stmt);
    sqlite3_reset(stmt);
    if (res != SQLITE_DONE)
    {
        printf("%s: Unable to execute statement: %s\n", __func__, sqlite3_errstr(res));
    }
    return res == SQLITE_DONE;
}

bool SQLiteBatch::ExecStatement(sqlite3_stmt* stmt, const uint8_t* blob, const uint64_t blob_len)
{
    if (!m_database.m_db)
    {
        return false;
    }
    assert(stmt);
    // Bind: leftmost parameter in statement is index 1
    if (!BindBlobToStatement(stmt, 1, blob, blob_len, "key"))
    {
        return false;
    }
    // Execute
    int res = sqlite3_step(stmt);
    sqlite3_clear_bindings(stmt);
    sqlite3_reset(stmt);
    if (res != SQLITE_DONE)
    {
        printf("%s: Unable to execute statement: %s\n", __func__, sqlite3_errstr(res));
    }
    return res == SQLITE_DONE;
}

bool SQLiteBatch::erase_key(const DataStream& key)
{
    return ExecStatement(m_delete_stmt, key.data(), key.size());
}

bool SQLiteBatch::ErasePrefix(uint8_t* prefix, const uint64_t prefix_len)
{
    return ExecStatement(m_delete_prefix_stmt, prefix, prefix_len);
}

bool SQLiteBatch::HasKey(DataStream&& key)
{
    if (!m_database.m_db)
    {
        return false;
    }
    assert(m_read_stmt);
    // Bind: leftmost parameter in statement is index 1
    if (!BindBlobToStatement(m_read_stmt, 1, key.data(), key.size(), "key"))
    {
        return false;
    }
    int res = sqlite3_step(m_read_stmt);
    sqlite3_clear_bindings(m_read_stmt);
    sqlite3_reset(m_read_stmt);
    return res == SQLITE_ROW;
}

SQLiteCursor::Status SQLiteCursor::Next(DataStream& key, DataStream& value)
{
    int res = sqlite3_step(m_cursor_stmt);
    if (res == SQLITE_DONE)
    {
        return Status::DONE;
    }
    if (res != SQLITE_ROW)
    {
        printf("%s: Unable to execute cursor step: %s\n", __func__, sqlite3_errstr(res));
        return Status::FAIL;
    }

    key.clear();
    value.clear();

    // Leftmost column in result is index 0
    key.write( (char*)sqlite3_column_blob(m_cursor_stmt, 0), (size_t)sqlite3_column_bytes(m_cursor_stmt, 0) );
    value.write( (char*)sqlite3_column_blob(m_cursor_stmt, 1), (size_t)sqlite3_column_bytes(m_cursor_stmt, 1) );
    return Status::MORE;
}

SQLiteCursor::~SQLiteCursor()
{
    sqlite3_clear_bindings(m_cursor_stmt);
    sqlite3_reset(m_cursor_stmt);
    int res = sqlite3_finalize(m_cursor_stmt);
    if (res != SQLITE_OK)
    {
        printf("%s: cursor closed but could not finalize cursor statement: %s\n",
                  __func__, sqlite3_errstr(res));
    }
}

std::unique_ptr<SQLiteCursor> SQLiteBatch::GetNewCursor()
{
    if (!m_database.m_db)
    {
        return nullptr;
    }
    auto cursor = std::make_unique<SQLiteCursor>();

    const char* stmt_text = "SELECT key, value FROM main";
    int res = sqlite3_prepare_v2(m_database.m_db, stmt_text, -1, &cursor->m_cursor_stmt, nullptr);
    if (res != SQLITE_OK)
    {
        throw std::runtime_error(std::string(__func__) + ": Failed to setup cursor SQL statement: " + sqlite3_errstr(res) + "\n");
    }

    return cursor;
}

std::unique_ptr<SQLiteCursor> SQLiteBatch::GetNewPrefixCursor(uint8_t* prefix, const uint64_t prefix_len)
{
    if (!m_database.m_db)
    {
        return nullptr;
    }
    // To get just the records we want, the SQL statement does a comparison of the binary data
    // where the data must be greater than or equal to the prefix, and less than
    // the prefix incremented by one (when interpreted as an integer)
    std::vector<uint8_t> start_range(prefix, prefix + prefix_len);
    std::vector<uint8_t> end_range(prefix, prefix + prefix_len);
    auto it = end_range.rbegin();
    for (; it != end_range.rend(); ++it)
    {
        if (*it == std::numeric_limits<uint8_t>::max())
        {
            *it = 0;
            continue;
        }
        *it += 1;
        break;
    }
    if (it == end_range.rend())
    {
        // If the prefix is all 0xff bytes, clear end_range as we won't need it
        end_range.clear();
    }

    auto cursor = std::make_unique<SQLiteCursor>(start_range, end_range);
    if (!cursor)
    {
        return nullptr;
    }

    const char* stmt_text = end_range.empty() ? "SELECT key, value FROM main WHERE key >= ?" :
                            "SELECT key, value FROM main WHERE key >= ? AND key < ?";
    int res = sqlite3_prepare_v2(m_database.m_db, stmt_text, -1, &cursor->m_cursor_stmt, nullptr);
    if (res != SQLITE_OK)
    {
        throw std::runtime_error("SQLiteDatabase: Failed to setup cursor SQL statement: " + std::string(sqlite3_errstr(res)) + "\n");
    }

    if (!BindBlobToStatement(cursor->m_cursor_stmt, 1, cursor->m_prefix_range_start.data(), cursor->m_prefix_range_start.size(), "prefix_start"))
    {
        return nullptr;
    }
    if (!end_range.empty())
    {
        if (!BindBlobToStatement(cursor->m_cursor_stmt, 2, cursor->m_prefix_range_end.data(), cursor->m_prefix_range_end.size(), "prefix_end"))
        {
            return nullptr;
        }
    }

    return cursor;
}

bool SQLiteBatch::TxnBegin()
{
    if (!m_database.m_db || sqlite3_get_autocommit(m_database.m_db) == 0)
    {
        return false;
    }
    int res = sqlite3_exec(m_database.m_db, "BEGIN TRANSACTION", nullptr, nullptr, nullptr);
    if (res != SQLITE_OK)
    {
        printf("SQLiteBatch: Failed to begin the transaction\n");
    }
    return res == SQLITE_OK;
}

bool SQLiteBatch::TxnCommit()
{
    if (!m_database.m_db || sqlite3_get_autocommit(m_database.m_db) != 0)
    {
        return false;
    }
    int res = sqlite3_exec(m_database.m_db, "COMMIT TRANSACTION", nullptr, nullptr, nullptr);
    if (res != SQLITE_OK)
    {
        printf("SQLiteBatch: Failed to commit the transaction\n");
    }
    return res == SQLITE_OK;
}

bool SQLiteBatch::TxnAbort()
{
    if (!m_database.m_db || sqlite3_get_autocommit(m_database.m_db) != 0)
    {
        return false;
    }
    int res = sqlite3_exec(m_database.m_db, "ROLLBACK TRANSACTION", nullptr, nullptr, nullptr);
    if (res != SQLITE_OK)
    {
        printf("SQLiteBatch: Failed to abort the transaction\n");
    }
    return res == SQLITE_OK;
}

std::string SQLiteDatabaseVersion()
{
    return std::string(sqlite3_libversion());
}
