// This file is part of the libroam c++ library
// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2016 The Bitcoin Core developers
// Copyright (c) 2014-2018 The Eccoin developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "uint256.h"

#include "util.h"

#include <stdio.h>
#include <string.h>

template <uint32_t BITS>
base_blob<BITS>::base_blob(const std::vector<uint8_t> &vch)
{
    assert(vch.size() == sizeof(data));
    std::memcpy(data, &vch[0], sizeof(data));
}

template <uint32_t BITS>
std::string base_blob<BITS>::get_hex() const
{
    char psz[sizeof(data) * 2 + 1];
    for (uint32_t i = 0; i < sizeof(data); i++)
    {
        sprintf(psz + i * 2, "%02x", data[sizeof(data) - i - 1]);
    }
    return std::string(psz, psz + sizeof(data) * 2);
}

template <uint32_t BITS>
void base_blob<BITS>::set_hex(const char *psz)
{
    std::memset(data, 0, sizeof(data));

    // skip leading spaces
    while (isspace(*psz))
        psz++;

    // skip 0x
    if (psz[0] == '0' && tolower(psz[1]) == 'x')
        psz += 2;

    // hex string to uint
    const char *pbegin = psz;
    while (::hex_digit(*psz) != -1)
        psz++;
    psz--;
    uint8_t *p1 = (uint8_t *)data;
    uint8_t *pend = p1 + WIDTH;
    while (psz >= pbegin && p1 < pend)
    {
        *p1 = ::hex_digit(*psz--);
        if (psz >= pbegin)
        {
            *p1 |= ((uint8_t)::hex_digit(*psz--) << 4);
            p1++;
        }
    }
}

template <uint32_t BITS>
void base_blob<BITS>::set_hex(const std::string &str)
{
    set_hex(str.c_str());
}

template <uint32_t BITS>
std::string base_blob<BITS>::to_string() const
{
    return (get_hex());
}

// Explicit instantiations for base_blob<160>
template base_blob<160>::base_blob(const std::vector<uint8_t> &);
template std::string base_blob<160>::get_hex() const;
template std::string base_blob<160>::to_string() const;
template void base_blob<160>::set_hex(const char *);
template void base_blob<160>::set_hex(const std::string &);

// Explicit instantiations for base_blob<256>
template base_blob<256>::base_blob(const std::vector<uint8_t> &);
template std::string base_blob<256>::get_hex() const;
template std::string base_blob<256>::to_string() const;
template void base_blob<256>::set_hex(const char *);
template void base_blob<256>::set_hex(const std::string &);

static void inline HashMix(uint32_t &a, uint32_t &b, uint32_t &c)
{
    // Taken from lookup3, by Bob Jenkins.
    a -= c;
    a ^= ((c << 4) | (c >> 28));
    c += b;
    b -= a;
    b ^= ((a << 6) | (a >> 26));
    a += c;
    c -= b;
    c ^= ((b << 8) | (b >> 24));
    b += a;
    a -= c;
    a ^= ((c << 16) | (c >> 16));
    c += b;
    b -= a;
    b ^= ((a << 19) | (a >> 13));
    a += c;
    c -= b;
    c ^= ((b << 4) | (b >> 28));
    b += a;
}

static void inline HashFinal(uint32_t &a, uint32_t &b, uint32_t &c)
{
    // Taken from lookup3, by Bob Jenkins.
    c ^= b;
    c -= ((b << 14) | (b >> 18));
    a ^= c;
    a -= ((c << 11) | (c >> 21));
    b ^= a;
    b -= ((a << 25) | (a >> 7));
    c ^= b;
    c -= ((b << 16) | (b >> 16));
    a ^= c;
    a -= ((c << 4) | (c >> 28));
    b ^= a;
    b -= ((a << 14) | (a >> 18));
    c ^= b;
    c -= ((b << 24) | (b >> 8));
}

uint64_t uint256::get_hash(const uint256 &salt) const
{
    uint32_t a, b, c;
    const uint32_t *pn = (const uint32_t *)data;
    const uint32_t *salt_pn = (const uint32_t *)salt.data;
    a = b = c = 0xdeadbeef + WIDTH;

    a += pn[0] ^ salt_pn[0];
    b += pn[1] ^ salt_pn[1];
    c += pn[2] ^ salt_pn[2];
    HashMix(a, b, c);
    a += pn[3] ^ salt_pn[3];
    b += pn[4] ^ salt_pn[4];
    c += pn[5] ^ salt_pn[5];
    HashMix(a, b, c);
    a += pn[6] ^ salt_pn[6];
    b += pn[7] ^ salt_pn[7];
    HashFinal(a, b, c);

    return ((((uint64_t)b) << 32) | c);
}
