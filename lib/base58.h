// This file is part of the libroam c++ library
// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2016 The Bitcoin Core developers
// Copyright (c) 2014-2018 The Eccoin developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

/**
 * Why base-58 instead of standard base-64 encoding?
 * - Don't want 0OIl characters that look the same in some fonts and
 *      could be used to create visually identical looking data.
 * - A string with non-alphanumeric characters is not as easily accepted as input.
 * - E-mail usually won't line-break if there's no punctuation to break at.
 * - Double-clicking selects the whole string as one word if it's all alphanumeric.
 */
#ifndef BITCOIN_BASE58_H
#define BITCOIN_BASE58_H

#include "secure_memory.h"

#include <cstring>
#include <string>
#include <vector>

/**
 * Encode a byte vector into a base58-encoded string, including checksum
 */
std::string encode_base58_check(const std::vector<uint8_t> &vchIn);

/**
 * Decode a base58-encoded string (psz) that includes a checksum into a byte
 * vector (vchRet), return true if decoding is successful
 */
bool decode_base58_check(const char *psz, std::vector<uint8_t> &vchRet);

/**
 * Decode a base58-encoded string (str) that includes a checksum into a byte
 * vector (vchRet), return true if decoding is successful
 */
bool decode_base58_check(const std::string &str, std::vector<uint8_t> &vchRet);


/**
 * Base class for all base58-encoded data
 */
class Base58Data
{
protected:
    //! the version byte(s)
    std::vector<uint8_t> vch_version;

    //! the actually encoded data
    typedef std::vector<uint8_t, zero_after_free_allocator<uint8_t> > vector_uchar;
    vector_uchar vch_data;

    Base58Data()
    {
        vch_version.clear();
        vch_data.clear();
    }

    void set_data(const std::vector<uint8_t> &vchVersionIn, const void *pdata, size_t nSize)
    {
        vch_version = vchVersionIn;
        vch_data.resize(nSize);
        if (!vch_data.empty())
        {
            memcpy(&vch_data[0], pdata, nSize);
        }
    }
    void set_data(const std::vector<uint8_t> &vchVersionIn,
        const uint8_t *pbegin,
        const uint8_t *pend)
    {
        set_data(vchVersionIn, (void *)pbegin, pend - pbegin);
    }

public:
    bool set_string(const char *psz, uint32_t nVersionBytes = 1)
    {
        std::vector<uint8_t> vchTemp;
        const bool rc58 = decode_base58_check(psz, vchTemp);
        if ((!rc58) || (vchTemp.size() < nVersionBytes))
        {
            vch_data.clear();
            vch_version.clear();
            return false;
        }
        vch_version.assign(vchTemp.begin(), vchTemp.begin() + nVersionBytes);
        vch_data.resize(vchTemp.size() - nVersionBytes);
        if (!vch_data.empty())
        {
            memcpy(&vch_data[0], &vchTemp[nVersionBytes], vch_data.size());
        }
        memory_cleanse(&vchTemp[0], vch_data.size());
        return true;
    }
    bool set_string(const std::string &str)
    {
        return set_string(str.c_str());
    }
    std::string to_string() const
    {
        std::vector<uint8_t> vch = vch_version;
        vch.insert(vch.end(), vch_data.begin(), vch_data.end());
        return encode_base58_check(vch);
    }
    int32_t compare_to(const Base58Data &b58) const
    {
        if (vch_version < b58.vch_version)
        {
            return -1;
        }
        if (vch_version > b58.vch_version)
        {
            return 1;
        }
        if (vch_data < b58.vch_data)
        {
            return -1;
        }
        if (vch_data > b58.vch_data)
        {
            return 1;
        }
        return 0;
    }
    bool operator==(const Base58Data &b58) const
    {
        return compare_to(b58) == 0;
    }
    bool operator<=(const Base58Data &b58) const
    {
        return compare_to(b58) <= 0;
    }
    bool operator>=(const Base58Data &b58) const
    {
        return compare_to(b58) >= 0;
    }
    bool operator<(const Base58Data &b58) const
    {
        return compare_to(b58) < 0;
    }
    bool operator>(const Base58Data &b58) const
    {
        return compare_to(b58) > 0;
    }
};

#endif // BITCOIN_BASE58_H
