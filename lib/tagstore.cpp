// This file is part of libroam
// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2016 The Bitcoin Core developers
// Copyright (c) 2014-2019 The Eccoin developers
// Copyright (c) 2019-2022 Greg Griffith
// Copyright (c) 2019-2022 The Roam Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "tagstore.h"

#include "crypter.h"

static bool EncryptSecret(const KeyingMaterial &vMasterKey,
    const KeyingMaterial &vchPlaintext,
    const uint256 &nIV,
    CryptedPrivKey &vchCiphertext)
{
    Crypter cKeyCrypter;
    std::vector<uint8_t> chIV(WALLET_CRYPTO_KEY_SIZE);
    memcpy(&chIV[0], &nIV, WALLET_CRYPTO_KEY_SIZE);
    if (!cKeyCrypter.set_key(vMasterKey, chIV))
    {
        return false;
    }
    return cKeyCrypter.encrypt(*((const KeyingMaterial *)&vchPlaintext), vchCiphertext);
}

static bool DecryptSecret(const KeyingMaterial &vMasterKey,
    const CryptedPrivKey &vchCiphertext,
    const uint256 &nIV,
    KeyingMaterial &vchPlaintext)
{
    Crypter cKeyCrypter;
    std::vector<uint8_t> chIV(WALLET_CRYPTO_KEY_SIZE);
    memcpy(&chIV[0], &nIV, WALLET_CRYPTO_KEY_SIZE);
    if (!cKeyCrypter.set_key(vMasterKey, chIV))
    {
        return false;
    }
    return cKeyCrypter.decrypt(vchCiphertext, *((KeyingMaterial *)&vchPlaintext));
}

static bool DecryptTag(const KeyingMaterial &vMasterKey, const RoutingTag &tagIn, RoutingTag &tagOut)
{
    KeyingMaterial vchSecret;
    if (!DecryptSecret(vMasterKey, tagIn.get_privkey(), tagIn.get_pubkey().get_hash(), vchSecret))
    {
        return false;
    }
    if (vchSecret.size() != 32)
    {
        return false;
    }
    tagOut = RoutingTag(tagIn.is_private(), tagIn.get_pubkey(), vchSecret);
    // TODO : we probably dont need to verify the pubkey here
    return tagOut.verify_pubkey(tagIn.get_pubkey());
}

///////////////////////////////////////////////////

bool NetTagStore::SetCrypted()
{
    cs_TagStore.lock();
    if (fUseCrypto)
    {
        cs_TagStore.unlock();
        return true;
    }
    if (!mapTags.empty())
    {
        cs_TagStore.unlock();
        return false;
    }
    fUseCrypto = true;
    cs_TagStore.unlock();
    return true;
}

//! will encrypt previously unencrypted keys
bool NetTagStore::EncryptKeys(KeyingMaterial &vMasterKeyIn)
{
    {
        cs_TagStore.lock();
        if (IsCrypted())
        {
            cs_TagStore.unlock();
            return false;
        }
        fUseCrypto = true;
        for (auto &mTag : mapTags)
        {
            const RoutingTag &tag = mTag.second;
            PubKey vchPubKey = tag.get_pubkey();
            const PrivKey tagprivkey = tag.get_privkey();
            KeyingMaterial vchSecret(tagprivkey.begin(), tagprivkey.end());
            CryptedPrivKey vchCryptedSecret;
            if (!EncryptSecret(vMasterKeyIn, vchSecret, vchPubKey.get_hash(), vchCryptedSecret))
            {
                cs_TagStore.unlock();
                return false;
            }
            RoutingTag cryptedTag(tag.is_private(), tag.get_pubkey(), vchCryptedSecret);
            if (!AddCryptedTagToTagMap(cryptedTag))
            {
                cs_TagStore.unlock();
                return false;
            }
        }
        mapTags.clear();
        cs_TagStore.unlock();
    }
    return true;
}

bool NetTagStore::Unlock(const KeyingMaterial &vMasterKeyIn)
{
    {
        cs_TagStore.lock();
        if (!SetCrypted())
        {
            cs_TagStore.unlock();
            return false;
        }
        bool keyPass = false;
        bool keyFail = false;
        TagMap::const_iterator mi = mapTags.begin();
        for (; mi != mapTags.end(); ++mi)
        {
            const RoutingTag tagIn = (*mi).second;
            RoutingTag tagOut;
            if (!DecryptTag(vMasterKeyIn, tagIn, tagOut))
            {
                keyFail = true;
                break;
            }
            keyPass = true;
            if (fDecryptionThoroughlyChecked)
            {
                break;
            }
        }
        if (keyPass && keyFail)
        {
            // LogPrintf("The routing tag db is probably corrupted: Some keys decrypt but not all.\n");
            assert(false);
        }
        if (keyFail || !keyPass)
        {
            cs_TagStore.unlock();
            return false;
        }
        vMasterKey = vMasterKeyIn;
        fDecryptionThoroughlyChecked = true;
        cs_TagStore.unlock();
    }
    return true;
}

bool NetTagStore::AddCryptedTagToTagMap(const RoutingTag &tag)
{
    cs_TagStore.lock();
    if (!SetCrypted())
    {
        cs_TagStore.unlock();
        return false;
    }
    mapTags[tag.get_pubkey().get_key_id()] = tag;
    cs_TagStore.unlock();
    return true;
}

bool NetTagStore::AddTagToTagMap(const RoutingTag &tag)
{
    {
        cs_TagStore.lock();
        if (!IsCrypted())
        {
            mapTags[tag.get_pubkey().get_key_id()] = tag;
            cs_TagStore.unlock();
            return true;
        }
        if (IsLocked())
        {
            cs_TagStore.unlock();
            return false;
        }
        CryptedPrivKey vchCryptedSecret;
        PrivKey tagprivkey = tag.get_privkey();
        KeyingMaterial vchSecret(tagprivkey.begin(), tagprivkey.end());
        if (!EncryptSecret(vMasterKey, vchSecret, tag.get_pubkey().get_hash(), vchCryptedSecret))
        {
            cs_TagStore.unlock();
            return false;
        }
        RoutingTag cryptedTag(tag.is_private(), tag.get_pubkey(), vchCryptedSecret);
        if (!AddCryptedTagToTagMap(cryptedTag))
        {
            cs_TagStore.unlock();
            return false;
        }
        cs_TagStore.unlock();
    }
    return true;
}

NetTagStore::NetTagStore() : fUseCrypto(false), fDecryptionThoroughlyChecked(false)
{
    set_null();
}

NetTagStore::~NetTagStore()
{
    proutingdbEncryption->Close();
    delete proutingdbEncryption;
    proutingdbEncryption = nullptr;
}

void NetTagStore::set_null()
{
    fFileBacked = false;
    nMasterKeyMaxID = 0;
    proutingdbEncryption = nullptr;
}

bool NetTagStore::IsCrypted() const
{
    return fUseCrypto.load();
}
bool NetTagStore::IsLocked() const
{
    if (!IsCrypted())
    {
        return false;
    }
    bool result;
    {
        cs_TagStore.lock();
        result = vMasterKey.empty();
        cs_TagStore.unlock();
    }
    return result;
}

bool NetTagStore::Lock()
{
    if (!SetCrypted())
    {
        return false;
    }
    {
        cs_TagStore.lock();
        vMasterKey.clear();
        cs_TagStore.unlock();
    }
    return true;
}

bool NetTagStore::HaveTag(const KeyID &address) const
{
    cs_TagStore.lock();
    bool res = mapTags.count(address) > 0;
    cs_TagStore.unlock();
    return res;
}

bool NetTagStore::GetTag(const KeyID &address, RoutingTag &tagOut) const
{
    cs_TagStore.lock();
    if (!IsCrypted())
    {
        TagMap::const_iterator mi = mapTags.find(address);
        if (mi != mapTags.end())
        {
            tagOut = mi->second;
            cs_TagStore.unlock();
            return true;
        }
    }
    else
    {
        TagMap::const_iterator mi = mapTags.find(address);
        if (mi != mapTags.end())
        {
            const RoutingTag tagIn = (*mi).second;
            bool res = DecryptTag(vMasterKey, tagIn, tagOut);
            cs_TagStore.unlock();
            return res;
        }
    }
    cs_TagStore.unlock();
    return false;
}
bool NetTagStore::GetTagPubKey(const KeyID &address, PubKey &vchPubKeyOut) const
{
    cs_TagStore.lock();
    TagMap::const_iterator mi = mapTags.find(address);
    if (mi != mapTags.end())
    {
        vchPubKeyOut = (*mi).second.get_pubkey();
        cs_TagStore.unlock();
        return true;
    }
    cs_TagStore.unlock();
    return false;
}
void NetTagStore::GetTagIds(std::set<KeyID> &setAddress) const
{
    cs_TagStore.lock();
    setAddress.clear();
    TagMap::const_iterator mi = mapTags.begin();
    while (mi != mapTags.end())
    {
        setAddress.insert((*mi).first);
        mi++;
    }
    cs_TagStore.unlock();
}

bool NetTagStore::AddCryptedTag(const RoutingTag &tag)
{
    if (!AddCryptedTagToTagMap(tag))
        return false;
    if (!fFileBacked)
        return true;
    if (proutingdbEncryption)
    {
        return proutingdbEncryption->write_crypted_tag(tag);
    }
    // else
    return RoutingTagDB(strRoutingDir, strRoutingFile).write_crypted_tag(tag);
}

bool NetTagStore::AddTag(const RoutingTag &tag)
{
    if (!AddTagToTagMap(tag))
    {
        return false;
    }
    if (proutingdbEncryption)
    {
        return proutingdbEncryption->write_tag(tag);
    }
    // else
    return RoutingTagDB(strRoutingDir, strRoutingFile).write_tag(tag);
}
bool NetTagStore::load(const std::string &strDirName, const std::string &strFileName)
{
    strRoutingDir = strDirName;
    strRoutingFile = strFileName;
    fFileBacked = true;
    proutingdbEncryption = new RoutingTagDB(strDirName, strFileName);
    assert(proutingdbEncryption != nullptr);

    bool nLoadWalletRet = proutingdbEncryption->load_all_records(this);
    assert(nLoadWalletRet == true);

    if (setKeyPool.empty())
    {
        // TODO make this more than just 1 key
        while (setKeyPool.size() < (1 + 1))
        {
            int64_t nEnd = 1;
            if (!setKeyPool.empty())
            {
                nEnd = *(--setKeyPool.end()) + 1;
            }
            RoutingTag tag;
            tag.make_new_tag();
            PubKey pubkey = tag.get_pubkey();
            assert(tag.verify_pubkey(pubkey));
            if (!AddTagToTagMap(tag))
            {
                throw std::runtime_error("CWallet::GenerateNewKey(): AddKey failed");
            }
            if (!proutingdbEncryption->write_pool(nEnd, KeyPoolEntry(pubkey)))
            {
                throw std::runtime_error("Connman::Start(): writing generated key failed");
            }
            setKeyPool.insert(nEnd);
            // LogPrint("net", "keypool added key %d, size=%u\n", nEnd, setKeyPool.size());
        }
    }
    // Get the oldest key
    assert(setKeyPool.empty() == false);
    if (!proutingdbEncryption->read_last_used_public_tag(publicRoutingTag))
    {
        // assign new public routing tag from pool
        int64_t nIndex = -1;
        nIndex = *(setKeyPool.begin());
        KeyPoolEntry keypool;
        if (!proutingdbEncryption->read_pool(nIndex, keypool))
        {
            throw std::runtime_error("ReserveKeyFromKeyPool(): read failed");
        }
        if (!HaveTag(keypool.vchPubKey.get_key_id()))
        {
            throw std::runtime_error("ReserveKeyFromKeyPool(): unknown key in key pool");
        }
        assert(keypool.vchPubKey.is_valid());
        assert(GetTag(keypool.vchPubKey.get_key_id(), publicRoutingTag));
        proutingdbEncryption->write_last_used_public_tag(publicRoutingTag);
    }
    return true;
}

PubKey NetTagStore::GetCurrentPublicTagPubKey()
{
    return publicRoutingTag.get_pubkey();
}
bool NetTagStore::LoadCryptedTag(const RoutingTag &tag)
{
    return AddCryptedTagToTagMap(tag);
}
bool NetTagStore::LoadTag(const RoutingTag &tag)
{
    return AddTagToTagMap(tag);
}
