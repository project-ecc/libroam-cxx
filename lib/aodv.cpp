// This file is part of libroam
// Copyright (c) 2019-2022 Greg Griffith
// Copyright (c) 2019-2022 The Roam Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "aodv.h"

///////// PRIVATE ////////////

bool AodvRouteTable::_have_key_route(const PubKey &key)
{
    return (map_routes_by_pubkey.find(key) != map_routes_by_pubkey.end());
}

bool AodvRouteTable::have_key_route(const PubKey &key)
{
    cs_aodv.lock_shared();
    bool res = _have_key_route(key);
    cs_aodv.unlock_shared();
    return res;
}

bool AodvRouteTable::_have_id_route(const NodeId &node)
{
    return (map_routes_by_peer_id.find(node) != map_routes_by_peer_id.end());
}

bool AodvRouteTable::have_id_route(const NodeId &node)
{
    cs_aodv.lock_shared();
    bool res = _have_id_route(node);
    cs_aodv.unlock_shared();
    return res;
}

///////// PUBLIC ////////////

bool AodvRouteTable::have_route(const PubKey &key)
{
    cs_aodv.lock_shared();
    bool res = (map_routes_by_pubkey.find(key) != map_routes_by_pubkey.end());
    cs_aodv.unlock_shared();
    return res;
}

bool AodvRouteTable::have_route(const NodeId &node)
{
    cs_aodv.lock_shared();
    bool res = (map_routes_by_peer_id.find(node) != map_routes_by_peer_id.end());
    cs_aodv.unlock_shared();
    return res;
}

void AodvRouteTable::_get_id_key_table(std::map<NodeId, std::set<PubKey> > &IdKey)
{
    IdKey = map_routes_by_peer_id;
}

void AodvRouteTable::get_id_key_table(std::map<NodeId, std::set<PubKey> > &IdKey)
{
    cs_aodv.lock_shared();
    _get_id_key_table(IdKey);
    cs_aodv.unlock_shared();
}

void AodvRouteTable::_get_key_id_table(std::map<PubKey, std::set<NodeId> > &KeyId)
{
    KeyId = map_routes_by_pubkey;
}

void AodvRouteTable::get_key_id_table(std::map<PubKey, std::set<NodeId> > &KeyId)
{
    cs_aodv.lock_shared();
    _get_key_id_table(KeyId);
    cs_aodv.unlock_shared();
}

void AodvRouteTable::get_routing_tables(std::map<NodeId, std::set<PubKey> > &IdKey,
    std::map<PubKey, std::set<NodeId> > &KeyId,
    std::map<NodeId, PubKey> &directConnect)
{
    cs_aodv.lock_shared();
    _get_id_key_table(IdKey);
    _get_key_id_table(KeyId);
    directConnect = map_direct_peers;
    cs_aodv.unlock_shared();
    return;
}

void AodvRouteTable::add_route(const PubKey &key, const NodeId &node, bool directConnect)
{
    cs_aodv.lock();
    bool haveId = _have_id_route(node);
    bool haveKey = _have_key_route(key);
    if (haveId)
    {
        map_routes_by_peer_id[node].emplace(key);
    }
    else
    {
        std::set<PubKey> keys;
        keys.emplace(key);
        map_routes_by_peer_id.emplace(node, keys);
    }
    if (haveKey)
    {
        map_routes_by_pubkey[key].emplace(node);
    }
    else
    {
        std::set<NodeId> nodes;
        nodes.emplace(node);
        map_routes_by_pubkey.emplace(key, nodes);
    }
    if (directConnect)
    {
        map_direct_peers.emplace(node, key);
    }
    cs_aodv.unlock();
}

void AodvRouteTable::remove_routes_for_peer(const NodeId &node)
{
    cs_aodv.lock();
    // find the reverse lookup to find all pubkeys this node can route too.
    std::map<NodeId, std::set<PubKey> >::iterator iter = map_routes_by_peer_id.find(node);
    if (iter != map_routes_by_peer_id.end())
    {
        // for each pubkey, erase this node from the routes to that pubkey
        for (const PubKey &key : iter->second)
        {
            std::map<PubKey, std::set<NodeId> >::iterator it = map_routes_by_pubkey.find(key);
            if (it != map_routes_by_pubkey.end())
            {
                it->second.erase(node);
                // if we erased this node from the routes to that pubkey and there are
                // no other nodes that know a route for that pubkey, erase the entry for
                // the pubkey
                if (it->second.empty())
                {
                    map_routes_by_pubkey.erase(it);
                }
            }
        }
        // after we have removed all of the pubkeys that are routed through this
        // node, erase the entry for this node
        map_routes_by_peer_id.erase(iter);
    }
    map_direct_peers.erase(node);
    cs_aodv.unlock();
}

bool AodvRouteTable::get_key_node(const PubKey &key, NodeId &result)
{
    cs_aodv.lock_shared();
    if (!_have_key_route(key))
    {
        return false;
    }
    // TODO : randomise this to select a random node id for the key not always
    // the front in the set
    result = *(map_routes_by_pubkey[key].begin());
    cs_aodv.unlock_shared();
    return true;
}

void AodvRouteTable::add_new_request_time_data(const uint64_t &nonce)
{
    cs_aodv.lock();
    map_request_time.emplace(nonce, get_time_millis());
    cs_aodv.unlock();
}

bool AodvRouteTable::is_old_request(const uint64_t &nonce)
{
    cs_aodv.lock_shared();
    bool res = (map_request_source.find(nonce) != map_request_source.end());
    cs_aodv.unlock_shared();
    return res;
}

void AodvRouteTable::add_new_request_source(const uint64_t &nonce, const PubKey &source)
{
    cs_aodv.lock();
    if (source.is_valid() == false)
    {
        cs_aodv.unlock();
        return;
    }
    if (map_request_source.find(nonce) != map_request_source.end())
    {
        cs_aodv.unlock();
        return;
    }
    map_request_source.emplace(nonce, source);
    cs_aodv.unlock();
}

bool AodvRouteTable::get_request_source(const uint64_t &nonce, PubKey &source)
{
    cs_aodv.lock_shared();
    auto iter = map_request_source.find(nonce);
    if (iter == map_request_source.end())
    {
        cs_aodv.unlock_shared();
        return false;
    }
    source = iter->second;
    cs_aodv.unlock_shared();
    return true;
}

/*
void _RequestRouteToPeer(CConnman &connman, const PubKey &source, const uint64_t &nonce, const PubKey &searchKey)
{
    if (g_aodvtable.is_old_request(nonce))
    {
        return;
    }
    g_aodvtable.add_new_request_time_data(nonce);
    connman.PushMessageAll(source, RREQ, nonce, searchKey);
}

void RequestRouteToPeer(CConnman &connman, const PubKey &source, const uint64_t &nonce, const PubKey &searchKey)
{
    _RequestRouteToPeer(connman, source, nonce, searchKey);
}

void RequestRouteToPeer(CConnman *connman, const PubKey &source, const uint64_t &nonce, const PubKey &searchKey)
{
    _RequestRouteToPeer(*connman, source, nonce, searchKey);
}
*/
