// This file is part of libroam
// Copyright (c) 2019-2022 Greg Griffith
// Copyright (c) 2019-2022 The Roam Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "netmessage.h"
#include "packetmanager.h"

bool process_packet(PacketManager* pacman, const PubKey &searchKey, RoamPacket &newPacket)
{
    if (newPacket.n_total_length > ROAM_MAX_PACKET_SIZE)
    {
        // packet is larger than allowed
        // g_dosman->Misbehaving(pfrom, 100, "invalid-packet-too-large");
        // LogPrint("roam", "SENDPACKET: invalid-packet-too-large \n");
        return false;
    }

    bool ours = pacman->get_tag_store()->GetCurrentPublicTagPubKey() == searchKey;
    // we dont want to waste network time by passiing information that is declared out of scope
    if (ours)
    {
        if (!pacman->process_packet(newPacket))
        {
            // TODO : send an error back to the sender if possible
            // LogPrint("roam", "SENDPACKET: failed to process packet \n");
        }
    }
    else
    {
        NodeId peerNode;
        if (pacman->get_routing_table()->get_key_node(searchKey, peerNode))
        {
            // connman.PushMessageToId(peerNode, SENDPACKET, searchKey, newPacket);
        }
        else
        {
            // LogPrint("roam", "SENDPACKET: Could not find key for node \n");
            // TODO: send an error back to the sender if possible
        }
    }
    return true;
}

bool process_net_message(PacketManager* pacman, NetMessage &message)
{
    // Read header
    MessageHeader &hdr = message.hdr;
    if (!hdr.is_valid(MessageHeader::ROAM_NET_MAGIC))
    {
        // LogPrintf("PROCESSMESSAGE: ERRORS IN HEADER %s peer=%d\n", SanitizeString(hdr.get_command()), pfrom->id);
        return false;
    }
    const uint16_t strCommand = hdr.get_command();
    if (strCommand != RREQ && strCommand != RREP && strCommand != RERR && strCommand != SENDPACKET)
    {
        // not a roam message type
        return false;
    }
    // Message size
    DataStream &recv_stream = message.recv_stream;
    if (strCommand == RREQ)
    {
        uint64_t nonce = 0;
        PubKey searchKey;
        recv_stream >> nonce;
        recv_stream >> searchKey;
        //return ProcessRREQ(nonce, searchKey);
    }
    else if (strCommand == RREP)
    {
        uint64_t nonce = 0;
        PubKey searchKey;
        bool found;
        recv_stream >> nonce;
        recv_stream >> searchKey;
        recv_stream >> found;
        //return ProcessRREP(nonce, searchKey, found);
    }
    else if (strCommand == RERR)
    {
        // intentionally left blank
        // to be used for error reporting later
        //return ProcessRERR();
    }
    else if (strCommand == SENDPACKET)
    {
        PubKey searchKey;
        RoamPacketHeader newPacketHeader;
        recv_stream >> searchKey;
        // unpack one at a time
        recv_stream >> newPacketHeader;
        RoamPacket newPacket(newPacketHeader);
        recv_stream >> newPacket;
        return process_packet(pacman, searchKey, newPacket);
    }
    // message type not supported
    return false;
}

bool process_net_message(PacketManager* pacman, const uint8_t* data, uint64_t len_data)
{
    NetMessage msg;
    uint32_t handled = msg.read_header(data, len_data);
    if (handled != MessageHeader::HEADER_SIZE)
    {
        return false;
    }
    data = data + MessageHeader::HEADER_SIZE;
    len_data = len_data - MessageHeader::HEADER_SIZE;
    handled = msg.read_data(data, len_data);
    if (msg.complete() == false)
    {
        return false;
    }
    return process_net_message(pacman, msg);
}
