// This file is part of libroam
// Copyright (c) 2019-2022 Greg Griffith
// Copyright (c) 2019-2022 The Roam Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef ROAM_ROUTINGTAG_H
#define ROAM_ROUTINGTAG_H

#include "base58.h"
#include "key.h"
#include "pubkey.h"
#include "secure_memory.h"

/// RoutingTag is its own class instead of a pair holding isPrivate and a Key
/// or something similar to allow the addition of more data in the future if
/// needed without needing to restructure a lot of the code around it.

/// RoutingTag does not extend Key so that it may not be passed into a function that takes a ckey
class RoutingTag
{
private:
    bool isPrivate;
    // tags are compressed
    PubKey vchPubKey;
    PrivKey vchPrivKey;

public:
    RoutingTag()
    {
        isPrivate = true;
    }
    RoutingTag(bool _isPrivate, PubKey _vchPubKey, PrivKey _vchPrivKey)
    {
        isPrivate = _isPrivate;
        vchPubKey = _vchPubKey;
        vchPrivKey = _vchPrivKey;
    }

    ADD_SERIALIZE_METHODS

    template <typename Stream, typename Operation>
    inline void SerializationOp(Stream &s, Operation ser_action)
    {
        READWRITE(isPrivate);
        READWRITE(vchPubKey);
        READWRITE(vchPrivKey);
    }

    PubKey get_pubkey() const;
    PrivKey get_privkey() const;
    bool is_private() const;
    void convert_to_public_tag();
    void make_new_tag();
    bool verify_pubkey(const PubKey &pubkey) const;
    bool check_if_valid() const;
    bool sign(const uint256 &hash, std::vector<uint8_t> &vchSig, uint32_t test_case = 0) const;
    bool sign_compact(const uint256 &hash, std::vector<uint8_t> &vchSig) const;
};

static const std::vector<uint8_t> SECRET_KEY = std::vector<uint8_t>(1, 161);

/**
 * A base58-encoded secret key
 */
class TagSecret : public Base58Data
{
public:
    void set_key(const PrivKey &privkey)
    {
        Key key;
        // compressed is true. tags are always compressed.
        key.set_privkey(privkey);
        set_key(key);
    }
    void set_key(const Key &vchSecret)
    {
        assert(vchSecret.is_valid());
        set_data(SECRET_KEY, vchSecret.begin(), vchSecret.size());
        if (vchSecret.is_compressed())
        {
            vch_data.push_back(1);
        }
    }
    Key get_key()
    {
        Key ret;
        assert(vch_data.size() >= 32);
        ret.set(vch_data.begin(), vch_data.begin() + 32, vch_data.size() > 32 && vch_data[32] == 1);
        return ret;
    }
    bool is_valid() const
    {
        bool fExpectedFormat = vch_data.size() == 32 || (vch_data.size() == 33 && vch_data[32] == 1);
        bool fCorrectVersion = vch_version == SECRET_KEY;
        return fExpectedFormat && fCorrectVersion;
    }
    bool set_string(const char *pszSecret)
    {
        return Base58Data::set_string(pszSecret) && is_valid();
    }
    bool set_string(const std::string &strSecret)
    {
        return set_string(strSecret.c_str());
    }
    TagSecret(const Key &vchSecret)
    {
        set_key(vchSecret);
    }
    TagSecret(const PrivKey &vchSecret)
    {
        set_key(vchSecret);
    }
    TagSecret() {}
};

#endif // ECCOIN_ROUTINGTAG_H
