Currently using SQLite Version 3.44.2

Patches:
- `#define SQLITE_OMIT_LOAD_EXTENSION 1` is hardcoded at the top of sqlite3.c to disable sqlite extensions
