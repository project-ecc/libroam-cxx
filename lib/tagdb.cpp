// This file is part of libroam
// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2016 The Bitcoin Core developers
// Copyright (c) 2014-2019 The Eccoin developers
// Copyright (c) 2019-2022 Greg Griffith
// Copyright (c) 2019-2022 The Roam Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "tagdb.h"

#include "base58.h"
#include "tagstore.h"
#include "util.h"

class CTagDBState
{
public:
    uint32_t nKeys;
    uint32_t nCKeys;
    bool fIsEncrypted;

    CTagDBState()
    {
        nKeys = 0;
        nCKeys = 0;
        fIsEncrypted = false;
    }
};

namespace DBKeys
{
    const std::string TAG = "tag";
    const std::string CTAG = "ctag";
    const std::string MKEY = "mkey";
    const std::string LAST_PUBLIC_TAG = "lastpublictag";
    const std::string POOL = "pool";

    const std::vector<std::string> ALL_KEYS = {TAG, CTAG, MKEY, LAST_PUBLIC_TAG, POOL};
}

enum class DBErrors : int
{
    LOAD_OK = 0,
    NEED_RESCAN = 1,
    NEED_REWRITE = 2,
    EXTERNAL_SIGNER_SUPPORT_REQUIRED = 3,
    NONCRITICAL_ERROR = 4,
    TOO_NEW = 5,
    UNKNOWN_DESCRIPTOR = 6,
    LOAD_FAIL = 7,
    UNEXPECTED_LEGACY_ENTRY = 8,
    CORRUPT = 9,
};

bool RoutingTagDB::read_key(const DataStream &key, DataStream &value)
{
    std::unique_ptr<SQLiteBatch> batch = MakeBatch();
    return batch->read_key(key, value);
}

bool RoutingTagDB::write_key(const DataStream &key, const DataStream &value, bool overwrite)
{
    std::unique_ptr<SQLiteBatch> batch = MakeBatch();
    bool began = batch->TxnBegin();
    assert(began); // This is a critical error
    batch->write_key(key, value, overwrite);
    bool committed = batch->TxnCommit();
    return committed;
}

bool RoutingTagDB::erase_key(const DataStream &key)
{
    std::unique_ptr<SQLiteBatch> batch = MakeBatch();
    bool began = batch->TxnBegin();
    assert(began); // This is a critical error
    batch->erase_key(key);
    bool committed = batch->TxnCommit();
    return committed;
}

bool RoutingTagDB::write_tag(const RoutingTag &tag)
{
    // hash pubkey/privkey to accelerate wallet load
    // std::vector<uint8_t> vchKey;
    // vchKey.reserve(tag.vchPubKey.size() + tag.vchPrivKey.size());
    // vchKey.insert(tag.vchKey.end(), tag.vchPubKey.begin(), tag.vchPubKey.end());
    // vchKey.insert(tag.vchKey.end(), tag.vchPrivKey.begin(), tag.vchPrivKey.end());
    DataStream key;
    key << std::make_pair(std::string("tag"), tag.get_pubkey());
    DataStream value;
    value << tag;
    return write_key(key, value, false);
    // std::make_pair(tag.vchPrivKey, hash_dbl_sha256(vchKey.begin(), vchKey.end())), false);
}

bool RoutingTagDB::write_crypted_tag(const RoutingTag &tag)
{
    DataStream key;
    key << std::make_pair(std::string("ctag"), tag.get_pubkey());
    DataStream value;
    value << tag;
    if (!write_key(key, value, false))
    {
        return false;
    }
    DataStream key2;
    key2 << std::make_pair(std::string("tag"), tag.get_pubkey());
    erase_key(key2);
    return true;
}

bool RoutingTagDB::write_master_tag(uint32_t nID, const MasterKey &kMasterKey)
{
    DataStream key;
    key << std::make_pair(std::string("mkey"), nID);
    DataStream value;
    value << kMasterKey;
    return write_key(key, value, true);
}

bool RoutingTagDB::write_last_used_public_tag(RoutingTag &publicRoutingTag)
{
    publicRoutingTag.convert_to_public_tag();
    DataStream key;
    key << std::string("lastpublictag");
    DataStream value;
    value << publicRoutingTag;
    return write_key(key, value, true);
}

bool RoutingTagDB::read_last_used_public_tag(RoutingTag &publicRoutingTag)
{
    DataStream key;
    key << std::string("lastpublictag");
    DataStream value;
    if (read_key(key, value))
    {
        value >> publicRoutingTag;
        return true;
    }
    return false;
}

bool RoutingTagDB::read_pool(int64_t nPool, KeyPoolEntry &keypool)
{
    DataStream key;
    key << std::make_pair(std::string("pool"), nPool);
    DataStream value;
    if (read_key(key, value))
    {
        value >> keypool;
        return true;
    }
    return false;
}

bool RoutingTagDB::write_pool(int64_t nPool, const KeyPoolEntry &keypool)
{
    DataStream key;
    key << std::make_pair(std::string("pool"), nPool);
    DataStream value;
    value << keypool;
    return write_key(key, value, true);
}

bool RoutingTagDB::erase_pool(int64_t nPool)
{
    DataStream key;
    key << std::make_pair(std::string("pool"), nPool);
    return erase_key(key);
}

bool ReadKeyValue(NetTagStore *pwallet,
    DataStream &ssKey,
    DataStream &ssValue,
    CTagDBState &wss,
    std::string &strType,
    std::string &strErr)
{
    try
    {
        // Unserialize
        // Taking advantage of the fact that pair serialization
        // is just the two items serialized one after the other
        ssKey >> strType;
        if (strType == DBKeys::TAG)
        {
            PubKey vchPubKey;
            ssKey >> vchPubKey;
            if (!vchPubKey.is_valid())
            {
                strErr = "Error reading tag database: A Tag PubKey is corrupt";
                return false;
            }
            RoutingTag tag;
            wss.nKeys++;
            ssValue >> tag;
            if (!pwallet->LoadTag(tag))
            {
                strErr = "Error reading tag database: LoadTag failed";
                return false;
            }
        }
        else if (strType == DBKeys::MKEY)
        {
            uint32_t nID;
            ssKey >> nID;
            MasterKey kMasterKey;
            ssValue >> kMasterKey;
            if (pwallet->mapMasterKeys.count(nID) != 0)
            {
                char buff[1000];
                int size = snprintf(buff, 1000, "Error reading tag database: duplicate MasterKey id %u", nID);
                strErr = std::string(buff);
                return false;
            }
            pwallet->mapMasterKeys[nID] = kMasterKey;
            if (pwallet->nMasterKeyMaxID < nID)
                pwallet->nMasterKeyMaxID = nID;
        }
        else if (strType == DBKeys::CTAG)
        {
            PubKey vchPubKey;
            ssKey >> vchPubKey;
            if (!vchPubKey.is_valid())
            {
                strErr = "Error reading tag database: a crypted tag's PubKey is corrupt";
                return false;
            }
            RoutingTag tag;
            ssValue >> tag;
            wss.nCKeys++;
            if (!pwallet->LoadCryptedTag(tag))
            {
                strErr = "Error reading tag database: LoadCryptedTag failed";
                return false;
            }
            wss.fIsEncrypted = true;
        }
        else if (strType == DBKeys::LAST_PUBLIC_TAG)
        {
            ssValue >> pwallet->publicRoutingTag;
            if (!pwallet->LoadTag(pwallet->publicRoutingTag))
            {
                strErr = "Error reading tag database: lastpublictag LoadTag failed";
                return false;
            }
        }
        else if (strType == DBKeys::POOL)
        {
            int64_t nIndex;
            ssKey >> nIndex;
            KeyPoolEntry keypool;
            ssValue >> keypool;
            pwallet->setKeyPool.insert(nIndex);
        }
    }
    catch (...)
    {
        return false;
    }
    return true;
}

static bool IsKeyType(std::string strType)
{
    return (strType != "" && strType != DBKeys::POOL);
}

static bool LoadRecord(NetTagStore *pwallet, DataStream &ssKey, DataStream &ssValue, std::string &error)
{
    CTagDBState wss;
    // Try to be tolerant of single corrupt records:
    std::string strType;
    std::string strErr;
    if (!ReadKeyValue(pwallet, ssKey, ssValue, wss, strType, strErr))
    {
        // losing keys is considered a catastrophic error, anything else
        // we assume the user can live with:
        if (IsKeyType(strType))
        {
            return false;
        }
    }
    if (!strErr.empty())
    {
        // LogPrintf("%s\n", strErr);
    }
    return true;
}

struct LoadResult
{
    DBErrors m_result{DBErrors::LOAD_OK};
    int m_records{0};
};

static LoadResult LoadRecords(NetTagStore* pwallet, SQLiteBatch& batch, const std::string& key, DataStream& prefix)
{
    LoadResult result;
    DataStream ssKey(SER_NETWORK, ROAM_GENERAL_VERSION);
    DataStream ssValue(SER_NETWORK, ROAM_GENERAL_VERSION);

    std::unique_ptr<SQLiteCursor> cursor = batch.GetNewPrefixCursor(prefix.data(), prefix.size());
    if (!cursor)
    {
        printf("Error getting database cursor for '%s' records\n", key.c_str());
        result.m_result = DBErrors::CORRUPT;
        return result;
    }

    while (true)
    {
        SQLiteCursor::Status status = cursor->Next(ssKey, ssValue);
        if (status == SQLiteCursor::Status::DONE)
        {
            break;
        }
        else if (status == SQLiteCursor::Status::FAIL)
        {
            printf("Error reading next '%s' record from database\n", key.c_str());
            result.m_result = DBErrors::CORRUPT;
            return result;
        }
        std::string type;
        ssKey >> type;
        assert(type == key);
        std::string error;
        //printf("loading %s record \n", type.c_str());
        bool record_res = LoadRecord(pwallet, ssKey, ssValue, error);
        if (record_res == false)
        {
            //printf("%s\n", error);
            result.m_result = DBErrors::CORRUPT;
            return result;
        }
        //result.m_result = std::max(result.m_result, record_res);
        ++result.m_records;
    }
    return result;
}

bool RoutingTagDB::load_all_records(NetTagStore* pwallet)
{
    pwallet->publicRoutingTag = RoutingTag();
    SQLiteBatch batch(*this);
    for (const std::string &key : DBKeys::ALL_KEYS)
    {
        DataStream prefix(SER_NETWORK, ROAM_GENERAL_VERSION);
        prefix << key;
        LoadResult res = LoadRecords(pwallet, batch, key, prefix);
        if (res.m_result != DBErrors::LOAD_OK)
        {
            return false;
        }
    }
    return true;
}
