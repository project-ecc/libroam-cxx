// This file is part of libroam
// Copyright (c) 2019-2022 Greg Griffith
// Copyright (c) 2019-2022 The Roam Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef ROAM_PACKETMANAGER_H
#define ROAM_PACKETMANAGER_H

#include "aodv.h"
#include "datapacket_util.h"
#include "tagstore.h"
#include "key.h"
#include "util.h"

#include <map>
#include <utility>

// unused
static const uint8_t DEFAULT_TIMEOUT_CHECK_INTERVAL = 3; // 3 seconds

// unused
static const uint8_t DEFAULT_PACKET_TIMEOUT = 30; // 30 seconds

// unused
static const uint8_t DEFAULT_BUFFER_TIMEOUT = 60; // 60 seconds

class PacketBuffer
{
protected:
        // vec_recieved_packets should be partially stored on disk at some point
    std::vector<RoamPacket> vec_recieved_packets;

    // the protocol id using this buffer, matched RoamPacket::n_source_buffer_id
    uint16_t n_peer_buffer_id;

    // the token needed for authentication to read vec_recieved_packets
    // TODO : use a different token method because this one is very expensive to use often
    Key bound_key;

    // the bound_pubkey is the pubkey for the buffer, it was returned to the program as part of
    // buffer registration. it can be discarded or use for extra validation if desired
    PubKey bound_pubkey;

    // the last time the buffer was accessed
    uint64_t n_last_good_buffer_time;


    // rejects unsigned packets if set to true, set when buffer is registered,
    // cannot be changed unless buffer is released and reregistered
    bool bool_reject_unsigned;

    bool bool_has_new_data;

protected:
    PacketBuffer()
    {
        free_buffer();
    }

    bool is_used() const
    {
        if (bound_key.is_valid() == false)
        {
            printf("Buffer.is_used() return false, bound_key is not valid\n");
        }
        if (bound_pubkey.is_valid() == false)
        {
            printf("Buffer.is_used() return false, bound_pubkey is not valid\n");
        }
        return (bound_key.is_valid() == true && bound_pubkey.is_valid() == true);
    }

    void free_buffer()
    {
        vec_recieved_packets.clear();
        n_peer_buffer_id = 0;
        bound_key = Key();
        bound_pubkey = PubKey("");
        n_last_good_buffer_time = (uint64_t)get_time();
        bool_reject_unsigned = false;
        bool_has_new_data = false;
    }

    friend class PacketManager;

};


class PacketManager
{
    // Data members
private:
    std::recursive_mutex mutex;
    // Buffer holds info for communications to a single peer
    std::vector<PacketBuffer> vec_buffers;
    NetTagStore _tagstore;
    AodvRouteTable _aodvtable;
    Secp256k1VerifyHandle* verify_context;
public:


    // Methods
private:
    // disallow copies
    PacketManager(const PacketManager &pman) = delete;

    bool register_buffer(RoamPacket &newPacket);

public:
    PacketManager() = delete;
    PacketManager(const std::string &strDirName, const std::string &strFileName)
    {
        verify_context = new Secp256k1VerifyHandle();
        vec_buffers.clear();
        // not memory efficient, but instant access is instant
        vec_buffers = std::vector<PacketBuffer>(std::numeric_limits<uint16_t>::max(), PacketBuffer());

        _tagstore.set_null();
        _tagstore.load(strDirName, strFileName);
    }

    NetTagStore* get_tag_store()
    {
        return &_tagstore;
    }

    AodvRouteTable* get_routing_table()
    {
        return &_aodvtable;
    }

    bool process_packet(RoamPacket &newHeader);

    // ret should be a nullptr
    // if returned value is positive, it is size of data in ret
    // if returned value is negative, it is the error code
    int64_t create_packet(const std::vector<uint8_t> &vPubKey, const uint16_t nBufferId, const std::vector<uint8_t> &v_data, std::vector<uint8_t> &ret);
    int64_t create_packet(const std::vector<uint8_t> &vPubKey, const uint16_t nBufferId, const std::vector<uint8_t> &v_data,
        const std::string &strSenderPubKey, const std::vector<uint8_t> &v_signature, std::vector<uint8_t> &ret);

    // returns the bufferId, and pubkey of the newly registered buffer
    uint16_t register_buffer(std::string &strPubkey, const bool reject);

    bool get_buffer(const uint16_t bufferId, std::vector<RoamPacket> &bufferData);

    bool release_buffer(const uint16_t bufferId);

    bool get_buffer_key(const PubKey &pubkey, Key &key);

    std::vector<uint16_t> get_ids_with_new_data();

    // returns true if buffer timeout was reset, false if it was not reset or if bufferId not in use
    bool reset_buffer_timeout(const uint16_t bufferId);

    void record_request_origin(const uint64_t &nonce, const PubKey &source);

    bool get_request_origin(const uint64_t &nonce, PubKey &source);

    void record_route_to_peer(const PubKey &searchKey, const NodeId &node);

    bool have_route(const PubKey &key);

    bool get_key_node(const PubKey &key, NodeId &result);

    bool is_old_request(const uint64_t &nonce);

    void add_new_request_time_data(const uint64_t &nonce);
};

#endif
