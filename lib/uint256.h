// This file is part of the libroam c++ library
// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2016 The Bitcoin Core developers
// Copyright (c) 2014-2018 The Eccoin developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BITCOIN_UINT256_H
#define BITCOIN_UINT256_H

#include <assert.h>
#include <cstring>
#include <stdexcept>
#include <stdint.h>
#include <string>
#include <vector>

/** Template base class for fixed-sized opaque blobs. */
template <uint32_t BITS>
class base_blob
{
protected:
    enum
    {
        WIDTH = BITS / 8
    };
    uint8_t data[WIDTH];

public:
    base_blob()
    {
        std::memset(data, 0, sizeof(data));
    }
    explicit base_blob(const std::vector<uint8_t> &vch);

    bool is_null() const
    {
        for (int i = 0; i < WIDTH; i++)
        {
            if (data[i] != 0)
            {
                return false;
            }
        }
        return true;
    }

    void set_null()
    {
        std::memset(data, 0, sizeof(data));
    }
    friend inline bool operator==(const base_blob &a, const base_blob &b)
    {
        return std::memcmp(a.data, b.data, sizeof(a.data)) == 0;
    }
    friend inline bool operator!=(const base_blob &a, const base_blob &b)
    {
        return std::memcmp(a.data, b.data, sizeof(a.data)) != 0;
    }
    friend inline bool operator<(const base_blob &a, const base_blob &b)
    {
        return std::memcmp(a.data, b.data, sizeof(a.data)) < 0;
    }

    std::string get_hex() const;
    void set_hex(const char *psz);
    void set_hex(const std::string &str);
    std::string to_string() const;

    uint8_t *begin()
    {
        return &data[0];
    }
    uint8_t *end()
    {
        return &data[WIDTH];
    }
    const uint8_t *begin() const
    {
        return &data[0];
    }
    const uint8_t *end() const
    {
        return &data[WIDTH];
    }
    uint32_t size() const
    {
        return sizeof(data);
    }
    uint64_t get64(int n = 0) const
    {
        return data[2 * n] | (uint64_t)data[2 * n + 1] << 32;
    }
    uint32_t GetSerializeSize(int n_type, int n_version) const
    {
        return sizeof(data);
    }
    template <typename Stream>
    void Serialize(Stream &s) const
    {
        s.write((char *)data, sizeof(data));
    }

    template <typename Stream>
    void Unserialize(Stream &s)
    {
        s.read((char *)data, sizeof(data));
    }
};

/** 160-bit opaque blob.
 * @note This type is called uint160 for historical reasons only. It is an opaque
 * blob of 160 bits and has no integer operations.
 */
class uint160 : public base_blob<160>
{
public:
    uint160() {}
    uint160(const base_blob<160> &b) : base_blob<160>(b) {}
    explicit uint160(const std::vector<uint8_t> &vch) : base_blob<160>(vch) {}
};

/** 256-bit opaque blob.
 * @note This type is called uint256 for historical reasons only. It is an
 * opaque blob of 256 bits and has no integer operations. Use arith_uint256 if
 * those are required.
 */
class uint256 : public base_blob<256>
{
public:
    uint256() {}
    uint256(const base_blob<256> &b) : base_blob<256>(b) {}
    explicit uint256(const std::vector<uint8_t> &vch) : base_blob<256>(vch) {}
    /** A more secure, salted hash function.
     * @note This hash is not stable between little and big endian.
     */
    uint64_t get_hash(const uint256 &salt) const;
};

/* uint256 from const char *.
 * This is a separate function because the constructor uint256(const char*) can result
 * in dangerously catching uint256(0).
 */
inline uint256 uint256S(const char *str)
{
    uint256 rv;
    rv.set_hex(str);
    return rv;
}
/* uint256 from std::string.
 * This is a separate function because the constructor uint256(const std::string &str) can result
 * in dangerously catching uint256(0) via std::string(const char*).
 */
inline uint256 uint256S(const std::string &str)
{
    uint256 rv;
    rv.set_hex(str);
    return rv;
}

#endif // BITCOIN_UINT256_H
