// This file is part of the libroam c++ library
// Copyright (c) 2009-2017 The Bitcoin Core developers
// Copyright (c) 2015-2017 The Bitcoin Unlimited developers
// Copyright (c) 2014-2018 The Eccoin developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "key.h"

#include "crypto/common.h"
#include "crypto/hmac_sha512.h"
#include "pubkey.h"
#include "random.h"

#include <secp256k1.h>
#include <secp256k1_recovery.h>
#include <secp256k1_schnorr.h>

secp256k1_context *g_secp256k1_context_sign = nullptr;


/** These functions are taken from the libsecp256k1 distribution and are very ugly. */
static int ec_privkey_import_der(const secp256k1_context *ctx,
    uint8_t *out32,
    const uint8_t *privkey,
    size_t privkeylen)
{
    const uint8_t *end = privkey + privkeylen;
    int lenb = 0;
    int len = 0;
    memset(out32, 0, 32);
    /* sequence header */
    if (end < privkey + 1 || *privkey != 0x30)
    {
        return 0;
    }
    privkey++;
    /* sequence length constructor */
    if (end < privkey + 1 || !(*privkey & 0x80))
    {
        return 0;
    }
    lenb = *privkey & ~0x80;
    privkey++;
    if (lenb < 1 || lenb > 2)
    {
        return 0;
    }
    if (end < privkey + lenb)
    {
        return 0;
    }
    /* sequence length */
    len = privkey[lenb - 1] | (lenb > 1 ? privkey[lenb - 2] << 8 : 0);
    privkey += lenb;
    if (end < privkey + len)
    {
        return 0;
    }
    /* sequence element 0: version number (=1) */
    if (end < privkey + 3 || privkey[0] != 0x02 || privkey[1] != 0x01 || privkey[2] != 0x01)
    {
        return 0;
    }
    privkey += 3;
    /* sequence element 1: octet string, up to 32 bytes */
    if (end < privkey + 2 || privkey[0] != 0x04 || privkey[1] > 0x20 || end < privkey + 2 + privkey[1])
    {
        return 0;
    }
    memcpy(out32 + 32 - privkey[1], privkey + 2, privkey[1]);
    if (!secp256k1_ec_seckey_verify(ctx, out32))
    {
        memset(out32, 0, 32);
        return 0;
    }
    return 1;
}

static int ec_privkey_export_der(const secp256k1_context *ctx,
    uint8_t *privkey,
    size_t *privkeylen,
    const uint8_t *key32,
    int compressed)
{
    secp256k1_pubkey pubkey;
    size_t pubkeylen = 0;
    if (!secp256k1_ec_pubkey_create(ctx, &pubkey, key32))
    {
        *privkeylen = 0;
        return 0;
    }
    if (compressed)
    {
        static const uint8_t begin[] = {0x30, 0x81, 0xD3, 0x02, 0x01, 0x01, 0x04, 0x20};
        static const uint8_t middle[] = {0xA0, 0x81, 0x85, 0x30, 0x81, 0x82, 0x02, 0x01, 0x01, 0x30, 0x2C, 0x06,
            0x07, 0x2A, 0x86, 0x48, 0xCE, 0x3D, 0x01, 0x01, 0x02, 0x21, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFE, 0xFF, 0xFF, 0xFC, 0x2F, 0x30, 0x06, 0x04, 0x01, 0x00, 0x04, 0x01, 0x07, 0x04, 0x21, 0x02,
            0x79, 0xBE, 0x66, 0x7E, 0xF9, 0xDC, 0xBB, 0xAC, 0x55, 0xA0, 0x62, 0x95, 0xCE, 0x87, 0x0B, 0x07, 0x02, 0x9B,
            0xFC, 0xDB, 0x2D, 0xCE, 0x28, 0xD9, 0x59, 0xF2, 0x81, 0x5B, 0x16, 0xF8, 0x17, 0x98, 0x02, 0x21, 0x00, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFE, 0xBA, 0xAE, 0xDC,
            0xE6, 0xAF, 0x48, 0xA0, 0x3B, 0xBF, 0xD2, 0x5E, 0x8C, 0xD0, 0x36, 0x41, 0x41, 0x02, 0x01, 0x01, 0xA1, 0x24,
            0x03, 0x22, 0x00};
        uint8_t *ptr = privkey;
        memcpy(ptr, begin, sizeof(begin));
        ptr += sizeof(begin);
        memcpy(ptr, key32, 32);
        ptr += 32;
        memcpy(ptr, middle, sizeof(middle));
        ptr += sizeof(middle);
        pubkeylen = 33;
        secp256k1_ec_pubkey_serialize(ctx, ptr, &pubkeylen, &pubkey, SECP256K1_EC_COMPRESSED);
        ptr += pubkeylen;
        *privkeylen = ptr - privkey;
    }
    else
    {
        static const uint8_t begin[] = {0x30, 0x82, 0x01, 0x13, 0x02, 0x01, 0x01, 0x04, 0x20};
        static const uint8_t middle[] = {0xA0, 0x81, 0xA5, 0x30, 0x81, 0xA2, 0x02, 0x01, 0x01, 0x30, 0x2C, 0x06,
            0x07, 0x2A, 0x86, 0x48, 0xCE, 0x3D, 0x01, 0x01, 0x02, 0x21, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFE, 0xFF, 0xFF, 0xFC, 0x2F, 0x30, 0x06, 0x04, 0x01, 0x00, 0x04, 0x01, 0x07, 0x04, 0x41, 0x04,
            0x79, 0xBE, 0x66, 0x7E, 0xF9, 0xDC, 0xBB, 0xAC, 0x55, 0xA0, 0x62, 0x95, 0xCE, 0x87, 0x0B, 0x07, 0x02, 0x9B,
            0xFC, 0xDB, 0x2D, 0xCE, 0x28, 0xD9, 0x59, 0xF2, 0x81, 0x5B, 0x16, 0xF8, 0x17, 0x98, 0x48, 0x3A, 0xDA, 0x77,
            0x26, 0xA3, 0xC4, 0x65, 0x5D, 0xA4, 0xFB, 0xFC, 0x0E, 0x11, 0x08, 0xA8, 0xFD, 0x17, 0xB4, 0x48, 0xA6, 0x85,
            0x54, 0x19, 0x9C, 0x47, 0xD0, 0x8F, 0xFB, 0x10, 0xD4, 0xB8, 0x02, 0x21, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFE, 0xBA, 0xAE, 0xDC, 0xE6, 0xAF, 0x48, 0xA0,
            0x3B, 0xBF, 0xD2, 0x5E, 0x8C, 0xD0, 0x36, 0x41, 0x41, 0x02, 0x01, 0x01, 0xA1, 0x44, 0x03, 0x42, 0x00};
        uint8_t *ptr = privkey;
        memcpy(ptr, begin, sizeof(begin));
        ptr += sizeof(begin);
        memcpy(ptr, key32, 32);
        ptr += 32;
        memcpy(ptr, middle, sizeof(middle));
        ptr += sizeof(middle);
        pubkeylen = 65;
        secp256k1_ec_pubkey_serialize(ctx, ptr, &pubkeylen, &pubkey, SECP256K1_EC_UNCOMPRESSED);
        ptr += pubkeylen;
        *privkeylen = ptr - privkey;
    }
    return 1;
}

bool Key::check(const uint8_t *vch)
{
    return secp256k1_ec_seckey_verify(g_secp256k1_context_sign, vch);
}
void Key::make_new_key()
{
    do
    {
        get_strong_rand_bytes(vch, sizeof(vch));
    } while (!check(vch));
    bool_valid = true;
    bool_compressed = true;
}

bool Key::set_privkey(const PrivKey &privkey)
{
    if (!ec_privkey_import_der(g_secp256k1_context_sign, (uint8_t *)begin(), &privkey[0], privkey.size()))
    {
        return false;
    }
    bool_compressed = true;
    bool_valid = true;
    return true;
}

PrivKey Key::get_privkey() const
{
    assert(bool_valid);
    PrivKey privkey;
    int ret;
    size_t privkeylen;
    privkey.resize(279);
    privkeylen = 279;
    ret = ec_privkey_export_der(g_secp256k1_context_sign, (uint8_t *)&privkey[0], &privkeylen, begin(),
        bool_compressed ? SECP256K1_EC_COMPRESSED : SECP256K1_EC_UNCOMPRESSED);
    assert(ret);
    privkey.resize(privkeylen);
    return privkey;
}

PubKey Key::get_pubkey() const
{
    assert(bool_valid);
    secp256k1_pubkey pubkey;
    size_t clen = 65;
    PubKey result;
    int ret = secp256k1_ec_pubkey_create(g_secp256k1_context_sign, &pubkey, begin());
    assert(ret);
    secp256k1_ec_pubkey_serialize(g_secp256k1_context_sign, (uint8_t *)result.begin(), &clen, &pubkey,
        bool_compressed ? SECP256K1_EC_COMPRESSED : SECP256K1_EC_UNCOMPRESSED);
    assert(result.size() == clen);
    assert(result.is_valid());
    return result;
}

bool Key::sign_schorr(const uint256 &hash, std::vector<uint8_t> &vchSig, uint32_t test_case) const
{
    if (!bool_valid)
    {
        return false;
    }
    vchSig.resize(64);
    uint8_t extra_entropy[32] = {0};
    WriteLE32(extra_entropy, test_case);

    int ret = secp256k1_schnorr_sign(g_secp256k1_context_sign, &vchSig[0], hash.begin(), begin(),
        secp256k1_nonce_function_rfc6979, test_case ? extra_entropy : nullptr);
    assert(ret);
    return true;
}

bool Key::sign_compact(const uint256 &hash, std::vector<uint8_t> &vchSig) const
{
    if (!bool_valid)
    {
        return false;
    }
    vchSig.resize(65);
    int rec = -1;
    secp256k1_ecdsa_recoverable_signature sig;
    int ret = secp256k1_ecdsa_sign_recoverable(
        g_secp256k1_context_sign, &sig, hash.begin(), begin(), secp256k1_nonce_function_rfc6979, NULL);
    assert(ret);
    secp256k1_ecdsa_recoverable_signature_serialize_compact(
        g_secp256k1_context_sign, (uint8_t *)&vchSig[1], &rec, &sig);
    assert(ret);
    assert(rec != -1);
    vchSig[0] = 27 + rec + (bool_compressed ? 4 : 0);
    return true;
}

bool Key::load(PrivKey &privkey, PubKey &vchPubKey, bool fSkipCheck)
{
    if (!ec_privkey_import_der(g_secp256k1_context_sign, (uint8_t *)begin(), &privkey[0], privkey.size()))
    {
        return false;
    }
    bool_compressed = vchPubKey.is_compressed();
    bool_valid = true;
    if (fSkipCheck)
    {
        return true;
    }
    return verify_pubkey(vchPubKey);
}

bool Key::derive(Key &keyChild, uint256 &ccChild, uint32_t nChild, const uint256 &cc) const
{
    assert(is_valid());
    assert(is_compressed());
    uint8_t out[64];
    LockObject(out);
    if ((nChild >> 31) == 0)
    {
        PubKey pubkey = get_pubkey();
        assert(pubkey.begin() + 33 == pubkey.end());
        BIP32Hash(cc, nChild, *pubkey.begin(), pubkey.begin() + 1, out);
    }
    else
    {
        assert(begin() + 32 == end());
        BIP32Hash(cc, nChild, 0, begin(), out);
    }
    std::memcpy(ccChild.begin(), out + 32, 32);
    std::memcpy((uint8_t *)keyChild.begin(), begin(), 32);
    bool ret = secp256k1_ec_privkey_tweak_add(g_secp256k1_context_sign, (uint8_t *)keyChild.begin(), out);
    UnlockObject(out);
    keyChild.bool_compressed = true;
    keyChild.bool_valid = ret;
    return ret;
}

bool ecc_init_sanity_check()
{
    Key key;
    key.make_new_key();
    PubKey pubkey = key.get_pubkey();
    return key.verify_pubkey(pubkey);
}

bool Key::verify_pubkey(const PubKey &pubkey) const
{
    if (pubkey.is_compressed() != true)
    {
        return false;
    }
    uint8_t rnd[8];
    std::string str = "Roam key verification\n";
    get_strong_rand_bytes(rnd, sizeof(rnd));
    uint256 hash;
    DoubleSha256().write((uint8_t *)str.data(), str.size()).write(rnd, sizeof(rnd)).finalize(hash.begin());
    std::vector<uint8_t> vchSig;
    sign_schorr(hash, vchSig);
    return pubkey.verify_Schnorr(hash, vchSig);
}

void ecc_start()
{
    assert(g_secp256k1_context_sign == nullptr);

    secp256k1_context *ctx = secp256k1_context_create(SECP256K1_CONTEXT_SIGN);
    assert(ctx != nullptr);
    {
        // Pass in a random blinding seed to the secp256k1 context.
        uint8_t seed[32] = {0};
        LockObject(seed);
        get_strong_rand_bytes(seed, 32);
        bool ret = secp256k1_context_randomize(ctx, seed);
        assert(ret);
    }
    g_secp256k1_context_sign = ctx;
}

void ecc_stop()
{
    secp256k1_context *ctx = g_secp256k1_context_sign;
    g_secp256k1_context_sign = nullptr;
    if (ctx)
    {
        secp256k1_context_destroy(ctx);
    }
}
