// This file is part of libroam
// Copyright (c) 2019-2022 Greg Griffith
// Copyright (c) 2019-2022 The Roam Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "packetmanager.h"

// global package manager instance
// created when API calls roam_initalise
PacketManager* g_manager = nullptr;
