// This file is part of the libroam c++ library
// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2016 The Bitcoin Core developers
// Copyright (c) 2015-2017 The Bitcoin Unlimited developers
// Copyright (c) 2014-2018 The Eccoin developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BITCOIN_KEY_H
#define BITCOIN_KEY_H

#include "include/roam_serialize.h"
#include "pubkey.h"
#include "secure_memory.h"
#include "uint256.h"

#include <stdexcept>
#include <vector>


/**
 * secure_allocator is defined in allocators.h
 * PrivKey is a serialized private key, with all parameters included (279 bytes)
 */
typedef std::vector<uint8_t, secure_allocator<uint8_t> > PrivKey;
// crypted priv key is used for tags
typedef std::vector<uint8_t, secure_allocator<uint8_t> > CryptedPrivKey;

/** An encapsulated secp256k1 private key. */
class Key
{
private:
    //! Whether this private key is valid. We check for correctness when modifying the key
    //! data, so bool_valid should always correspond to the actual state.
    bool bool_valid;

    //! Whether the public key corresponding to this private key is (to be) compressed.
    bool bool_compressed;

    //! The actual byte data
    uint8_t vch[32];

    //! Check whether the 32-byte array pointed to be vch is valid keydata.
    bool static check(const uint8_t *vch);

public:
    //! Construct an invalid private key.
    Key() : bool_valid(false), bool_compressed(false)
    {
        LockObject(vch);
    }
    //! Copy constructor. This is necessary because of memlocking.
    Key(const Key &secret) : bool_valid(secret.bool_valid), bool_compressed(secret.bool_compressed)
    {
        LockObject(vch);
        memcpy(vch, secret.vch, sizeof(vch));
    }

    //! Destructor (again necessary because of memlocking).
    ~Key()
    {
        UnlockObject(vch);
    }
    friend bool operator==(const Key &a, const Key &b)
    {
        return a.bool_compressed == b.bool_compressed && a.size() == b.size() && memcmp(&a.vch[0], &b.vch[0], a.size()) == 0;
    }

    void operator=(const Key &b)
    {
        bool_valid = b.bool_valid;
        bool_compressed = b.bool_compressed;
        LockObject(vch);
        memcpy(vch, b.vch, sizeof(vch));
    }

    //! Initialize using begin and end iterators to byte data.
    template <typename T>
    void set(const T pbegin, const T pend, bool fCompressedIn)
    {
        if (pend - pbegin != 32)
        {
            bool_valid = false;
            return;
        }
        if (check(&pbegin[0]))
        {
            memcpy(vch, (uint8_t *)&pbegin[0], 32);
            bool_valid = true;
            bool_compressed = fCompressedIn;
        }
        else
        {
            bool_valid = false;
        }
    }

    //! Simple read-only vector-like interface.
    uint32_t size() const
    {
        return (bool_valid ? 32 : 0);
    }
    const uint8_t *begin() const
    {
        return vch;
    }
    const uint8_t *end() const
    {
        return vch + size();
    }
    //! Check whether this private key is valid.
    bool is_valid() const
    {
        return bool_valid;
    }
    //! Check whether the public key corresponding to this private key is (to be) compressed.
    bool is_compressed() const
    {
        return bool_compressed;
    }
    //! Initialize from a PrivKey (serialized OpenSSL private key data).
    bool set_privkey(const PrivKey &vchPrivKey);

    //! Generate a new private key using a cryptographic PRNG.
    void make_new_key();

    /**
     * Convert the private key to a PrivKey (serialized OpenSSL private key data).
     * This is expensive.
     */
    PrivKey get_privkey() const;

    /**
     * Compute the public key from a private key.
     * This is expensive.
     */
    PubKey get_pubkey() const;

    /**
     * Create a Schnorr signature.
     * The test_case parameter tweaks the deterministic nonce.
     */
    bool sign_schorr(const uint256 &hash, std::vector<uint8_t> &vchSig, uint32_t test_case = 0) const;


    /**
     * Create a compact signature (65 bytes), which allows reconstructing the used public key.
     * The format is one header byte, followed by two times 32 bytes for the serialized r and s values.
     * The header byte: 0x1B = first key with even y, 0x1C = first key with odd y,
     *                  0x1D = second key with even y, 0x1E = second key with odd y,
     *                  add 0x04 for compressed keys.
     */
    bool sign_compact(const uint256 &hash, std::vector<uint8_t> &vchSig) const;

    //! Derive BIP32 child key.
    bool derive(Key &keyChild, uint256 &ccChild, uint32_t nChild, const uint256 &cc) const;

    /**
     * Verify thoroughly whether a private key and a public key match.
     * This is done using a different mechanism than just regenerating it.
     * (An ECDSA signature is created then verified.)
     */
    bool verify_pubkey(const PubKey &vchPubKey) const;

    //! Load private key and check that public key matches.
    bool load(PrivKey &privkey, PubKey &vchPubKey, bool fSkipCheck = false);
};

/** Initialize the elliptic curve support. May not be called twice without calling ecc_stop first. */
void ecc_start(void);

/** Deinitialize the elliptic curve support. No-op if ecc_start wasn't called first. */
void ecc_stop(void);

/** Check that required EC support is available at runtime. */
bool ecc_init_sanity_check(void);

#endif // BITCOIN_KEY_H
