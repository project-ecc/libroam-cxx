// This file is part of libroam
// Copyright (c) 2019-2022 Greg Griffith
// Copyright (c) 2019-2022 The Roam Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "packetmanager.h"

#include <stdlib.h>

////////////////////////
///
///  Private
///



bool PacketManager::register_buffer(RoamPacket &newPacket)
{
    printf("inside internal RegisterBuffer \n");
    bool reject = !newPacket.v_signature.empty();
    std::string strPubkey = "";
    uint16_t bufferId = register_buffer(strPubkey, reject);
    if (bufferId == 0)
    {
        printf("returning false in RegisterBuffer (private method) \n");
        return false;
    }
    PacketBuffer &buffer = vec_buffers[bufferId];
    buffer.n_peer_buffer_id = newPacket.n_source_buffer_id;
    if (buffer.bool_reject_unsigned == true && newPacket.v_signature.empty())
    {
        // LogPrint("roam", "PacketManager::process_packet(): received unsigned packet on buffer that requires signature \n");
        return false;
    }
    if (packet_verify_signature(newPacket) == false)
    {
        // LogPrint("roam", "PacketManager::process_packet(): ERROR, failed to verify packet signature \n");
        return false;
    }
    buffer.vec_recieved_packets.push_back(std::move(newPacket));
    printf("register_buffer(): setting bool_has_new_data to true (%u) \n", bufferId);
    buffer.bool_has_new_data = true;
    // GetMainSignals().PacketComplete(newPacket.nProtocolId);
    return true;
}


////////////////////////
///
///  Public
///

bool PacketManager::process_packet(RoamPacket &newPacket)
{
    std::lock_guard<std::recursive_mutex> lock(mutex);
    // check buffer is active before checking the packet
    if (newPacket.n_dest_buffer_id == 0)
    {
        register_buffer(newPacket);
        return true;
    }
    PacketBuffer &buffer = vec_buffers[newPacket.n_dest_buffer_id];
    printf("checking if buffer is used in process packet \n");
    if (buffer.is_used() == false)
    {
        // protocolId needs to be bound by BindBuffer
        // LogPrint("roam", "PacketManager::process_packet(): ERROR, buffer %u is not in use \n", newPacket.nProtocolId);
        return false;
    }
    if (buffer.n_peer_buffer_id == 0)
    {
        buffer.n_peer_buffer_id = newPacket.n_source_buffer_id;
    }
    if (buffer.bool_reject_unsigned == true && newPacket.v_signature.empty())
    {
        // LogPrint("roam", "PacketManager::process_packet(): received unsigned packet on buffer that requires signature \n");
        return false;
    }
    if (packet_verify_signature(newPacket) == false)
    {
        // LogPrint("roam", "PacketManager::process_packet(): ERROR, failed to verify packet signature \n");
        return false;
    }
    buffer.vec_recieved_packets.push_back(std::move(newPacket));
    printf("process_packet(): setting bool_has_new_data to true (%u) \n", newPacket.n_dest_buffer_id);
    buffer.bool_has_new_data = true;
    // GetMainSignals().PacketComplete(newPacket.nProtocolId);
    return true;
}

int64_t PacketManager::create_packet(const std::vector<uint8_t> &vPubKey, const uint16_t nBufferId,
    const std::vector<uint8_t> &v_data, const std::string &strSenderPubKey,
    const std::vector<uint8_t> &v_signature, std::vector<uint8_t> &ret)
{
    std::lock_guard<std::recursive_mutex> lock(mutex);
    PacketBuffer &buffer = vec_buffers[nBufferId];
    RoamPacket newPacket(buffer.n_peer_buffer_id, nBufferId);
    packet_add_data(newPacket, v_data);
    if (strSenderPubKey != "" && v_signature != std::vector<uint8_t>{})
    {
        packet_add_sender(newPacket, PubKey(strSenderPubKey).get_bytes());
        packet_add_signature(newPacket, v_signature);
        if (packet_verify_signature(newPacket) == false)
        {
            // LogPrint("roam", "PacketManager::SendPacket(): ERROR, failed to verify packet signature \n");
            return 0;
        }
    }
    printf("create_packet(): vPubKey size = %lu \n", vPubKey.size());
    PubKey searchKey(vPubKey);
    printf("create_packet(): searchKey raw = %s \n", searchKey.get_hex().c_str());
    printf("create_packet(): searchKey hash = %s \n", searchKey.get_hash().get_hex().c_str());
    {
        // LOCK(_tagestore.cs_tagstore);

        // check if sending to self
        if (_tagstore.HaveTag(searchKey.get_key_id()))
        {
            printf("create_packet(): destination tag is ours, this is a local data transfer\n");
            // LogPrint("roam", "PacketManager::SendPacket(): Sending packet to local buffer \n");
            if(register_buffer(newPacket))
            {
                ret = packet_serialize(newPacket);
                return ret.size();
            }
            return -1;
        }
        printf("create_packet(): dest tag was not ours\n");
    }
    NodeId peerNode;
    if (!_aodvtable.get_key_node(vPubKey, peerNode))
    {
        // LogPrint("roam", "PacketManager::SendPacket(): ERROR, failed to get node id for provided key \n");
        return -1;
    }
    std::vector<uint8_t> vchPacket = packet_serialize(newPacket);
    ret = vchPacket;
    return vchPacket.size();
}

int64_t PacketManager::create_packet(const std::vector<uint8_t> &vPubKey, const uint16_t nBufferId,
    const std::vector<uint8_t> &v_data, std::vector<uint8_t> &ret)
{
    std::lock_guard<std::recursive_mutex> lock(mutex);
    return create_packet(vPubKey, nBufferId, v_data, "", {}, ret);
}

uint16_t PacketManager::register_buffer(std::string &strPubkey, const bool reject)
{
    std::lock_guard<std::recursive_mutex> lock(mutex);
    // make new key
    Key _key;
    _key.make_new_key();
    PubKey _pubkey = _key.get_pubkey();
    if (_key.verify_pubkey(_pubkey) == false)
    {
        // LogPrint("roam", "PacketManager::register_buffer(): ERROR, failed to verify pubkey \n");
        printf("PacketManager::register_buffer(): ERROR, failed to verify pubkey \n");
        return 0;
    }
    uint16_t bufferId;
    while (true)
    {
        bufferId = rand();
        if (bufferId == 0)
        {
            bufferId++;
        }
        bufferId = bufferId % (std::numeric_limits<uint16_t>::max() - 1);
        // ensure bufferId is never 0
        bufferId++;
        printf("checking if buffer is used in RegisterBuffer \n");
        if (vec_buffers[bufferId].is_used() == false)
        {
            break;
        }
    }
    PacketBuffer &buffer = vec_buffers[bufferId];
    // should already be free but free anyway for sanity
    buffer.free_buffer();
    buffer.n_peer_buffer_id = 0;
    buffer.bound_key = _key;
    buffer.bound_pubkey = _pubkey;
    buffer.bool_reject_unsigned = reject;
    strPubkey = _pubkey.get_hex();
    return bufferId;
}

bool PacketManager::release_buffer(const uint16_t bufferId)
{
    std::lock_guard<std::recursive_mutex> lock(mutex);
    PacketBuffer &buffer = vec_buffers[bufferId];
    printf("checking if buffer is used in ReleaseBuffer \n");
    if (buffer.is_used() == false)
    {
        // the call did nothing, the buffer was not registered. the end state is the same as
        // the intended end state of the user so return true.
        return true;
    }
    buffer.free_buffer();
    return true;
}

bool PacketManager::get_buffer(const uint16_t bufferId, std::vector<RoamPacket> &bufferData)
{
    std::lock_guard<std::recursive_mutex> lock(mutex);
    PacketBuffer &buffer = vec_buffers[bufferId];
    printf("check if buffer is used in GetBuffer\n");
    if (buffer.is_used())
    {
        bufferData = buffer.vec_recieved_packets;
        buffer.vec_recieved_packets.clear();
        buffer.n_last_good_buffer_time = get_time();
        buffer.bool_has_new_data = false;
        return true;
    }
    return false;
}

bool PacketManager::get_buffer_key(const PubKey &pubkey, Key &key)
{
    std::lock_guard<std::recursive_mutex> lock(mutex);
    for (auto& buffer : vec_buffers)
    {
        // TODO: do a reverse mapping of pubkeys to used buffers so we dont have to search
        // like this
        if (buffer.bound_pubkey.get_key_id() == pubkey.get_key_id())
        {
            key = buffer.bound_key;
            return true;
        }
    }
    // LogPrint("roam", "PacketManager::get_buffer_key(): ERROR, no key found for provided pubkey \n");
    return false;
}

// not efficient but sufficient
std::vector<uint16_t> PacketManager::get_ids_with_new_data()
{
    std::lock_guard<std::recursive_mutex> lock(mutex);
    std::vector<uint16_t> res;
    for (uint16_t i = 0; i < std::numeric_limits<uint16_t>::max(); ++i)
    {
        if (vec_buffers[i].bool_has_new_data == true)
        {
            res.emplace_back(i);
        }
    }
    return res;
}

bool PacketManager::reset_buffer_timeout(const uint16_t bufferId)
{
    std::lock_guard<std::recursive_mutex> lock(mutex);
    printf("checking if buffer is used in ResetBufferTimeout\n");
    if (vec_buffers[bufferId].is_used())
    {
        vec_buffers[bufferId].n_last_good_buffer_time = get_time();
        return true;
    }
    // LogPrint("roam", "PacketManager::reset_buffer_timeout(): ERROR, cannot reset buffer timeout on unused buffer \n");
    return false;
}

void PacketManager::record_request_origin(const uint64_t &nonce, const PubKey &source)
{
    _aodvtable.add_new_request_source(nonce, source);
}

bool PacketManager::get_request_origin(const uint64_t &nonce, PubKey &source)
{
    return _aodvtable.get_request_source(nonce, source);
}

void PacketManager::record_route_to_peer(const PubKey &searchKey, const NodeId &node)
{
    _aodvtable.add_route(searchKey, node);
}

bool PacketManager::have_route(const PubKey &key)
{
    return _aodvtable.have_route(key);
}

bool PacketManager::get_key_node(const PubKey &key, NodeId &result)
{
    return _aodvtable.get_key_node(key, result);
}

bool PacketManager::is_old_request(const uint64_t &nonce)
{
    return _aodvtable.is_old_request(nonce);
}

void PacketManager::add_new_request_time_data(const uint64_t &nonce)
{
    _aodvtable.add_new_request_time_data(nonce);
}
