// This file is part of libroam
// Copyright (c) 2019-2022 Greg Griffith
// Copyright (c) 2019-2022 The Roam Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef ROAM_AODV_MESSAGES_H
#define ROAM_AODV_MESSAGES_H

#include "pubkey.h"

typedef int64_t NodeId;

struct RREQRESPONSE
{
    uint64_t nonce;
    PubKey source;
    PubKey pubkey;
    bool found;

    RREQRESPONSE(uint64_t _nonce, PubKey _source, PubKey _pubkey, bool _found)
    {
        nonce = _nonce;
        source = _source;
        pubkey = _pubkey;
        found = _found;
    }

    friend inline bool operator<(const RREQRESPONSE &a, const RREQRESPONSE &b)
    {
        return (a.nonce < b.nonce);
    }
};

#endif // ROAM_AODV_MESSAGES_H
