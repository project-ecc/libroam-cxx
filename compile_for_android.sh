#!/bin/bash

# check for -h or --help in the args
for var in "$@"
do
    if [ "$var" = "-h" ] || [ "$var" = "--help" ]; then
        echo "This script will build the dependencies to build libnexa for mobile architectures and build them."
        echo "To reset the build tree run 'make distclean'"
        echo "./autogen.sh must be run before running this script."
        echo "All parameters passed into this script will be passed into configure"
        exit 0
    fi
done

./autogen.sh

make distclean
cd libsecp256k1
make distclean
cd ..


# set the compiler stuff, this is the default location on ubuntu 24
# build aarch64
export CC=/usr/lib/android-sdk/ndk/27.2.12479018/toolchains/llvm/prebuilt/linux-x86_64/bin/aarch64-linux-android35-clang
export CXX=/usr/lib/android-sdk/ndk/27.2.12479018/toolchains/llvm/prebuilt/linux-x86_64/bin/aarch64-linux-android35-clang++
export AR=/usr/lib/android-sdk/ndk/27.2.12479018/toolchains/llvm/prebuilt/linux-x86_64/bin/llvm-ar
export RANLIB=/usr/lib/android-sdk/ndk/27.2.12479018/toolchains/llvm/prebuilt/linux-x86_64/bin/llvm-ranlib
mkdir build-aarch64-linux-android
cd build-aarch64-linux-android
../configure --host aarch64-linux-android
make -j`nproc`
cd ..


make distclean
cd libsecp256k1
make distclean
cd ..


#build armv7a
export CC=/usr/lib/android-sdk/ndk/27.2.12479018/toolchains/llvm/prebuilt/linux-x86_64/bin/armv7a-linux-androideabi35-clang
export CXX=/usr/lib/android-sdk/ndk/27.2.12479018/toolchains/llvm/prebuilt/linux-x86_64/bin/armv7a-linux-androideabi35-clang++
mkdir build-armv7a-linux-androideabi
cd build-armv7a-linux-androideabi
../configure --host armv7a-linux-androideabi
make -j`nproc`
cd ..

make distclean
cd libsecp256k1
make distclean
cd ..


# build i686-linux-android
export CC=/usr/lib/android-sdk/ndk/27.2.12479018/toolchains/llvm/prebuilt/linux-x86_64/bin/i686-linux-android35-clang
export CXX=/usr/lib/android-sdk/ndk/27.2.12479018/toolchains/llvm/prebuilt/linux-x86_64/bin/i686-linux-android35-clang++
mkdir build-i686-linux-android
cd build-i686-linux-android
../configure --host i686-linux-android
make -j`nproc`
cd ..

make distclean
cd libsecp256k1
make distclean
cd ..


# build x86_64-linux-android
export CC=/usr/lib/android-sdk/ndk/27.2.12479018/toolchains/llvm/prebuilt/linux-x86_64/bin/x86_64-linux-android35-clang
export CXX=/usr/lib/android-sdk/ndk/27.2.12479018/toolchains/llvm/prebuilt/linux-x86_64/bin/x86_64-linux-android35-clang++
mkdir build-x86_64-linux-android
cd build-x86_64-linux-android
../configure --host x86_64-linux-android
make -j`nproc`
cd ..

make distclean
cd libsecp256k1
make distclean
cd ..

# build riscv64-linux-android
export CC=/usr/lib/android-sdk/ndk/27.2.12479018/toolchains/llvm/prebuilt/linux-x86_64/bin/riscv64-linux-android35-clang
export CXX=/usr/lib/android-sdk/ndk/27.2.12479018/toolchains/llvm/prebuilt/linux-x86_64/bin/riscv64-linux-android35-clang++
mkdir build-riscv64-linux-android
cd build-riscv64-linux-android
../configure --host riscv64-linux-android
make -j`nproc`
cd ..


exit 0
